        <!-- Javascript Files -->
        <script src="assets/vendors/jquery/jquery-3.5.0.min.js"></script>
        <script src="assets/vendors/bootstrap/bootstrap.bundle.min.js"></script>
        <script src="assets/vendors/magnific-popup/jquery.magnific-popup.min.js"></script>
        <script src="assets/vendors/svg-inject/svg-inject.min.js"></script>
        <script src="assets/vendors/modal-stepes/modal-steps.min.js"></script>
        <script src="assets/vendors/emojione/emojionearea.min.js"></script>
        <script src="assets/js/app.js"></script>
        <script src="javascript/chat.js"></script>
        <script>
            // Scroll to end of chat
            document.querySelector('.chat-finished').scrollIntoView({
                block: 'end',               // "start" | "center" | "end" | "nearest",
                behavior: 'auto'          //"auto"  | "instant" | "smooth",
            });

        </script>
    </body>


<!-- Mirrored from themes.mintycodes.com/quicky/ltr/light-skin/chat-1.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 17 Jan 2023 10:55:19 GMT -->
</html>