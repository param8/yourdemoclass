<?php 
class Course_model extends CI_Model 
{

  public function __construct()
  {
      parent::__construct();

  }

  public function get_courses($condition){  
    $this->db->select('courses.*,category.category ,users.name as user_name,users.email as user_email,cities.name as city_name');
    $this->db->from('courses');
    $this->db->join('category', 'category.id = courses.categoryID','left');
    $this->db->join('users', 'users.id = courses.userID','left');
    $this->db->join('cities', 'cities.id = courses.locationID','left');
    $this->db->where($condition);
    if($this->session->userdata('course')){
        $this->db->like('course', $this->session->userdata('course'));
    }
    if($this->session->userdata('mode')){
        $this->db->where('mode', $this->session->userdata('mode'));
    }
    if($this->session->userdata('start_date')){
        $this->db->where('start_date', $this->session->userdata('start_date'));
    }
    $this->db->where('users.status',1);
    $this->db->where('courses.status',1);
    $this->db->order_by('courses.id','desc');
    //$this->db->limit($limit, $start);
    return $this->db->get()->result();
}





function make_query($condition)
{
  $this->db->select('courses.*,category.category ,users.name as user_name,users.email as user_email,cities.name as city_name');
    $this->db->from('courses');
    $this->db->join('category', 'category.id = courses.categoryID','left');
    $this->db->join('users', 'users.id = courses.userID','left');
    $this->db->join('cities', 'cities.id = courses.locationID','left');
    $this->db->where($condition);
    if($this->session->userdata('course')){
        $this->db->like('course', $this->session->userdata('course'));
    }
    if($this->session->userdata('mode')){
        $this->db->where('mode', $this->session->userdata('mode'));
    }
    if($this->session->userdata('start_date')){
        $this->db->where('start_date', $this->session->userdata('start_date'));
    }

    if($this->session->userdata('location_city')){
      $this->db->where('locationID', $this->session->userdata('location_city'));
  }
    $this->db->where('users.status',1);
    $this->db->where('courses.status',1);

if(isset($_POST["search"]["value"]) && !empty($_POST["search"]["value"]))
{
    $this->db->like('courses.course', $_POST["search"]["value"]);
    $this->db->or_like('courses.start_date', date('Y-m-d',strtotime($_POST["search"]["value"])));
    $this->db->or_like('courses.end_date', date('Y-m-d',strtotime($_POST["search"]["value"])));
    $this->db->or_like('category.category', $_POST["search"]["value"]);
    $this->db->or_like('users.name', $_POST["search"]["value"]);
    $this->db->or_like('cities.name', $_POST["search"]["value"]);
    $this->db->or_like('courses.mode', $_POST["search"]["value"]);
}

   $this->db->order_by('courses.id', 'desc');
}
  function make_datatables($condition){
  $this->make_query($condition,);
  if($_POST["length"] != -1)
  {
    $this->db->limit($_POST['length'], $_POST['start']);
  }
  $query = $this->db->get();
  //echo $this->db->last_query();die;
  return $query->result_array();
}

function get_filtered_data($condition){
  $this->make_query($condition);
  $query = $this->db->get();
  return $query->num_rows();
  //echo $this->db->last_query();die;
}
function get_all_data($condition)
{
  $this->db->select('courses.*,category.category ,users.name as user_name,users.email as user_email,cities.name as city_name');
  $this->db->from('courses');
  $this->db->join('category', 'category.id = courses.categoryID','left');
  $this->db->join('users', 'users.id = courses.userID','left');
  $this->db->join('cities', 'cities.id = courses.locationID','left');
  $this->db->where($condition);
  if($this->session->userdata('course')){
      $this->db->like('course', $this->session->userdata('course'));
  }
  if($this->session->userdata('mode')){
      $this->db->where('mode', $this->session->userdata('mode'));
  }
  if($this->session->userdata('start_date')){
      $this->db->where('start_date', $this->session->userdata('start_date'));
  }
  if($this->session->userdata('location_city')){
    $this->db->where('locationID', $this->session->userdata('location_city'));
}
  $this->db->where('users.status',1);
  $this->db->where('courses.status',1);

if(isset($_POST["search"]["value"]) && !empty($_POST["search"]["value"]))
{
  $this->db->like('courses.course', $_POST["search"]["value"]);
  $this->db->or_like('courses.start_date', date('Y-m-d',strtotime($_POST["search"]["value"])));
  $this->db->or_like('courses.end_date', date('Y-m-d',strtotime($_POST["search"]["value"])));
  $this->db->or_like('category.category', $_POST["search"]["value"]);
  $this->db->or_like('users.name', $_POST["search"]["value"]);
  $this->db->or_like('cities.name', $_POST["search"]["value"]);
  $this->db->or_like('courses.mode', $_POST["search"]["value"]);
}
  return $this->db->count_all_results();
}




function make_query_booked_course($condition)
{
  $this->db->select('booked_courses.*,courses.course as course_name,courses.start_date,courses.end_date,courses.fees,courses.description as course_discription,courses.discount,courses.mode,users.id as student_id,users.name as student_name,users.user_type as Student_user_type,users.email as student_email,users.contact as student_contact,users.profile_pic,category.category,u.id as institute_id,u.name as institute_name,u.user_type as institute_user_type,u.email as institute_email,u.contact as institute_contact,batch.batch as course_batch ,batch.time_formate');
  $this->db->from('booked_courses');
  $this->db->join('users', 'users.id = booked_courses.studentID','left');
  $this->db->join('courses', 'courses.id = booked_courses.courseID','left');
  $this->db->join('cities', 'cities.id = courses.locationID','left');
  $this->db->join('category', 'category.id = courses.categoryID','left');
  $this->db->join('users u', 'u.id = courses.userID','left');
  $this->db->join('batch ', 'batch.id = booked_courses.batch','left');
  $this->db->where($condition);
  $this->db->where('u.status',1);
  $this->db->where('courses.status',1);
  if($this->session->userdata('courses_booked')){
      $this->db->where('courses.id',$this->session->userdata('courses_booked'));  
  }
  $this->db->order_by('booked_courses.id','desc');

if(isset($_POST["search"]["value"]) && !empty($_POST["search"]["value"]))
{
    $this->db->like('courses.course', $_POST["search"]["value"]);
    $this->db->or_like('courses.start_date', date('Y-m-d',strtotime($_POST["search"]["value"])));
    $this->db->or_like('courses.end_date', date('Y-m-d',strtotime($_POST["search"]["value"])));
    $this->db->or_like('category.category', $_POST["search"]["value"]);
    $this->db->or_like('users.name', $_POST["search"]["value"]);
    $this->db->or_like('cities.name', $_POST["search"]["value"]);
    $this->db->or_like('courses.mode', $_POST["search"]["value"]);
}

   $this->db->order_by('courses.id', 'desc');
}
  function make_datatables_booked_course($condition){
  $this->make_query_booked_course($condition,);
  if($_POST["length"] != -1)
  {
    $this->db->limit($_POST['length'], $_POST['start']);
  }
  $query = $this->db->get();
  //echo $this->db->last_query();die;
  return $query->result_array();
}

function get_filtered_data_booked_course($condition){
  $this->make_query_booked_course($condition);
  $query = $this->db->get();
  return $query->num_rows();
  //echo $this->db->last_query();die;
}
function get_all_data_booked_course($condition)
{
  $this->db->select('booked_courses.*,courses.course as course_name,courses.start_date,courses.end_date,courses.fees,courses.description as course_discription,courses.discount,courses.mode,users.id as student_id,users.name as student_name,users.user_type as Student_user_type,users.email as student_email,users.contact as student_contact,users.profile_pic,category.category,u.id as institute_id,u.name as institute_name,u.user_type as institute_user_type,u.email as institute_email,u.contact as institute_contact,batch.batch as course_batch ,batch.time_formate');
  $this->db->from('booked_courses');
  $this->db->join('users', 'users.id = booked_courses.studentID','left');
  $this->db->join('courses', 'courses.id = booked_courses.courseID','left');
  $this->db->join('cities', 'cities.id = courses.locationID','left');
  $this->db->join('category', 'category.id = courses.categoryID','left');
  $this->db->join('users u', 'u.id = courses.userID','left');
  $this->db->join('batch ', 'batch.id = booked_courses.batch','left');
  $this->db->where($condition);
  $this->db->where('u.status',1);
  $this->db->where('courses.status',1);
  if($this->session->userdata('courses_booked')){
      $this->db->where('courses.id',$this->session->userdata('courses_booked'));  
  }
  $this->db->order_by('booked_courses.id','desc');

if(isset($_POST["search"]["value"]) && !empty($_POST["search"]["value"]))
{
  $this->db->like('courses.course', $_POST["search"]["value"]);
  $this->db->or_like('courses.start_date', date('Y-m-d',strtotime($_POST["search"]["value"])));
  $this->db->or_like('courses.end_date', date('Y-m-d',strtotime($_POST["search"]["value"])));
  $this->db->or_like('category.category', $_POST["search"]["value"]);
  $this->db->or_like('users.name', $_POST["search"]["value"]);
  $this->db->or_like('cities.name', $_POST["search"]["value"]);
  $this->db->or_like('courses.mode', $_POST["search"]["value"]);
}
  return $this->db->count_all_results();
}





  public function get__admin_courses($condition){  
    $this->db->select('courses.*,category.category ,users.name as user_name,users.email as user_email,cities.name as city_name');
    $this->db->from('courses');
    $this->db->join('category', 'category.id = courses.categoryID','left');
    $this->db->join('users', 'users.id = courses.userID','left');
    $this->db->join('cities', 'cities.id = courses.locationID','left');
    $this->db->where($condition);
    if($this->session->userdata('course')){
        $this->db->like('course', $this->session->userdata('course'));
    }
    if($this->session->userdata('mode')){
        $this->db->where('mode', $this->session->userdata('mode'));
    }
    if($this->session->userdata('start_date')){
        $this->db->where('start_date', $this->session->userdata('start_date'));
    }
    // $this->db->where('users.status',1);
    // $this->db->where('courses.status',1);
    $this->db->order_by('courses.id','desc');
    //$this->db->limit($limit, $start);
    return $this->db->get()->result();
}

public function get_courses_count($condition){
           $this->db->where($condition);
           $this->db->where('users.status',1);
           $this->db->where('courses.status',1);
           $this->db->join('users', 'users.id=courses.userID','left');
    return $this->db->count_all("courses");
}

// public function load_more_courses($condition){
//     $this->db->select('courses.*,category.category ,users.name as user_name,cities.city as city_name');
//     $this->db->from('courses');
//     $this->db->join('category', 'category.id = courses.categoryID','left');
//     $this->db->join('users', 'users.id = courses.userID','left');
//     $this->db->join('cities', 'cities.id = courses.locationID','left');
//     $this->db->where($condition); 
//     $this->db->limit(2);
//     return $this->db->get()->result();
// }

  public function get_course($condition){
    $this->db->select('courses.*,category.category ,users.name as user_name,users.user_type as userType,users.email as user_email,cities.name as city_name');
    $this->db->from('courses');
    $this->db->join('category', 'category.id = courses.categoryID','left');
    $this->db->join('users', 'users.id = courses.userID','left');
    $this->db->join('cities', 'cities.id = courses.locationID','left');
    $this->db->where($condition);
    $this->db->where('users.status',1);
    $this->db->where('courses.status',1);
    return $this->db->get()->row();
  }

  public function get_user_profile($condition,$user_type){
    if($user_type=='Institute'){
    $this->db->select('courses.*,users.name as user_name,users.user_type as userType,users.email as user_email,users.contact as user_contact,users.address as user_address,users.profile_pic,cities.name as city_name,
    site_info.discription as userDiscription,site_info.facebook_url as userFacebookURL,site_info.twitter_url as userTwitterURL,site_info.insta_url as userInstaURL,site_info.linkedin_url as userLinkedInURL,site_info.youtube_url as userYoutubeURL');
    }if($user_type=='Student'){
      $this->db->select('courses.*,users.name as user_name,users.user_type as userType,users.email as user_email,users.contact as user_contact,users.address as user_address,users.profile_pic,cities.name as city_name,
      student_detail.dob,student_detail.whatsappNo as studentWhatsAppNo,student_detail.school_collage_name,student_detail.target_exam, student_detail.skill ');
    }
    $this->db->from('courses');
    // $this->db->join('category', 'category.id = courses.categoryID','left');
    $this->db->join('users', 'users.id = courses.userID','left');
    $this->db->join('cities', 'cities.id = courses.locationID','left');
    if($user_type=='Institute'){
      $this->db->join('site_info', 'site_info.adminID = users.id','left');
    }
    if($user_type=='Student'){
      $this->db->join('student_detail', 'student_detail.studentID = users.id','left');
    }
    $this->db->where($condition);
    $this->db->where('users.status',1);
    $this->db->where('courses.status',1);
   return  $this->db->get()->row();
    // echo $this->db->last_query();die;
  }

  public function get_course_by_text($condition){
    $this->db->select('courses.*,category.category ,users.name as user_name,users.email as user_email,cities.name as city_name');
    $this->db->from('courses');
    $this->db->join('category', 'category.id = courses.categoryID','left');
    $this->db->join('users', 'users.id = courses.userID','left');
    $this->db->join('cities', 'cities.id = courses.locationID','left');
    $this->db->like($condition);
    $this->db->where('users.status',1);
    $this->db->where('courses.status',1);
    return $this->db->get()->result();
  }

  public function get_more_courses($condition){
    $this->db->select('courses.*,category.category ,users.name as user_name,users.user_type as userType,cities.name as city_name');
    $this->db->from('courses');
    $this->db->join('category', 'category.id = courses.categoryID','left');
    $this->db->join('users', 'users.id = courses.userID','left');
    $this->db->join('cities', 'cities.id = courses.locationID','left');
    $this->db->where($condition);
    $this->db->where('users.status',1);
    $this->db->where('courses.status',1);
    $this->db->order_by('rand()');
    $this->db->limit(10);
    return $this->db->get()->result();
}



public function get_courses_home($condition,$page_type = NULL){  
    $this->db->select('courses.*,category.category ,users.name as user_name,cities.name as city_name');
    $this->db->from('courses');
    $this->db->join('category', 'category.id = courses.categoryID','left');
    $this->db->join('users', 'users.id = courses.userID','left');
    $this->db->join('cities', 'cities.id = courses.locationID','left');
    $this->db->where($condition);
    $this->db->where('users.status',1);
    $this->db->where('courses.status',1);
    $this->db->limit(10);
    $this->db->order_by('rand()');
    return $this->db->get()->result();
}

  public function store_course($data){
    return $this->db->insert('courses',$data);
  }

 public function store_rating($data){
    return $this->db->insert('course_rating',$data);
 }


 public function get_ratings($condition){
  $this->db->select('course_rating.*,users.name,users.profile_pic');
  $this->db->from('course_rating');
  $this->db->join('users', 'users.id = course_rating.userID','left');
  $this->db->where($condition);
  $this->db->order_by('course_rating.id','desc');
  return $this->db->get()->result();
 }

 public function get_dubliccate_ratings($condition){
    $this->db->select('course_rating.*,users.name,users.profile_pic');
    $this->db->from('course_rating');
    $this->db->join('users', 'users.id = course_rating.userID','left');
    $this->db->where($condition);
    $this->db->order_by('course_rating.id','desc');
    return $this->db->get()->result();
   }

 public function get_rating_home(){
    $this->db->select('course_rating.*,users.name,users.profile_pic,users.user_type');
    $this->db->from('course_rating');
    $this->db->join('users', 'users.id = course_rating.userID','left');
    $this->db->group_by('course_rating.userID');
    $this->db->limit(4);
    $this->db->order_by('rand()');
    return $this->db->get()->result();
 }


 public function store_student_course($data)
 {
    return $this->db->insert('booked_courses',$data);
 }

 public function get_student_booked_course($condition){
    $this->db->select('booked_courses.*,courses.course as course_name,courses.start_date,courses.end_date,courses.fees,courses.description as course_discription,courses.discount,courses.mode,users.id as student_id,users.name as student_name,users.user_type as Student_user_type,users.email as student_email,users.contact as student_contact,users.profile_pic,category.category,u.id as institute_id,u.name as institute_name,u.user_type as institute_user_type,u.email as institute_email,u.contact as institute_contact,batch.batch as course_batch ,batch.time_formate');
    $this->db->from('booked_courses');
    $this->db->join('users', 'users.id = booked_courses.studentID','left');
    $this->db->join('courses', 'courses.id = booked_courses.courseID','left');
    $this->db->join('cities', 'cities.id = courses.locationID','left');
    $this->db->join('category', 'category.id = courses.categoryID','left');
    $this->db->join('users u', 'u.id = courses.userID','left');
    $this->db->join('batch ', 'batch.id = booked_courses.batch','left');
    $this->db->where($condition);
    $this->db->where('u.status',1);
    $this->db->where('courses.status',1);
    if($this->session->userdata('courses_booked')){
        $this->db->where('courses.id',$this->session->userdata('courses_booked'));  
    }
    $this->db->order_by('booked_courses.id','desc');
    return $this->db->get()->result();
}

public function get_count_booked_course($condition){
  $this->db->where($condition);
  return $this->db->get('booked_courses')->result();
}

public function get_home_booked_course($condition){
  $this->db->select('booked_courses.*,courses.course as course_name,courses.start_date,courses.end_date,courses.fees,courses.description as course_discription,courses.discount,courses.mode,users.name as student_name,users.email as student_email,users.contact as student_contact,users.profile_pic,category.category,u.name as institute_name,u.email as institute_email,u.contact as institute_contact,batch.batch as course_batch ,batch.time_formate');
  $this->db->from('booked_courses');
  $this->db->join('users', 'users.id = booked_courses.studentID','left');
  $this->db->join('courses', 'courses.id = booked_courses.courseID','left');
  $this->db->join('cities', 'cities.id = courses.locationID','left');
  $this->db->join('category', 'category.id = courses.categoryID','left');
  $this->db->join('users u', 'u.id = courses.userID','left');
  $this->db->join('batch ', 'batch.id = booked_courses.batch','left');
  $this->db->where($condition);
  $this->db->where('u.status',1);
  $this->db->where('courses.status',1);
  if($this->session->userdata('courses_booked')){
      $this->db->where('courses.id',$this->session->userdata('courses_booked'));  
  }
  $this->db->order_by('rand()');
  $this->db->limit('4');
  return $this->db->get()->result();
}

public function get_booked_course($condition){
    $this->db->where($condition);
    return $this->db->get('booked_courses')->row();
}
 
public function updateStudentCourse($data,$id){
    $this->db->where('id',$id);
    return $this->db->update('booked_courses',$data);
}

public function updateCourse($data,$id){
    $this->db->where('id',$id);
    return $this->db->update('courses',$data);
     //echo $this->db->last_query();die;
}

public function updateAvgCourse($data,$id){
  $this->db->where('id',$id);
  return $this->db->update('avrg_course_price',$data);
   //echo $this->db->last_query();die;
}

public function get_all_courses($condition){
		$this->db->where($condition);
    $this->db->where('users.status',1);
    $query = $this->db->get('users')->result_array();
    //print_r($query);die;
    foreach($query as $key => $que){
      $this->db->select('count(courses.id) as courseTotal');
      $this->db->where('userID',$que['id']);
      $this->db->where('status',1);
      $query[$key]['object'] = $this->db->get('courses')->row_array();
    }

   // print_r($query);die;
    return $query;
}

public function get_booked_course_by_student(){
  $this->db->select('courses.*');
  $this->db->join('courses', 'courses.id=booked_courses.courseID', 'left');
  $this->db->join('users', 'users.id=courses.userID', 'left');
  $this->db->where('users.status',1);
  $this->db->where('courses.status',1);
  $this->db->group_by('booked_courses.courseID'); 
  $query = $this->db->get('booked_courses')->result_array();
  
  foreach($query as $key => $row){
    $this->db->select('count(studentID) as totalStudent');
    $this->db->where('courseID',$row['id']);
    $this->db->group_by('studentID');
    $query[$key]['object'] = $this->db->get('booked_courses')->result_array();
  }
 // echo $this->db->last_query();die;
  return $query;
}

public function getcomments($condition){
  $this->db->select('course_rating.*,users.profile_pic');
  $this->db->join('users','users.id=course_rating.userID','left');
  $this->db->where($condition);
  $this->db->limit(3);
  return $this->db->get('course_rating')->result();
}

public function get_student_booked_courses($condition){
  $this->db->select('booked_courses.*,users.id as userID,users.email');
  $this->db->from('booked_courses');
  $this->db->join('users', 'users.id = booked_courses.studentID','left');
  $this->db->where($condition);
  $this->db->group_by('booked_courses.studentID');
  return $this->db->get()->result();
}

public function get_avg_course($condition){
  $this->db->select('AVG(price) as avg_price');
  $this->db->from('booked_courses');
  $this->db->where($condition);
  return $this->db->get()->row();
  //$this->db->last_query();
}

public function avg_courses_wise_location($condition){
 $this->db->select('*');
 $this->db->from('avrg_course_price');
 $this->db->group_by('city');
 $this->db->where($condition);
  return $this->db->get()->result();
 //$this->db->last_query();
}

public function get_max_course($condition){
  $this->db->select('courses.*,category.category ,users.name as user_name,cities.name as city_name');
    $this->db->from('courses');
    $this->db->join('category', 'category.id = courses.categoryID','left');
    $this->db->join('users', 'users.id = courses.userID','left');
    $this->db->join('cities', 'cities.id = courses.locationID','left');
    $this->db->where($condition);
    $this->db->where('users.status',1);
    //$this->db->limit(10);
    $this->db->order_by('rand()');
    return $this->db->get()->result();
}

public function get_low_course($condition){
  $this->db->select('courses.*,category.category ,users.name as user_name,cities.name as city_name');
    $this->db->from('courses');
    $this->db->join('category', 'category.id = courses.categoryID','left');
    $this->db->join('users', 'users.id = courses.userID','left');
    $this->db->join('cities', 'cities.id = courses.locationID','left');
    $this->db->where($condition);
    $this->db->where('users.status',1);
    //$this->db->limit(10);
    $this->db->order_by('rand()');
    return $this->db->get()->result();
}

public function get_institute_bycourse($course){
 // print_r($course);die;
  $this->db->select('users.id,users.name'); 
  $this->db->from('courses');
  $this->db->join('users', 'users.id = courses.userID','left');
  $this->db->like($course);
 return  $this->db->get()->result();
 
}

public function store_course_instittute($data){
  //print_r($data);die;
  return $this->db->insert('avrg_course_price',$data);
}

public function get_avgCourses($condition,$value){
  $this->db->select('*'); 
  $this->db->from('avrg_course_price');
  $this->db->where($condition);
  if($value !=""){
    $this->db->like('course',$value);
    $this->db->or_like('city',$value);
    $this->db->or_like('institute',$value);
    $this->db->or_like('price',$value);

  }
  
  return $this->db->get()->result();
  //echo $this->db->last_query(); die;

}

  public function get_city_courses($condition){
    $this->db->select('courses.*,category.category ,users.name as user_name,users.email as user_email,cities.name as city_name');
    $this->db->from('courses');
    $this->db->join('category', 'category.id = courses.categoryID','left');
    $this->db->join('users', 'users.id = courses.userID','left');
    $this->db->join('cities', 'cities.id = courses.locationID','left');
    $this->db->where($condition);
    $this->db->where('users.status',1);
    $this->db->order_by('courses.id','desc');
    //$this->db->limit($limit, $start);
    return $this->db->get()->result();
  }


  function make_query_city_course($condition)
{
  $this->db->select('courses.*,category.category ,users.name as user_name,users.email as user_email,cities.name as city_name');
    $this->db->from('courses');
    $this->db->join('category', 'category.id = courses.categoryID','left');
    $this->db->join('users', 'users.id = courses.userID','left');
    $this->db->join('cities', 'cities.id = courses.locationID','left');
    $this->db->where($condition);
    $this->db->where('users.status',1);

if(isset($_POST["search"]["value"]) && !empty($_POST["search"]["value"]))
{
    $this->db->like('courses.course', $_POST["search"]["value"]);
    $this->db->or_like('courses.start_date', date('Y-m-d',strtotime($_POST["search"]["value"])));
    $this->db->or_like('courses.end_date', date('Y-m-d',strtotime($_POST["search"]["value"])));
    $this->db->or_like('category.category', $_POST["search"]["value"]);
    $this->db->or_like('users.name', $_POST["search"]["value"]);
    $this->db->or_like('cities.name', $_POST["search"]["value"]);
    $this->db->or_like('courses.mode', $_POST["search"]["value"]);
}

   $this->db->order_by('courses.id', 'desc');
}
  function make_datatables_city_course($condition){
  $this->make_query_city_course($condition,);
  if($_POST["length"] != -1)
  {
    $this->db->limit($_POST['length'], $_POST['start']);
  }
  $query = $this->db->get();
  //echo $this->db->last_query();die;
  return $query->result_array();
}

function get_filtered_data_city_course($condition){
  $this->make_query_city_course($condition);
  $query = $this->db->get();
  return $query->num_rows();
  //echo $this->db->last_query();die;
}
function get_all_data_city_course($condition)
{
  $this->db->select('courses.*,category.category ,users.name as user_name,users.email as user_email,cities.name as city_name');
  $this->db->from('courses');
  $this->db->join('category', 'category.id = courses.categoryID','left');
  $this->db->join('users', 'users.id = courses.userID','left');
  $this->db->join('cities', 'cities.id = courses.locationID','left');
  $this->db->where($condition);
  
  $this->db->where('users.status',1);

if(isset($_POST["search"]["value"]) && !empty($_POST["search"]["value"]))
{
  $this->db->like('courses.course', $_POST["search"]["value"]);
  $this->db->or_like('courses.start_date', date('Y-m-d',strtotime($_POST["search"]["value"])));
  $this->db->or_like('courses.end_date', date('Y-m-d',strtotime($_POST["search"]["value"])));
  $this->db->or_like('category.category', $_POST["search"]["value"]);
  $this->db->or_like('users.name', $_POST["search"]["value"]);
  $this->db->or_like('cities.name', $_POST["search"]["value"]);
  $this->db->or_like('courses.mode', $_POST["search"]["value"]);
}
  return $this->db->count_all_results();
}


}