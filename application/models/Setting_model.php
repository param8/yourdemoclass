<?php 
class Setting_model extends CI_Model 
{

  public function __construct()
  {
      parent::__construct();

  }

  public function update_siteInfo($data){
    $adminID =  $this->session->userdata('id');
    $this->db->where('adminID', $adminID);
    return $this->db->update('site_info',$data);
    //echo $this->db->last_query();die;
  }

  public function store_siteInfo($data){
    return $this->db->insert('site_info',$data);
  }

  public function get_avg_courses($condition){
    $this->db->where($condition);
    return $this->db->get('avrg_course_price')->result();
  }
  public function get_avg_course_price($condition){
    $this->db->select('AVG(price) as avg_price,course,institute,price,city');
    $this->db->where($condition);
    return $this->db->get('avrg_course_price')->result();
  }
}