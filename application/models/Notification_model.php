<?php 
class Notification_model extends CI_Model 
{

  public function __construct()
  {
      parent::__construct();

  }

  public function get_notifications($condition){
    if($this->session->userdata('user_type')=='Admin'){
        $this->db->where($condition);
        $this->db->select('notification.*,send_to.name as reciverName, send_by.name as senderName');
        $this->db->join('users as send_to', 'send_to.id = notification.userID','left');
        $this->db->join('users as send_by', 'send_by.id = notification.send_by','left');
        $this->db->order_by('notification.id','desc');
    }else{
        $this->db->where($condition);
        $this->db->order_by('notification.id','desc');
    }
     return $this->db->get('notification')->result();
    // echo $this->db->last_query();die;
  }

  public function store_notification($data){
    return $this->db->insert('notification',$data);
  }

  public function update_notification($data,$id){
      if($this->session->userdata('user_type')!='Admin'){
           $this->db->where('userID',$id);
      }
    return $this->db->update('notification',$data);
  }


  function make_query($condition)
{
  if($this->session->userdata('user_type')=='Admin'){
   
   
    $this->db->select('notification.*,send_to.name as reciverName, send_by.name as senderName');
    $this->db->from('notification');
    $this->db->join('users as send_to', 'send_to.id = notification.userID','left');
    $this->db->join('users as send_by', 'send_by.id = notification.send_by','left');
    $this->db->where($condition);
    $this->db->order_by('notification.id','desc');
}else{
  $this->db->from('notification');
    $this->db->where($condition);
    $this->db->order_by('notification.id','desc');
}

if(isset($_POST["search"]["value"]) && !empty($_POST["search"]["value"]))
{
    $this->db->like('notification.message', $_POST["search"]["value"]);
    // $this->db->or_like('send_to.name', date('Y-m-d',strtotime($_POST["search"]["value"])));
    // $this->db->or_like('send_by.name', date('Y-m-d',strtotime($_POST["search"]["value"])));
}

}
  function make_datatables($condition){
  $this->make_query($condition,);
  if($_POST["length"] != -1)
  {
    $this->db->limit($_POST['length'], $_POST['start']);
  }
  $query = $this->db->get();
  //echo $this->db->last_query();die;
  return $query->result_array();
}

function get_filtered_data($condition){
  $this->make_query($condition);
  $query = $this->db->get();
  return $query->num_rows();
  //echo $this->db->last_query();die;
}
function get_all_data($condition)
{
  if($this->session->userdata('user_type')=='Admin'){
   
    $this->db->select('notification.*,send_to.name as reciverName, send_by.name as senderName');
    $this->db->from('notification');
    $this->db->join('users as send_to', 'send_to.id = notification.userID','left');
    $this->db->join('users as send_by', 'send_by.id = notification.send_by','left');
    $this->db->where($condition);
    $this->db->order_by('notification.id','desc');
}else{
    $this->db->from('notification');
    $this->db->where($condition);
    $this->db->order_by('notification.id','desc');
}

if(isset($_POST["search"]["value"]) && !empty($_POST["search"]["value"]))
{
    $this->db->like('notification.message', $_POST["search"]["value"]);
    // $this->db->or_like('send_to.name', date('Y-m-d',strtotime($_POST["search"]["value"])));
    // $this->db->or_like('send_by.name', date('Y-m-d',strtotime($_POST["search"]["value"])));
}
  return $this->db->count_all_results();
}

}