<!--=====================================-->
<!--=       Breadcrumb Area Start      =-->
<!--=====================================-->
<style>
  .notification{
  height: 41px!important;
  line-height: 29px!important;
  padding: 0 24px!important;
  }
</style>
<div class="edu-breadcrumb-area">
  <div class="container">
    <div class="breadcrumb-inner">
      <div class="page-title">
        <h1 class="title"><?=$page_title?></h1>
      </div>
      <ul class="edu-breadcrumb">
        <li class="breadcrumb-item"><a href="<?=base_url('home')?>">Home</a></li>
        <li class="separator"><i class="icon-angle-right"></i></li>
        <li class="breadcrumb-item"><a href="#"><?=$page_title?></a></li>
      </ul>
    </div>
  </div>
  <ul class="shape-group">
    <li class="shape-1">
      <span></span>
    </li>
    <li class="shape-2 scene"><img data-depth="2" src="<?=base_url('public/website/assets/images/about/shape-13.png')?>" alt="shape"></li>
    <li class="shape-3 scene"><img data-depth="-2" src="<?=base_url('public/website/assets/images/about/shape-15.png')?>" alt="shape"></li>
    <li class="shape-4">
      <span></span>
    </li>
    <li class="shape-5 scene"><img data-depth="2" src="<?=base_url('public/website/assets/images/about/shape-07.png')?>" alt="shape"></li>
  </ul>
</div>
<!--=====================================-->
<!--=          Login Area Start         =-->
<!--=====================================-->
<section class="account-page-area section-gap-equal">
  <div class="container position-relative">
    <div class="row g-5 justify-content-center">
      <div class="col-lg-5">
        <div class="login-form-box">
          <h3 class="title">Sign in</h3>
          <!-- <p>Don’t have an account? <a href="#">Sign up</a></p> -->
          <form action="<?=base_url('Authantication/login')?>" id="Frontlogin">
            <div class="form-group">
              <label for="current-log-email">Mobile or Email*</label>
              <input type="email" name="email" id="email" placeholder="Email or Mobile">
            </div>
            <div class="form-group">
              <label for="current-log-password">Password*</label>
              <input type="password" name="password" id="password" placeholder="Password">
              <span class="password-show"><i class="icon-76"></i></span>
            </div>
            <div class="form-group chekbox-area">
              <div class="edu-form-check">
                <input type="checkbox" id="remember-me">
                <label for="remember-me">Remember Me</label>
              </div>
              <a href="#" class="password-reset notification" data-toggle="modal" data-target="#resetPasswordModal">Lost your password?</a>
            </div>
            <div class="form-group">
              <button type="submit" class="edu-btn btn-medium">Sign in <i class="icon-4"></i></button>
            </div>
          </form>
        </div>
      </div>
      <div class="col-lg-5">
        <div class="login-form-box registration-form">
          <h3 class="title">Registration</h3>
          <!-- <p>Already have an account? <a href="#">Sign in</a></p> -->
          <form action="<?=base_url('Authantication/register')?>" id="Frontregister">
            <div class="form-group">
              <label for="email">User Type*</label>
              <?php $user_types = array('Institute'=>'Institute/Coaching','Student'=>'Student','Experts'=>'Experts');?>
              <select class="dropdown_css" id="user_type" name="user_type" onchange="userTypeFields(this.value)">
                  <option value="">Select User Type</option>
                <?php foreach($user_types as $key=>$userType){?>
                <option value="<?=$key?>"><?=$userType?></option>
                <?php } ?>
              </select>
            </div>
            <div id="reg_form"></div>
          </form>
        </div>
      </div>
    </div>
    <ul class="shape-group">
      <li class="shape-1 scene"><img data-depth="2" src="<?=base_url('public/website/assets/images/about/shape-07.png')?>" alt="Shape"></li>
      <li class="shape-2 scene"><img data-depth="-2" src="<?=base_url('public/website/assets/images/about/shape-13.png')?>" alt="Shape"></li>
      <li class="shape-3 scene"><img data-depth="2" src="<?=base_url('public/website/assets/images/counterup/shape-02.png')?>" alt="Shape"></li>
    </ul>
  </div>
</section>
<div class="modal fade" id="resetPasswordModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Forget Password</h5>
        <button type="button" class="close btn-secondary" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="<?=base_url('Ajax_controller/sendOTP')?>" method="POST" id="resetForm">
        <div class="modal-body">
          <div class="form-group">
            <input type="email" class="" id="email" name="email" onchange="checkEmail(this.value)" placeholder="Enter Email" style="border:1px;">
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-danger btn-lg  " data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary btn-lg ">Send</button>
        </div>
      </form>
    </div>
  </div>
</div>
<script>
  $("form#Frontlogin").submit(function(e) {
  $(':input[type="submit"]').prop('disabled', true);
  e.preventDefault();    
  var formData = new FormData(this);
  $.ajax({
  url: $(this).attr('action'),
  type: 'POST',
  data: formData,
  cache: false,
  contentType: false,
  processData: false,
  dataType: 'json',
  success: function (data) {
  if(data.status==200) {
  //$('.modal').modal('hide');
  //toastr.success(data.message);
    Swal.fire({
    icon: 'success',
    title: 'Success',
    text: data.message,
    //footer: '<a href="">Why do I have this issue?</a>'
    })
  $(':input[type="submit"]').prop('disabled', false);
  setTimeout(function(){
          if(data.user_type=='Institute'){
            location.href="<?=base_url('institute-home')?>";
          }else{
            location.href="<?=base_url('home')?>";
          }
  	
  }, 1000) 
  
  }else if(data.status==403) {
  //toastr.error(data.message);
     Swal.fire({
      icon: 'error',
      title: 'Oops...',
      text: data.message,
      //footer: '<a href="">Why do I have this issue?</a>'
    })
  $(':input[type="submit"]').prop('disabled', false);
  }else{
      Swal.fire({
      icon: 'error',
      title: 'Oops...',
      text: 'Something went wrong',
      //footer: '<a href="">Why do I have this issue?</a>'
    })
  $(':input[type="submit"]').prop('disabled', false);
  }
  },
  error: function(){} 
  });
  });
  
  $("form#Frontregister").submit(function(e) {
  $(':input[type="submit"]').prop('disabled', true);
  e.preventDefault();    
  var formData = new FormData(this);
  $.ajax({
  url: $(this).attr('action'),
  type: 'POST',
  data: formData,
  cache: false,
  contentType: false,
  processData: false,
  dataType: 'json',
  success: function (data) {
  if(data.status==200) {
  Swal.fire({
    icon: 'success',
    title: 'Success',
    text: data.message,
    //footer: '<a href="">Why do I have this issue?</a>'
    })
  $(':input[type="submit"]').prop('disabled', false);
  setTimeout(function(){
  	location.href="<?=base_url('my-account')?>";
  }, 1000) 
  
  }else if(data.status==403) {
  //toastr.error(data.message);
     Swal.fire({
      icon: 'error',
      title: 'Oops...',
      text: data.message,
      //footer: '<a href="">Why do I have this issue?</a>'
    })
  $(':input[type="submit"]').prop('disabled', false);
  }else{
      Swal.fire({
      icon: 'error',
      title: 'Oops...',
      text: 'Something went wrong',
      //footer: '<a href="">Why do I have this issue?</a>'
    })
  toastr.error('Something went wrong');
  $(':input[type="submit"]').prop('disabled', false);
  }
  },
  error: function(){} 
  });
  });
  
  // function resetPasswordModal(){
  //   $('#resetPassword').modal('show');
  // }
  
  function checkEmail(email){
  $.ajax({
  url: '<?=base_url('Ajax_controller/checkEmail')?>',
  type: 'POST',
  data: {email},
  dataType: 'json',
  success: function (data) {
      if(data.status==200){
             Swal.fire({
      icon: 'success',
      title: 'Success...',
      text: data.message,
      //footer: '<a href="">Why do I have this issue?</a>'
    }) 
      }else{
     Swal.fire({
      icon: 'error',
      title: 'Oops...',
      text: data.message,
      //footer: '<a href="">Why do I have this issue?</a>'
    })
      }

  }
  });
  }


  $("form#resetForm").submit(function(e) {
  $(':input[type="submit"]').prop('disabled', true);
  e.preventDefault();    
  var formData = new FormData(this);
  $.ajax({
  url: $(this).attr('action'),
  type: 'POST',
  data: formData,
  cache: false,
  contentType: false,
  processData: false,
  dataType: 'json',
  success: function (data) {
  if(data.status==200) {
  //$('.modal').modal('hide');
  //toastr.success(data.message);
    Swal.fire({
    icon: 'success',
    title: 'Success',
    text: data.message,
    //footer: '<a href="">Why do I have this issue?</a>'
    })
  $(':input[type="submit"]').prop('disabled', false);
  setTimeout(function(){
          
            location.href="<?=base_url('reset-password')?>";
         
  	
  }, 1000) 
  
  }else if(data.status==403) {
  //toastr.error(data.message);
     Swal.fire({
      icon: 'error',
      title: 'Oops...',
      text: data.message,
      //footer: '<a href="">Why do I have this issue?</a>'
    })
  $(':input[type="submit"]').prop('disabled', false);
  }else{
      Swal.fire({
      icon: 'error',
      title: 'Oops...',
      text: 'Something went wrong',
      //footer: '<a href="">Why do I have this issue?</a>'
    })
  $(':input[type="submit"]').prop('disabled', false);
  }
  },
  error: function(){} 
  });
  });
  
  function userTypeFields(user_type){
  $.ajax({
  url: '<?=base_url('Ajax_controller/registration_form')?>',
  type: 'POST',
  data: {user_type},
  success: function (data) {
    $('#reg_form').html(data);
  }
  });
  }
  $( document ).ready(function() {
  userTypeFields();
  });
  
  	$(document).ready(function(){
        $(window).scrollTop(400);
		   
	   });
    
</script>