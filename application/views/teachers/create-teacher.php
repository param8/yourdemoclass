         <div class="edu-breadcrumb-area">
            <div class="container">
                <div class="breadcrumb-inner">
                    <div class="page-title">
                        <h1 class="title"><?=$page_title?></h1>
                    </div>
                    <ul class="edu-breadcrumb">
                        <li class="breadcrumb-item"><a href="<?=base_url('home')?>">Home</a></li>
                        <li class="separator"><i class="icon-angle-right"></i></li>
                        <li class="breadcrumb-item active" aria-current="page"><?=$page_title?></li>
                    </ul>
                </div>
            </div>
            <ul class="shape-group">
                <li class="shape-1">
                    <span></span>
                </li>
                <li class="shape-2 scene"><img data-depth="2" src="<?=base_url('public/website/assets/images/about/shape-13.png')?>" alt="shape"></li>
                <li class="shape-3 scene"><img data-depth="-2" src="<?=base_url('public/website/assets/images/about/shape-15.png')?>" alt="shape"></li>
                <li class="shape-4">
                    <span></span>
                </li>
                <li class="shape-5 scene"><img data-depth="2" src="<?=base_url('public/website/assets/images/about/shape-07.png')?>" alt="shape"></li>
            </ul>
        </div>

       
        <!--=====================================-->
        <!--=      Contact Form Area Start      =-->
        <!--=====================================-->
        <section class="edu-section-gap contact-form-area">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-lg-8">
                        <div class="contact-form">
                            <div class="section-title section-center">
                                <h3 class="title">Teacher Basic Informations</h3>
                            </div>
                            <form class="rnt-contact-form rwt-dynamic-form" id="addTeacher" method="POST" action="<?=base_url('teachers/store')?>">
                                <div class="row row--10">
                                    <div class="form-group col-lg-6">
                                        <input class="form-control" type="text" name="name" id="name" placeholder="Teacher Name">
                                    </div>
                                    <div class="form-group col-lg-6">
                                        <select class="form-control" name="subjectID" id="subjectID">
                                          <option value="">Select Subject</option>
                                          <?php foreach($subjects as $subject){?>
                                            <option value="<?=$subject->id?>"><?=$subject->subject?></option>
                                            <?php } ?>
                                        </select>
                                    </div>

                                    <div class="form-group col-lg-6">
                                        <input type="text" class="form-control" name="experience" id="experience" placeholder="Teacher experience">
                                    </div>
                                    <div class="form-group col-lg-6">
                                        <input type="text" class="form-control" name="qulification" id="qulification" placeholder="Teacher Qualifiaction">
                                    </div>
                        
                                    <div class="form-group col-lg-6">
                                        <input type="text" class="form-control" name="students" id="students" placeholder="Number of students served" maxlength="4"   oninput="this.value = this.value.replace(/[^0-9]/g, '').replace(/(\..*)\./g, '$1');">
                                    </div>

                                    <div class="form-group col-6">
                                        <input type="file" class="form-control" name="image" id="image" placeholder="Upload teacher image">
                                    </div>
                                  
                                    <div class="form-group col-12 text-center">
                                        <button class="rn-btn edu-btn submit-btn" name="submit" type="submit">Submit Now <i class="icon-4"></i></button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <ul class="shape-group">
                <li class="shape-1 scene"><img data-depth="-2" src="<?=base_url('public/website/assets/images/about/shape-15.png')?>" alt="shape"></li>
                <li class="shape-2 scene"><img data-depth="2" src="<?=base_url('public/website/assets/images/cta/shape-04.png')?>" alt="shape"></li>
                <li class="shape-3 scene"><span data-depth="1"></span></li>
                <li class="shape-4 scene"><img data-depth="-2" src="<?=base_url('public/website/assets/images/about/shape-13.png')?>" alt="shape"></li>
            </ul>
        </section>
        <!--=====================================-->
        <!--=        Footer Area Start          =-->
        <!--=====================================-->
        <!-- Start Footer Area  -->

  <script>
    $("form#addTeacher").submit(function(e) {
      $(':input[type="submit"]').prop('disabled', true);
      e.preventDefault();    
      var formData = new FormData(this);
      $.ajax({
      url: $(this).attr('action'),
      type: 'POST',
      data: formData,
      cache: false,
      contentType: false,
      processData: false,
      dataType: 'json',
      success: function (data) {
        if(data.status==200) {
        //$('.modal').modal('hide');
        toastr.success(data.message);
        $(':input[type="submit"]').prop('disabled', false);
            setTimeout(function(){
              location.reload();
        }, 1000) 
    
        }else if(data.status==403) {
        toastr.error(data.message);
        $(':input[type="submit"]').prop('disabled', false);
        }else{
        toastr.error('Something went wrong');
        $(':input[type="submit"]').prop('disabled', false);
        }
      },
      error: function(){} 
      });
    });
   </script>
      