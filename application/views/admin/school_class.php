
  
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
	  <div class="container-full">
		<!-- Content Header (Page header) -->
		<div class="content-header">
			<div class="d-flex align-items-center">
				<div class="mr-auto">
					<h3 class="page-title"><i class="fa fa-university"> <?=$page_title?></i></h3>
					<div class="d-inline-block align-items-center">
						<nav>
							<ol class="breadcrumb">
								<li class="breadcrumb-item"><a href="#"><i class="mdi mdi-home-outline"></i></a></li>
								<li class="breadcrumb-item" aria-current="page"><?=$page_title?></li>
								<li class="breadcrumb-item active" aria-current="page"><?=$page_title?></li>
							</ol>
						</nav>
					</div>
				</div>
				
			</div>
		</div>

		<!-- Main content -->
		<section class="content">
		  <div class="row">
       <div class="col-md-6 col-lg-6">
			   <div class="box"> 
            <div class="box-header with-border">
                <h3 class="box-title">All <?=$page_title?></h3>
            </div>
          </div>
        </div>
        <div class="col-md-6 col-lg-6 ">
          <div class="box "> 
            <div class="box-header with-border">
              <a href="#" class="btn btn-primary btn-sm float-right" data-toggle="modal" data-target="#exampleModal" data-whatever="@mdo">Add Class <i class="fa fa-plus"></i></a>
            </div>
          </div>
				  <!-- <h6 class="box-subtitle">Export data to Copy, CSV, Excel, PDF & Print</h6> -->
				</div>
				<!-- /.box-header -->
				<div class="box-body">
					<div class="table-responsive">
					  <table id="example" class="table table-bordered table-hover display nowrap margin-top-10 w-p100">
						<thead>
							<tr>
                  <th>SNO</th>
                  <th>Image</th>
								  <th>School Name</th>
								  <th>Class Name</th>
                  <th>Language</th>
								  <th>Status</th>
								  <th>Created Date</th>
                  <th>Action</th>
							</tr>
						</thead>
						<tbody>
                <?php foreach($school_classes as $key=>$school_class){?>
							<tr>
								<td><?=$key+1;?></td>
                <td style="width: 20%;"><img src="<?=$school_class->image;?>" alt="book" style="width: 100px; height: 80px;"></td>
								<td><?= $school_class->name?></td>
								<td><?= $school_class->class?></td>
								<td><?= $school_class->language_name?></td>
								<td><?= $school_class->status == 1 ? '<span class="text-success">Active</span>' : '<span class="text-danger">De-Active</span>'?></td>
                <td><?= date('d-m-Y',strtotime($school_class->create_date));?></td>
                <td>
                  <a href="#" onclick="editModalShow('<?=$school_class->sc_u_id?>')" class="btn btn-warning btn-sm" data-toggle="tooltip" title="Edit School Class"><i class="fa fa-edit"></i></a>
                  <a href="#" onclick="deleteSchoolClass('<?=$school_class->sc_u_id?>')" class="btn btn-danger btn-sm" data-toggle="tooltip" title="Delete School Class"><i class="fa fa-trash"></i></a>
                </td>
							</tr>
             <?php } ?>
				
						</tbody>				  
					
					</table>
					</div>              
				</div>
				<!-- /.box-body -->
			  </div>
			  <!-- /.box -->          
			</div>
			<!-- /.col -->
		  </div>
		  <!-- /.row -->
		</section>
		<!-- /.content -->
	  
	  </div>
  </div>
  <!-- /.content-wrapper -->

  <!-- Add School Class Modal Start -->
  <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Add School Class</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="<?=base_url('admin/school_class/store')?>" id="addSchoolClass" method="POST" enctype="multipart/form-data">
      <div class="modal-body">
          <div class="form-group">
            <label for="s_id" class="col-form-label">School Name:</label>
            <select class="form-control school_id" name="s_id" id="s_id">
              <option value="">Select School</option>
              <?php foreach($schools as $school){?>
                <option value="<?=$school->id?>"><?=$school->name?></option>
              <?php } ?>
            </select>
          </div>
          <div class="form-group">
            <label for="language_id" class="col-form-label">Language:</label>
            <select class="form-control language_id" name="language_id" id="language_id">
              <option value="">Select Language</option>
              <?php foreach($languages as $language){?>
                <option value="<?=$language->id?>"><?=$language->language_name?></option>
              <?php } ?>
            </select>
          </div>
          <div class="form-group">
            <label for="classname" class="col-form-label">Class Name:</label>
            <input type="text" class="form-control" name="classname" id="classname">
          </div>
          <div class="form-group">
            <label for="remarks" class="col-form-label">Remarks:</label>
            <textarea  class="form-control" name="remarks" id="remarks"> </textarea>
          </div>
          <div class="form-group">
            <label for="image" class="col-form-label">Class Image:</label>
            <input type="file" class="form-control" name="image" id="image">
          </div>
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" name="submit" class="btn btn-primary">Submit</button>
      </div>
      </form>
    </div>
  </div>
</div>
  <!-- Add School Class Modal End -->

  <!-- Edit Schoo Class Modal Start -->
<div class="modal fade" id="editSchoolClassModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Edit School Class</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="<?=base_url('admin/school_class/update')?>" id="editSchoolClass" method="POST" enctype="multipart/form-data">
      <div class="modal-body" id="editFormData">
  
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" name="submit" class="btn btn-primary">Edit</button>
      </div>
      </form>
    </div>
  </div>
</div>
  <!-- Edit School Class Modal End -->
  
  <script type="text/javascript">
  $("form#addSchoolClass").submit(function(e) {
     $(':input[type="submit"]').prop('disabled', true);
     e.preventDefault();    
     var formData = new FormData(this);
     $.ajax({
       url: $(this).attr('action'),
       type: 'POST',
       data: formData,
       cache: false,
       contentType: false,
       processData: false,
       dataType: 'json',
       success: function (data) {
         if(data.status==200) {
           toastr.success(data.message);
  				setTimeout(function(){
                      location.reload();
           }, 1000) 
  
         }else if(data.status==403) {
           toastr.error(data.message);
           $(':input[type="submit"]').prop('disabled', false);
         }else{
           toastr.error('Unable to add school class');
           $(':input[type="submit"]').prop('disabled', false);
         }
       },
       error: function(){} 
     });
   });
  
   function editModalShow(uid){
    $.ajax({
       url: '<?=base_url('admin/school_class/editForm')?>',
       type: 'POST',
       data: {uid},
       success: function (data) {
        $('#editSchoolClassModal').modal('show');
         $('#editFormData').html(data);
       }
     });
  }

  $("form#editSchoolClass").submit(function(e) {
     $(':input[type="submit"]').prop('disabled', true);
     e.preventDefault();    
     var formData = new FormData(this);
     $.ajax({
       url: $(this).attr('action'),
       type: 'POST',
       data: formData,
       cache: false,
       contentType: false,
       processData: false,
       dataType: 'json',
       success: function (data) {
         if(data.status==200) {
           toastr.success(data.message);
  				setTimeout(function(){
                      location.reload();
           }, 1000) 
  
         }else if(data.status==403) {
           toastr.error(data.message);
           $(':input[type="submit"]').prop('disabled', false);
         }else{
           toastr.error('Unable to edit school class');
           $(':input[type="submit"]').prop('disabled', false);
         }
       },
       error: function(){} 
     });
   });

   function deleteSchoolClass(uid){
     var messageText  = "All the items in this class will also deleted!";
     var confirmText =  'Yes, delete it!';
     var message  ="Class deleted Successfully!";
    Swal.fire({
        title: 'Are you sure?',
        text: messageText,
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: confirmText
        }).then((result) => {
        if (result.isConfirmed) {
            $.ajax({
                url: '<?=base_url('admin/school_class/delete')?>', 
                method: 'POST',
                data: {uid},
                success: function(result){
                toastr.success(message);
                setTimeout(function(){
                   window.location.reload();
                }, 2000);
            }
          });      
          }
        })
  }
</script>
  