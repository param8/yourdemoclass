
  
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
	  <div class="container-full">
		<!-- Content Header (Page header) -->
		<div class="content-header">
			<div class="d-flex align-items-center">
				<div class="mr-auto">
					<h3 class="page-title"><i class="fa fa-book"> <?=$page_title?></i></h3>
					<div class="d-inline-block align-items-center">
						<nav>
							<ol class="breadcrumb">
								<li class="breadcrumb-item"><a href="#"><i class="mdi mdi-home-outline"></i></a></li>
								<li class="breadcrumb-item" aria-current="page"><?=$page_title?></li>
								<li class="breadcrumb-item active" aria-current="page"><?=$page_title?></li>
							</ol>
						</nav>
					</div>
				</div>
				
			</div>
		</div>

		<!-- Main content -->
		<section class="content">
		  <div class="row">
       <div class="col-md-6 col-lg-6">
			   <div class="box"> 
            <div class="box-header with-border">
                <h3 class="box-title">All <?=$page_title?></h3>
            </div>
          </div>
        </div>
        <div class="col-md-6 col-lg-6 ">
          <div class="box "> 
            <div class="box-header with-border">
              <a href="#" class="btn btn-primary btn-sm float-right" data-toggle="modal" data-target="#addCategorytModal" data-whatever="@mdo">Add <?=$page_title?> <i class="fa fa-plus"></i></a>
            </div>
          </div>
				  <!-- <h6 class="box-subtitle">Export data to Copy, CSV, Excel, PDF & Print</h6> -->
				</div>
				<div class="box-body">
					<div class="table-responsive">
					  <table id="example" class="table table-bordered table-hover display nowrap margin-top-10 w-p100">
						<thead>
							<tr>
                <th>SNO</th>
                <th>User</th>
                <th>Category</th>
								<th>Created Date</th>
                <th>Status</th>
                <th>Action</th>
							</tr>
						</thead>
						<tbody>
            				<?php 
                       if($categories){
                       foreach($categories as $key=>$category){
                    ?>
							<tr>
								<td><?=$key+1;?></td>
                <td><?= $category->name?></td>
								<td><?= $category->category?></td>
                <td><?= date('d-m-Y',strtotime($category->created_at));?></td>
                <td><?= $category->status == 1 ? '<span class="text-success">Active</span>' : '<span class="text-danger" onclick="stateActive(this.value)">De-Active</span>'?></td>
                <td>
                  <a href="#" onclick="editModalShow('<?=base64_encode($category->id)?>')" class="btn btn-warning btn-sm" data-toggle="tooltip" title="Edit Subject"><i class="fa fa-edit"></i></a>
                  <?php if($category->status == 1){?>
                  <a href="#" onclick="deleteCategory('<?=base64_encode($category->id)?>',<?= $category->status?>)" class="btn btn-danger btn-sm" data-toggle="tooltip" title="Delete Subject"><i class="fa fa-trash"></i></a>
                  <?php } if($category->status == 0){?>
                  <a href="#" onclick="deleteCategory('<?=base64_encode($category->id)?>',<?= $category->status?>)" class="btn btn-success btn-sm" data-toggle="tooltip" title="Active Subject"><i class="fa fa-check"></i></a>
                <?php } ?>
                </td>
							</tr>
							<?php } } ?>
				
						</tbody>				  
					
					</table>
					</div>              
				</div>
				<!-- /.box-body -->
			  </div>
			  </div>
			  <!-- /.box -->          
			</div>
			<!-- /.col -->
		  </div>
		  <!-- /.row -->
		</section>
		<!-- /.content -->
	  
	  </div>
  </div>
  <!-- /.content-wrapper -->

  <!-- Add Schoo Modal Start -->
  <div class="modal fade" id="addCategorytModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Add <?=$page_title?></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="<?=base_url('admin/category/store')?>" id="addCategory" method="POST" enctype="multipart/form-data">
      <div class="modal-body">
          <div class="form-group">
            <label for="name" class="col-form-label">Category:</label>
            <input type="text" class="form-control" name="category" id="category">
           </div>
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" name="submit" class="btn btn-primary">Submit</button>
      </div>
      </form>
    </div>
  </div>
</div>
  <!-- Add School Modal End -->


  <!-- Edit Schoo Modal Start -->
  <div class="modal fade" id="editCategoryModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Edit <?=$page_title?></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="<?=base_url('admin/category/update')?>" id="editSubject" method="POST" enctype="multipart/form-data">
      <div class="modal-body" id="editFormData">
  
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" name="submit" class="btn btn-primary">Edit</button>
      </div>
      </form>
    </div>
  </div>
</div>
  <!-- Edit School Modal End -->

  
  <script type="text/javascript">
  function editModalShow(id){
    $.ajax({
       url: '<?=base_url('admin/category/editForm')?>',
       type: 'POST',
       data: {id},
       success: function (data) {
        $('#editCategoryModal').modal('show');
         $('#editFormData').html(data);
       }
     });
  }

  $("form#addCategory").submit(function(e) {
     $(':input[type="submit"]').prop('disabled', true);
     e.preventDefault();    
     var formData = new FormData(this);
     $.ajax({
       url: $(this).attr('action'),
       type: 'POST',
       data: formData,
       cache: false,
       contentType: false,
       processData: false,
       dataType: 'json',
       success: function (data) {
         if(data.status==200) {
           toastr.success(data.message);
  				setTimeout(function(){
            location.reload();
           }, 1000) 
  
         }else if(data.status==403) {
           toastr.error(data.message);
           $(':input[type="submit"]').prop('disabled', false);
         }else{
           toastr.error('Unable to add Category');
           $(':input[type="submit"]').prop('disabled', false);
         }
       },
       error: function(){} 
     });
   });

   $("form#editCategory").submit(function(e) {
     $(':input[type="submit"]').prop('disabled', true);
     e.preventDefault();    
     var formData = new FormData(this);
     $.ajax({
       url: $(this).attr('action'),
       type: 'POST',
       data: formData,
       cache: false,
       contentType: false,
       processData: false,
       dataType: 'json',
       success: function (data) {
         if(data.status==200) {
           toastr.success(data.message);
  				setTimeout(function(){
                      location.reload();
           }, 1000) 
  
         }else if(data.status==403) {
           toastr.error(data.message);
           $(':input[type="submit"]').prop('disabled', false);
         }else{
           toastr.error('Unable to edit category');
           $(':input[type="submit"]').prop('disabled', false);
         }
       },
       error: function(){} 
     });
   });


   function deleteCategory(id,statusType){
    if(statusType==1){
     var status = 0;
     var messageText  = "You want delete this category!";
     var confirmText =  'Yes, delete it!';
     var message  ="Category delete Successfully!";
    
   }
   if(statusType==0){
    var status = 1;
    var messageText  = "You want active this category!";
     var confirmText =  'Yes, active it!';
     var message  ="Category active Successfully!";
   }
    Swal.fire({
        title: 'Are you sure?',
        text: messageText,
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: confirmText
        }).then((result) => {
        if (result.isConfirmed) {
            $.ajax({
                url: '<?=base_url('admin/category/delete')?>', 
                method: 'POST',
                data: {id,status},
                success: function(result){
                  if(result==1){
                    toastr.success(message);
                    setTimeout(function(){
                      window.location.reload();
                    }, 2000);
                    }else{
                      toastr.error("Status not change");
                    }
                 }
          });
          
          }
        })
  }
  
</script>
  