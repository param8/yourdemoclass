<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <div class="container-full">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="d-flex align-items-center">
        <div class="mr-auto">
          <h3 class="page-title"><i class="fa fa-university"> <?=$page_title?></i></h3>
          <div class="d-inline-block align-items-center">
            <nav>
              <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="#"><i class="mdi mdi-home-outline"></i></a></li>
                <li class="breadcrumb-item" aria-current="page"><?=$page_title?></li>
                <li class="breadcrumb-item active" aria-current="page"><?=$page_title?></li>
              </ol>
            </nav>
          </div>
        </div>
      </div>
    </div>
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-12 col-lg-12">
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title"><?=$page_title?></h3>
            </div>
          </div>
        </div>
        </div>
        <!-- /.box-header -->
        <div class="">
          <form action="<?=base_url('admin/Courses/avg_form')?>" id="averageForm" method="POST" enctype="multipart/form-data">
            <div class="modal-body">
              <div class="form-group row">
             
                <div class="row col-md-12">
                
                <div class="col-md-6"  >
                      <label for="course" class="col-form-label">Course:</label>
                      <input type="text" name="course" id="course" placeholder="Enter Institute Name" class="form-control">
                    </div>
                    <div class="col-md-6"  >
                      <label for="institute" class="col-form-label">Institute:</label>
                      <input type="text" name="institute" id="institute" placeholder="Enter Institute Name" class="form-control">
                    </div>
                    <div class="col-md-6">
                      <label for="city" class="col-form-label">City:</label>
                      <input type="text" name="city" id="city" placeholder="Enter City Name" class="form-control">
                    </div>
               
                    <div class="col-md-6">
                    <label for="course_price" class="col-form-label">Course Price:</label>
                    <input type="text" class="form-control" name="course_price" id="course_price" placeholder="Enter Course Price">
                </div>

                <div class="col-md-6">
                    <label for="course_price" class="col-form-label">Mode:</label>
                    <select class="form-control" name="mode" id="mode">
                      <option value="">Select mode</option>
                      <option value="online"> Online</option>
                      <option value="offline"> Offline</option>
                    </select>
                </div>
                </div>
              </div>
            </div>
            <div class="modal-footer">
              <button type="submit" name="submit" class="btn btn-primary">Submit</button>
            </div>
          </form>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->          
  </div>
  <!-- /.col -->
</div>
<!-- /.row -->
</section>
<!-- /.content -->
</div>
</div>
<!-- /.content-wrapper -->
<script type="text/javascript">
  $("form#averageForm").submit(function(e) {
     $(':input[type="submit"]').prop('disabled', true);
     e.preventDefault();    
     var formData = new FormData(this);
     $.ajax({
       url: $(this).attr('action'),
       type: 'POST',
       data: formData,
       cache: false,
       contentType: false,
       processData: false,
       dataType: 'json',
       success: function (data) {
         if(data.status==200) {
           toastr.success(data.message);
  				setTimeout(function(){
                      location.reload();
           }, 1000) 
  
         }else if(data.status==403) {
           toastr.error(data.message);
           $(':input[type="submit"]').prop('disabled', false);
         }else{
           toastr.error('Unable to add site info');
           $(':input[type="submit"]').prop('disabled', false);
         }
       },
       error: function(){} 
     });
   });
</script>
<script>
  function getInstitutes(course){
    if ($('#course').val()=='OtherCourse') {    
    $("#other").show();
    $("#otherInstitut").hide();
    $("#otherCity").hide();
      } else{
        $("#other").hide();
        $("#otherInstitut").show();
        $("#otherCity").show();
      }
    $.ajax({
       url: '<?=base_url("Ajax_controller/getInstitutes")?>',
       type: 'POST',
       data: {course},  
       success: function (data) {
        if(data!=""){
        $('#institute').html(data);
        }
        else{
        $('#institute').hide();
        }
       },
     });
  }

  function getCity(stateID){
        
      $.ajax({
      url: '<?=base_url('Ajax_controller/get_city')?>',
      type: 'POST',
      data: {stateID},
      success: function (data) {
        $('#city').html(data);
              $('.city').html(data);
      }
      });
    }

    function otherCourse() {
    if ($('#hpanel').val()=='other') {    
    $("#otherform").show();
      } else{
        $("#otherform").hide();
      }
  }
</script>