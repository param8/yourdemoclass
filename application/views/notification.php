


<div class="edu-breadcrumb-area">
  <div class="container">
    <div class="breadcrumb-inner">
        
      <div class="page-title">
        <h1 class="title"><?=$page_title?></h1>
      </div>
      <ul class="edu-breadcrumb">
        <li class="breadcrumb-item"><a href="<?=base_url('home')?>">Home</a></li>
        <li class="separator"><i class="icon-angle-right"></i></li>
        <li class="breadcrumb-item"><a href="#"><?=$page_title?></a></li>
      </ul>
    </div>
  </div>
  <ul class="shape-group">
    <li class="shape-1">
      <span></span>
    </li>
    <li class="shape-2 scene"><img data-depth="2" src="<?=base_url('public/website/assets/images/about/shape-13.png')?>" alt="shape"></li>
    <li class="shape-3 scene"><img data-depth="-2" src="<?=base_url('public/website/assets/images/about/shape-15.png')?>" alt="shape"></li>
    <li class="shape-4">
      <span></span>
    </li>
    <li class="shape-5 scene"><img data-depth="2" src="<?=base_url('public/website/assets/images/about/shape-07.png')?>" alt="shape"></li>
  </ul>
</div>
<!--=====================================-->
<!--=           Cart Area Start         =-->
<!--=====================================-->
<section class="cart-page-area edu-section-gap">
  <div class="container">
    <div class="table-responsive">
    <table id="notificationDataTable" class="table  noti_f table-striped table-bordered" style="width:100%">
        <thead>
            <tr>
                <th>Sno.</th>
                <th>Message</th>
                <th>Send Date</th>
               
            </tr>
        </thead>
       <tbody>

        </tbody>
    </table>
    </div>

    
  </div>
</section>