<div class="edu-breadcrumb-area">
            <div class="container">
                <div class="breadcrumb-inner">
                    <div class="page-title">
                        <h1 class="title"><?=$page_title?></h1>
                    </div>
                    <ul class="edu-breadcrumb">
                        <li class="breadcrumb-item"><a href="<?=base_url('home')?>">Home</a></li>
                        <li class="separator"><i class="icon-angle-right"></i></li>
                        <li class="breadcrumb-item active" aria-current="page"><?=$page_title?></li>
                    </ul>
                </div>
            </div>
            <ul class="shape-group">
                <li class="shape-1">
                    <span></span>
                </li>
                <li class="shape-2 scene"><img data-depth="2" src="<?=base_url('public/website/assets/images/about/shape-13.png')?>" alt="shape"></li>
                <li class="shape-3 scene"><img data-depth="-2" src="<?=base_url('public/website/assets/images/about/shape-15.png')?>" alt="shape"></li>
                <li class="shape-4">
                    <span></span>
                </li>
                <li class="shape-5 scene"><img data-depth="2" src="<?=base_url('public/website/assets/images/about/shape-07.png')?>" alt="shape"></li>
            </ul>
        </div>

        <!--=====================================-->
        <!--=       Contact Me Area Start       =-->
        <!--=====================================-->
        <section class="contact-us-area">
            <div class="container">
                <div class="row g-5">
                    <div class="col-xl-4 col-lg-6">
                        <div class="contact-us-info">
                            <h3 class="heading-title">We're Always Eager to Hear From You!</h3>
                            <ul class="address-list">
                                <li>
                                    <h5 class="title">Address</h5>
                                    <p><?=$siteinfo->site_address?></p>
                                </li>
                                <li>
                                    <h5 class="title">Email</h5>
                                    <p><a href="mailto:<?=$siteinfo->site_email?>"><?=$siteinfo->site_email?></a></p>
                                </li>
                                <li>
                                    <h5 class="title">Phone</h5>
                                    <p><a href="tel:<?=$siteinfo->site_contact?>">(+91) <?=$siteinfo->site_contact?></a></p>
                                </li>
                            </ul>
                            <ul class="social-share">
                                <li><a href="#"><i class="icon-share-alt"></i></a></li>
                                <li><a href="<?=!empty($siteinfo->facebook_url) ? $siteinfo->facebook_url : '#';?>"><i class="icon-facebook"></i></a></li>
                                <li><a href="<?=!empty($siteinfo->twitter_url) ? $siteinfo->twitter_url : '#';?>"><i class="icon-twitter"></i></a></li>
                                <li><a href="<?=!empty($siteinfo->linkedin_url) ? $siteinfo->linkedin_url : '#';?>"><i class="icon-linkedin2"></i></a></li>
                                <li><a href="<?=!empty($siteinfo->insta_url) ? $siteinfo->insta_url : '#';?>"><i class="icon-instagram"></i></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="offset-xl-2 col-lg-6">
                        <div class="contact-form form-style-2">
                            <div class="section-title">
                                <h4 class="title">Get In Touch</h4>
                                <p>Fill out this form for booking a consultant advising session.</p>
                            </div>
                            <form class="rnt-contact-form " id="enquirySend"  action="<?=base_url('home/user_enquiry')?>">
                                <div class="row row--10">
                                    <div class="form-group col-12">
                                        <input type="text" name="name" id="name" placeholder="Your name">
                                    </div>
                                    <div class="form-group col-12">
                                        <input type="email" name="email" id="email" placeholder="Enter your email">
                                    </div>
                                    <div class="form-group col-12">
                                        <input type="tel" name="phone" id="phone" placeholder="Phone number" maxlength="10" oninput="this.value = this.value.replace(/[^0-9]/g, '').replace(/(\..*)\./g, '$1');">
                                    </div>
                                    <div class="form-group col-12">
                                        <textarea name="message" id="message" cols="30" rows="4" placeholder="Your message"></textarea>
                                    </div>
                                    <div class="form-group col-12">
                                        <button class="edu-btn btn-medium submit-btn"  type="submit">Submit Message <i class="icon-4"></i></button>
                                    </div>
                                </div>
                            </form>
                            <ul class="shape-group">
                                <li class="shape-1 scene"><img data-depth="1" src="<?=base_url('public/website/assets/images/about/shape-13.png')?>" alt="Shape"></li>
                                <li class="shape-2 scene"><img data-depth="-1" src="<?=base_url('public/website/assets/images/counterup/shape-02.png')?>" alt="Shape"></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--=====================================-->
        <!--=      Google Map Area Start        =-->
        <!--=====================================-->
        <!--<div class="google-map-area">-->
        <!--    <div class="mapouter">-->
        <!--        <div class="gmap_canvas">-->
        <!--            <iframe id="gmap_canvas" src="https://maps.google.com/maps?q=noida,%20India&amp;t=&amp;z=15&amp;ie=UTF8&amp;iwloc=&amp;output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe>-->
        <!--        </div>-->
        <!--    </div>-->
        <!--</div>-->

    <script>
         $("form#enquirySend").submit(function(e) {
			$(':input[type="submit"]').prop('disabled', true);
			e.preventDefault();    
			var formData = new FormData(this);
			$.ajax({
			url: $(this).attr('action'),
			type: 'POST',
			data: formData,
			cache: false,
			contentType: false,
			processData: false,
			dataType: 'json',
			success: function (data) {
				if(data.status==200) {
			      Swal.fire({
                  icon: 'success',
                  title: 'Success',
                  text: data.message,
                  //footer: '<a href="">Why do I have this issue?</a>'
                })
                $(':input[type="submit"]').prop('disabled', false);
                    setTimeout(function(){
                      location.reload();
                }, 1000) 
		
				}else if(data.status==403) {

			      Swal.fire({
                  icon: 'error',
                  title: 'Oops...',
                  text: data.message,
                  //footer: '<a href="">Why do I have this issue?</a>'
                })
                $(':input[type="submit"]').prop('disabled', false);
				}else{
			      Swal.fire({
                  icon: 'error',
                  title: 'Oops...',
                  text: data.message,
                  //footer: '<a href="">Why do I have this issue?</a>'
                })
                $(':input[type="submit"]').prop('disabled', false);
				}
			},
			error: function(){} 
			});
		});

        </script>