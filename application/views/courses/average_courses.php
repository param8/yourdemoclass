<?php $usretype=$this->session->userdata('user_type');
  $userid=$this->session->userdata('id');?>
   <?php if($this->session->userdata('user_type')=='Student' ){ ?>
<div class="banner-search p-2 mt-5">
<div class="container">
  <div class="row justify-content-center">
  <div class="col-md-12">
  <div class="card en_query_c">
    <form action="<?=base_url('Courses/course_form')?>" id="courseForm">
      <div class="col-md-12">
        <div class="enqueri">
        <div class="inp_ut" id="course_div">
            <input class="form-control" type="text" name="name" id="name" placeholder="Name">
          </div>
          <div class="inp_ut" id="course_div">
            <input class="form-control" type="text" name="course_name" id="course_name" placeholder="Course Name">
          </div>
          <div class="inp_ut" id="institute_div">
            <input class="form-control" type="text"  name="institute_name" id="institute_name" placeholder="Institute Name">
          </div>
          <div class="inp_ut" id="city_div">
            <input class="form-control" type="text" name="institute_city"  placeholder="City">
          </div>
          <div class="inp_ut" id="classmode_div">
            <select name="classemode"  placeholder="Select Class Mode">
              <option value=""> Select Mode</option>
              <option value="online"> Online</option>
              <option value="offline"> Offline</option>
            </select>
          </div>
          <div class="inp_ut">
            <input type="text" class="form-control" name="course_price" id="course_price" placeholder="Course Price">
          </div>
          <div class="input-group">
            <button class="rn-btn edu-btn btn-medium submit-btn" name="submit" type="submit">Submit <i class="icon-4"></i></button>
          </div>
        </div>
      </div>
    </form>
  </div>
  </div>
  </div>
</div>
   </div>
<?php } ?>
<!-- End Blog Grid  -->
<div class="container">
  <div class="row">
    <div class="col-md-12">
      <div class="course mt-5 mb-5 card">
        <div class="sear_ch text-right pt-3 pr-3">
          <input type="button " placeholder="Search..." name="avg_serach" id="avg_serach" onkeyup="get_average(this.value)" class="searchbar">
        </div>
        <!-- start -->
        <div id="getAvgData">
        
      </div>
        <!-- end -->
      </div>
    </div>
  </div>
</div>
<script>

 function get_average(value){
    $.ajax({
      url: '<?=base_url('Courses/get_avgCourses')?>',
      type: 'POST',
      data: {value},
      dataType:'html',
        success: function (responce) {
          $('#getAvgData').html(responce);
        }
   });
 }
  

  $(document ).ready(function() {
    get_average(value=null);
  });
  
             $("form#courseForm").submit(function(e) {
              //alert('fgfgfgfd');
        $(':input[type="submit"]').prop('disabled', true);
        e.preventDefault();    
        var formData = new FormData(this);
        $.ajax({
        url: $(this).attr('action'),
        type: 'POST',
        data: formData,
        cache: false,
        contentType: false,
        processData: false,
        dataType: 'json',
        success: function (data) {
          if(data.status==200) {
          //$('.modal').modal('hide');
          toastr.success(data.message);
          $(':input[type="submit"]').prop('disabled', false);
              setTimeout(function(){
                location.reload();
          }, 1000) 
      
          }else if(data.status==403) {
          toastr.error(data.message);
          $(':input[type="submit"]').prop('disabled', false);
          }else{
          toastr.error('Something went wrong');
          $(':input[type="submit"]').prop('disabled', false);
          }
        },
        error: function(){} 
        });
      });
                 
</script>