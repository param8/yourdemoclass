<?php  
  $countFive = 0;
  $countFoure = 0;
  $countThree = 0;
  $countTwo = 0;
  $countOne = 0;
  $totalCountFive = 0;
  $totalCountFoure = 0;
  $totalCountThree = 0;
  $totalCountTwo = 0;
  $totalCountOne = 0;
  $percentageFive = 0;
  $percentageFoure = 0;
  $percentageThree = 0;
  $percentageTwo = 0;
  $percentageOne = 0;
  if(count($ratings_five) > 0){
   $countFive = count($ratings_five);
   $percentageFive = ($countFive * 100)/count($ratings);
   $totalCountFive = $countFive*5;
  }
  if(count($ratings_four) > 0){
   $countFoure = count($ratings_four);
   $percentageFoure = ($countFoure * 100)/count($ratings);
   $totalCountFoure = $countFoure*4;
  }
  if(count($ratings_three) > 0){
   $countThree = count($ratings_three);
   $percentageThree = ($countThree * 100)/count($ratings);
   $totalCountThree = $countThree*3;
  }
  if(count($ratings_two) > 0){
   $countTwo = count($ratings_two);
   $percentageTwo = ($countTwo * 100)/count($ratings);
   $totalCountTwo = $countTwo*2;
  }
  if(count($ratings_one) > 0){
   $countOne = count($ratings_one);
   $percentageOne = ($countOne * 100)/count($ratings);
   $totalCountOne = $countOne*1;
  }
  
  $sumTotalCount = $totalCountFive + $totalCountFoure + $totalCountThree + $totalCountTwo + $totalCountOne;
  $sumCount = $countFive + $countFoure + $countThree + $countTwo + $countOne;
  
  if($sumTotalCount > 0 && $sumCount > 0){
    $averageRating = $sumTotalCount/$sumCount;
  }else{
    $averageRating = 0;
  }
  $Totalrating = $averageRating;
  
  $RoundedRating = $Totalrating;
  
  
    
    $stars="";
    $newwhole = floor($RoundedRating);
    $fraction = $RoundedRating - $newwhole;
    if($newwhole > 0){
    for($s=1;$s<=$newwhole;$s++){
      $stars .= '<i class="icon-23"></i>';	
    }
    if($fraction >= 0.25){
      $stars .= '<i class="icon-23 n50"></i>';	
    }
    for($l=1;$l<=5-$newwhole;$l++){
      $stars .= '<i class="icon-23 text-secondary"></i>';
     }
    }else{
      for($s=1;$s<=5;$s++){
       $stars .= '<i class="icon-23 text-secondary"></i>';
      }
    }
    
    $student_booked_course = array();
    foreach($booked_courses as $bookedCourse){
       $student_booked_course[] = $bookedCourse->studentID; 
    }
    $booked_student_course = array();
    foreach($booked as $bookedStudent){
        $booked_student_course[] = $bookedStudent->courseID; 
    }
    
    //print_r($booked_student_course);die;
  ?>
<div class="edu-breadcrumb-area breadcrumb-style-3">
  <div class="container">
    <div class="breadcrumb-inner">
      <ul class="edu-breadcrumb">
        <li class="breadcrumb-item"><a href="<?=base_url('home')?>">Home</a></li>
        <li class="separator"><i class="icon-angle-right"></i></li>
        <li class="breadcrumb-item active" aria-current="page"><?=$page_title?></li>
      </ul>
      <div class="page-title">
        <h1 class="title"><?=$course->course?></h1>
      </div>
      <ul class="course-meta c_detail">
        <li><i class="icon-58"></i>Total <?=count($booked_courses) > 0 ? count($booked_courses) : 0?> Enrolled Student</li>
        <li><i class="icon-59"></i><?=$course->category?></li>
        <li class="course-rating">
          <div class="rating">
            <!-- <i class="icon-23"></i>
              <i class="icon-23"></i>
              <i class="icon-23"></i>
              <i class="icon-23"></i>
              <i class="icon-23 n50"></i> -->
            <?=$stars?>
          </div>
          <span class="rating-count">(<?=$Totalrating?> Rating)</span>
        </li>
      </ul>
    </div>
  </div>
  <ul class="shape-group">
    <li class="shape-1">
      <span></span>
    </li>
    <li class="shape-2 scene"><img data-depth="2" src="<?=base_url('public/website/assets/images/about/shape-13.png')?>" alt="shape"></li>
    <li class="shape-3 scene"><img data-depth="-2" src="<?=base_url('public/website/assets/images/about/shape-15.png')?>" alt="shape"></li>
    <li class="shape-4">
      <span></span>
    </li>
    <li class="shape-5 scene"><img data-depth="2" src="<?=base_url('public/website/assets/images/about/shape-07.png')?>" alt="shape"></li>
  </ul>
</div>
<!--=====================================-->
<!--=     Courses Details Area Start    =-->
<!--=====================================-->
<?php 
  $earlier = new DateTime($course->start_date);
  $later = new DateTime($course->end_date);
  $totalDays =  $later->diff($earlier)->format("%a");
  if($course->fees > 0 && $course->discount > 0){
      $price =$course->fees - ($course->discount*$course->fees)/100;
      $discount =($course->discount*$course->fees)/100;
  }else{
      $price =$course->fees;
      $discount = 0;
      
  }
  ?>
<section class="edu-section-gap course-details-area">
  <div class="container">
    <div class="row row--30">
      <div class="col-lg-8">
        <div class="course-details-content">
          <ul class="nav nav-tabs" id="myTab" role="tablist">
            <li class="nav-item" role="presentation">
              <button class="nav-link active" id="overview-tab" data-bs-toggle="tab" data-bs-target="#overview" type="button" role="tab" aria-controls="overview" aria-selected="true">Overview</button>
            </li>
            <!-- <li class="nav-item" role="presentation">
              <button class="nav-link" id="carriculam-tab" data-bs-toggle="tab" data-bs-target="#carriculam" type="button" role="tab" aria-controls="carriculam" aria-selected="false">Carriculam</button>
              </li> -->
            <!-- <li class="nav-item" role="presentation">
              <button class="nav-link" id="instructor-tab" data-bs-toggle="tab" data-bs-target="#instructor" type="button" role="tab" aria-controls="instructor" aria-selected="false">Instructor</button>
              </li> -->
            <li class="nav-item" role="presentation">
              <button class="nav-link" id="review-tab" data-bs-toggle="tab" data-bs-target="#review" type="button" role="tab" aria-controls="review" aria-selected="false">Reviews</button>
            </li>
          </ul>
          <div class="tab-content" id="myTabContent">
            <div class="tab-pane fade show active" id="overview" role="tabpanel" aria-labelledby="overview-tab">
              <div class="course-tab-content">
                <div class="course-overview">
                  <h3 class="heading-title">Course Description</h3>
                  <p><?=$course->description?></p>
                </div>
              </div>
            </div>
            <div class="tab-pane fade" id="review" role="tabpanel" aria-labelledby="review-tab">
              <div class="course-tab-content">
                <div class="course-review">
                  <h3 class="heading-title">Course Rating</h3>
                  <p><?=$averageRating?> average rating based on <?=count($ratings)?> rating</p>
                  <div class="row g-0 align-items-center">
                    <div class="col-sm-4">
                      <div class="rating-box">
                        <div class="rating-number"><?=$averageRating?></div>
                        <div class="rating">
                          <?=$stars?>
                          <!-- <i class="icon-23"></i>
                            <i class="icon-23"></i>
                            <i class="icon-23"></i>
                            <i class="icon-23"></i>
                            <i class="icon-23"></i> -->
                        </div>
                        <span>(<?=count($ratings)?> Review)</span>
                      </div>
                    </div>
                    <div class="col-sm-8">
                      <div class="review-wrapper">
                        <?php 
                          for($k=1; $k<=5; $k++){
                          
                          ?>
                        <div class="single-progress-bar">
                          <div class="rating-text">
                            <?=$k?> <i class="icon-23"></i>
                          </div>
                          <div class="progress">
                            <div class="progress-bar" role="progressbar" style="width: <?= $k==5 ? $percentageFive : ($k==4 ? $percentageFoure : ($k==3 ? $percentageThree: ($k==2 ? $percentageTwo : $percentageOne ) ));?>%" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
                          </div>
                          <span class="rating-value"><?= $k==5 ? $countFive : ($k==4 ? $countFoure : ($k==3 ? $countThree: ($k==2 ? $countTwo : $countOne ) ));?></span>
                        </div>
                        <?php } ?>
                      </div>
                    </div>
                  </div>
                  <!-- Start Comment Area  -->
                  <div class="comment-area">
                    <h3 class="heading-title">Reviews</h3>
                    <div class="comment-list-wrapper">
                      <!-- Start Single Comment  -->
                      <?php 
                        if(count($ratings)>0){
                         foreach($ratings as $rating){
                        ?>
                      <div class="comment">
                        <div class="thumbnail">
                          <img src="<?=base_url($rating->profile_pic)?>" alt="Comment Images">
                        </div>
                        <div class="comment-content">
                          <div class="rating">
                            <?php 
                              $userRating = array();
                              for($i=1; $i<=$rating->rating; $i++){
                                $userRating[] = $i;
                              }
                              for($j=1; $j<=5; $j++){
                                if(in_array($j,$userRating)){
                              ?>
                            <i class="icon-23"></i>
                            <?php }else{
                              ?>
                            <i class="icon-23 text-secondary"></i>
                            <?php
                              } } ?>
                          </div>
                          <h5 class="title"><?=$rating->name?></h5>
                          <span class="date"><?=date('M d, Y',strtotime($rating->created_at))?></span>
                          <p><?=$rating->description?></p>
                        </div>
                      </div>
                      <?php } } ?>
                      <!-- End Single Comment  -->
                    </div>
                  </div>
                  <!-- End Comment Area  -->
                  <?php if($this->session->userdata('user_type')!='Institute'){?>
                  <div class="comment-form-area">
                    <h3 class="heading-title">Write a Review</h3>
                    <form class="comment-form" id="addRating" action="<?=base_url('Courses/store_rating')?>" method="POST">
                      <div class="rating-icon">
                        <h6 class="title">Rating Here</h6>
                        <div class="rating">
                          <?php 
                            $rating_stars = array(1,2,3,4,5) ;
                             foreach($rating_stars as $stars){
                            ?>
                          <input type="radio" class="rating-width" name="rating_course" id="rating_course<?=$stars?>" value="<?=$stars?>" onclick="checkedStar(<?=$stars?>)"><i class="icon-23 rating-color<?=$stars?>"></i>
                          <?php } ?>
                        </div>
                      </div>
                      <div class="row g-5">
                        <!-- <div class="form-group col-lg-6">
                          <input type="text" name="title" id="title" placeholder="Review Title">
                          </div> -->
                        <div class="form-group col-12">
                          <textarea name="description" id="description" cols="30" rows="5" placeholder="Review summary"></textarea>
                        </div>
                        <?php  if(count($dublicateRatings)<=0){?>
                        <div class="form-group col-12">
                          <?=$this->session->userdata('email') ? '<button  type="submit" class="edu-btn submit-btn">Submit Review <i class="icon-4"></i></button>' : '<a  href="'.base_url('my-account').'" class="edu-btn submit-btn">Submit Review <i class="icon-4"></i></a>'; ?>
                        </div>
                        <?php } ?>
                      </div>
                    </form>
                  </div>
                  <?php } ?>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-lg-4">
        <div class="course-sidebar-3 sidebar-top-position">
          <div class="edu-course-widget widget-course-summery">
            <div class="inner">
              <div class="thumbnail">
                <img src="<?=base_url($course->image)?>" width="340" height="250" alt="Courses">
                <!-- <a href="https://www.youtube.com/watch?v=PICj5tr9hcc" class="play-btn video-popup-activation"><i class="icon-18"></i></a> -->
              </div>
              <div class="content">
                <h4 class="widget-title">Course Includes:</h4>
                <ul class="course-item">
                    <?php if($price>0){?>
                  <li>
                    <span class="label"><i class="icon-60"></i>MRP:</span>
                    <span class="value ">₹ <?=$course->fees?></span>
                  </li>
                  <li>
                    <span class="label"><i class="icon-60"></i>Discount:</span>
                    <span class="value ">₹ <?=$discount?></span>
                  </li>
                  <li>
                    <span class="label"><i class="icon-60"></i>Price:</span>
                    <span class="value price">₹ <?=$price?></span>
                  </li>
                  <?php }else{?>
                    <li>
                    <span class="label"><i class="icon-60"></i>Price:</span>
                    <span class="value price">Free</span>
                  </li>
                  <?php } ?>
                  <li>
                      
                    <span class="label"><i class="icon-62"></i>Instrutor:</span>
                    <span class="value"><a href="<?=base_url('user-profile/'.base64_encode($course->userID).'/'.base64_encode($course->userType))?>" title="Instrutor Profile"><?=$course->user_name?></a></span>
                  </li>
                  <li>
                    <span class="label"><i class="icon-61"></i>Duration:</span>
                    <span class="value"><?=$totalDays?> Days</span>
                  </li>
                  <!-- <li>
                    <span class="label">
                        <img class="svgInject" src="assets/images/svg-icons/books.svg" alt="book icon">
                        Lessons:</span>
                    <span class="value">8</span>
                    </li> -->
                  <li>
                    <span class="label"><i class="icon-63"></i>Total Seats:</span>
                    <span class="value"><?=$course->seats?></span>
                  </li>
                  <li>
                    <span class="label"><i class="icon-63"></i>Remaning Seats:</span>
                    <span class="value"><?=$course->remaning_seats?></span>
                  </li>
                  <li>
                    <span class="label"><i class="icon-59"></i>Category:</span>
                    <span class="value"><?=$course->category?></span>
                  </li>
                  <li>
                    <span class="label"><i class="icon-64"></i>Certificate:</span>
                    <span class="value">Yes</span>
                  </li>
                  <li>
                    <span class="label"><i class="icon-13"></i>Batch:</span>
                    <span class="value"><input type="text" id="batch" name="batch" value="<?=$course->batch?>" readonly>
                      <?php
                        //$batchs =  explode(',',$course->batch); 
                        
                      //   $this->db->where_in('id',explode(',',$course->batch));
                      //   $batchs = $this->db->get('batch')->result();
                      //    //echo $this->db->last_query();
                         ?>
                      <!-- // <select id="batch"  name="batch">
                      //   <option value="">Select Batch</option>
                      //   <?php //foreach($batchs as $batch){?>
                      //   <option value="<?//=$batch->id?>"><?//=$batch->batch.' '.$batch->time_formate ?></option>
                      //   <?php //} ?>
                      // </select> -->
                    </span>
                  </li>

                </ul>
                <?php if($this->session->userdata('user_type')=='Student'){
                  if(in_array($this->session->userdata('id'),$student_booked_course)){
                    ?>
                    <div class="read-more-btn">
                         <a href="javascript:void(0)"  class="edu-btn btn-danger">Alraedy Booked <i class="icon-3"></i></a>
                    </div>
                    <?php
                  }else{
                 ?>
                <div class="read-more-btn">
                  <?php if($course->remaning_seats<=0){?>
                  <a href="javascript:void(0)"  class="edu-btn btn-danger">Out of stock <i class="icon-3"></i></a>
                  <?php } else{?>
                  <a href="javascript:void(0)" onclick="addStudentCourse()" class="edu-btn">Book Now <i class="icon-4"></i></a>
                  <?php } ?>
                </div>
                <?php } } ?>
                <?php if(empty($this->session->userdata('user_type'))){?>
                <div class="read-more-btn">
                  <a href="javascript:void(0)" onclick="redirectSigin()"  class="edu-btn">Book Now <i class="icon-4"></i></a> 
                </div>
                <?php } ?>
                <!-- <div class="share-area">
                  <h4 class="title">Share On:</h4>
                  <ul class="social-share">
                      <li><a href="#"><i class="icon-facebook"></i></a></li>
                      <li><a href="#"><i class="icon-twitter"></i></a></li>
                      <li><a href="#"><i class="icon-linkedin2"></i></a></li>
                      <li><a href="#"><i class="icon-youtube"></i></a></li>
                  </ul>
                  </div> -->
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<!--=====================================-->
<!--=     More Courses Area Start    =-->
<!--=====================================-->
<!-- Start Course Area  -->
<div class="gap-bottom-equal">
  <div class="container">
    <div class="section-title section-left" data-sal-delay="150" data-sal="slide-up" data-sal-duration="800">
      <h3 class="title">Related Courses for You</h3>
    </div>
    <div class="row g-5">
      <!-- Start Single Course  -->
      <div class="owl-carousel owl-theme">
      <?php 
        foreach($courses as $moreCourse){
           $earlier_more = new DateTime($moreCourse->start_date);
           $later_more = new DateTime($moreCourse->end_date);
           $totalDays_more =  $later_more->diff($earlier_more)->format("%a");
           if($moreCourse->fees > 0 && $moreCourse->discount > 0){
               $price_more =$moreCourse->fees - ($moreCourse->discount*$moreCourse->fees)/100;
           }else{
               $price_more =$moreCourse->fees;
           }
        
           $this->db->where(array('course_rating.courseID' => $moreCourse->id,'course_rating.rating'=>5));
           $five =  $this->db->get('course_rating')->result();
        
           $this->db->where(array('course_rating.courseID' => $moreCourse->id,'course_rating.rating'=>4));
           $four =  $this->db->get('course_rating')->result();
        
           $this->db->where(array('course_rating.courseID' => $moreCourse->id,'course_rating.rating'=>3));
           $three =  $this->db->get('course_rating')->result();
        
           $this->db->where(array('course_rating.courseID' => $moreCourse->id,'course_rating.rating'=>2));
           $two =  $this->db->get('course_rating')->result();
        
           
           $this->db->where(array('course_rating.courseID' => $moreCourse->id,'course_rating.rating'=>1));
           $one =  $this->db->get('course_rating')->result();
        
        
           $count_Five = 0;
           $count_Foure = 0;
           $count_Three = 0;
           $count_Two = 0;
           $count_One = 0;
           $total_CountFive = 0;
           $total_CountFoure = 0;
           $total_CountThree = 0;
           $total_CountTwo = 0;
           $total_CountOne = 0;
           if(count($five) > 0){
            $count_Five = count($five);
            $total_CountFive = $count_Five*5;
           }
           if(count($four) > 0){
            $count_Foure = count($four);
            $total_CountFoure = $count_Foure*4;
           }
           if(count($three) > 0){
            $count_Three = count($three);
            $total_CountThree = $count_Three*3;
           }
           if(count($two) > 0){
            $count_Two = count($two);
            $total_CountTwo = $count_Two*2;
           }
           if(count($one) > 0){
            $count_One = count($one);
            $total_CountOne = $count_One*1;
           }
           $sumTotalCountRating = $total_CountFive + $total_CountFoure + $total_CountThree + $total_CountTwo + $total_CountOne;
           $sumCountRating = $count_Five + $count_Foure + $count_Three + $count_Two + $count_One;
           if($sumTotalCountRating > 0 && $sumCountRating > 0){
           $TotalratingAvg = $sumTotalCountRating/$sumCountRating;
           }else{
             $TotalratingAvg = 0;
           }
             
             $stars_rating="";
             $newwholeRating = floor($TotalratingAvg);
             $fractionRating = $TotalratingAvg - $newwholeRating;
             if($newwholeRating > 0){
             for($s=1;$s<=$newwholeRating;$s++){
               $stars_rating .= '<i class="icon-23"></i>';	
             }
             if($fractionRating >= 0.25){
               $stars_rating .= '<i class="icon-23 n50"></i>';	
             }
           }
             else{
               for($s=1;$s<=5;$s++){
                $stars_rating .= '<i class="icon-23 text-secondary"></i>';
               }
             }
        ?>
      <div class="item" data-sal-delay="150" data-sal="slide-up" data-sal-duration="800">
        <div class="edu-course course-style-5 inline" data-tipped-options="inline: 'inline-tooltip-<?=$moreCourse->id?>'">
          <div class="inner">
            <div class="thumbnail">
              <a href="<?=base_url('course-details/'.base64_encode($moreCourse->id))?>">
              <img src="<?=base_url($moreCourse->image)?>" alt="Course Meta" style="height:269px;">
              </a>
            </div>
            <div class="content">
              <div class="course-price price-round"><?php if($price_more > 0) { ?>₹ <?=$price_more ?><?php }else{?>Free<?php }?></div>
              <h5 class="title">
                <a href="<?=base_url('course-details/'.base64_encode($moreCourse->id))?>"><?=$moreCourse->course?></a>
              </h5>
              <div class="course-rating">
                <div class="rating">
                  <!-- <i class="icon-23"></i>
                    <i class="icon-23"></i>
                    <i class="icon-23"></i>
                    <i class="icon-23"></i>
                    <i class="icon-23"></i> -->
                  <?=$stars_rating?>
                </div>
                <span class="rating-count">(<?=$TotalratingAvg?>)</span>
              </div>
              <p><?=substr($moreCourse->description,0,100)?></p>
              <ul class="course-meta">
                <li><i class="icon-24"></i><?=$moreCourse->seats?> Total Seats</li>
                <li class="text-danger"><i class="icon-24 text-danger"></i><?=$moreCourse->remaning_seats?> Remaning Seats</li>
              </ul>
            </div>
          </div>
        </div>
        <div id="inline-tooltip-<?=$moreCourse->id?>" style="display:none">
          <div class="course-layout-five-tooltip-content">
            <div class="content">
              <!-- <span class="course-level">Cooking</span> -->
              <h5 class="title">
                <a href="<?=base_url('user-profile/'.base64_encode($moreCourse->userID).'/'.base64_encode($moreCourse->userType))?>">
                  <p><strong>Institute Name :-</strong> <?=$moreCourse->user_name?></p>
                </a>
              </h5>
              <div class="course-rating">
                <div class="rating">
                  <!-- <i class="icon-23"></i>
                    <i class="icon-23"></i>
                    <i class="icon-23"></i>
                    <i class="icon-23"></i>
                    <i class="icon-23"></i> -->
                  <?=$stars_rating?>
                </div>
                <span class="rating-count">(<?=$TotalratingAvg?>)</span>
              </div>
              <ul class="course-meta">
                <!-- <li>15 Lessons</li>
                  <li>35 hrs</li>
                  <li>Beginner</li> -->
              </ul>
              <div class="course-feature">
                <p><?=substr($moreCourse->description,0,200)?></p>
              </div>
              <?php //if($this->session->userdata('user_type')=='Student'){?>
              <div class="button-group">
                <a href="<?=base_url('course-details/'.base64_encode($moreCourse->id))?>" class="edu-btn btn-secondary btn-small"><?= $this->session->userdata('user_type')=='Institute'? 'Course Detail' :($this->session->userdata('user_type')=='Experts'? 'Course Detail' : (in_array($moreCourse->id,$booked_student_course) ? '<span ><b>Already Booked</b></span>' : 'Book Now' )) ?> <i class="icon-4"></i></a>
              </div>
              <?php //} ?>
            </div>
          </div>
        </div>
      </div>
      <?php }?>
      <!-- End Single Course  -->
    </div>
  </div>
</div>
</div>

<!-- Institute Courses -->
<div class="gap-bottom-equal">
  <div class="container">
    <div class="section-title section-left" data-sal-delay="150" data-sal="slide-up" data-sal-duration="800">
      <h3 class="title">Institute Courses</h3>
    </div>
    <div class="row g-5">
        <div class="owl-carousel owl-theme">
      <!-- Start Single Course  -->
      <?php 
        foreach($instituteCourses as $instCourse){
           $earlier_more = new DateTime($instCourse->start_date);
           $later_more = new DateTime($instCourse->end_date);
           $totalDays_more =  $later_more->diff($earlier_more)->format("%a");
           if($instCourse->fees > 0 && $instCourse->discount > 0){
               $price_more =$instCourse->fees - ($instCourse->discount*$instCourse->fees)/100;
           }else{
               $price_more =$instCourse->fees;
           }
        
           $this->db->where(array('course_rating.courseID' => $instCourse->id,'course_rating.rating'=>5));
           $five =  $this->db->get('course_rating')->result();
        
           $this->db->where(array('course_rating.courseID' => $instCourse->id,'course_rating.rating'=>4));
           $four =  $this->db->get('course_rating')->result();
        
           $this->db->where(array('course_rating.courseID' => $instCourse->id,'course_rating.rating'=>3));
           $three =  $this->db->get('course_rating')->result();
        
           $this->db->where(array('course_rating.courseID' => $instCourse->id,'course_rating.rating'=>2));
           $two =  $this->db->get('course_rating')->result();
        
           
           $this->db->where(array('course_rating.courseID' => $instCourse->id,'course_rating.rating'=>1));
           $one =  $this->db->get('course_rating')->result();
        
        
           $count_Five = 0;
           $count_Foure = 0;
           $count_Three = 0;
           $count_Two = 0;
           $count_One = 0;
           $total_CountFive = 0;
           $total_CountFoure = 0;
           $total_CountThree = 0;
           $total_CountTwo = 0;
           $total_CountOne = 0;
           if(count($five) > 0){
            $count_Five = count($five);
            $total_CountFive = $count_Five*5;
           }
           if(count($four) > 0){
            $count_Foure = count($four);
            $total_CountFoure = $count_Foure*4;
           }
           if(count($three) > 0){
            $count_Three = count($three);
            $total_CountThree = $count_Three*3;
           }
           if(count($two) > 0){
            $count_Two = count($two);
            $total_CountTwo = $count_Two*2;
           }
           if(count($one) > 0){
            $count_One = count($one);
            $total_CountOne = $count_One*1;
           }
           $sumTotalCountRating = $total_CountFive + $total_CountFoure + $total_CountThree + $total_CountTwo + $total_CountOne;
           $sumCountRating = $count_Five + $count_Foure + $count_Three + $count_Two + $count_One;
           if($sumTotalCountRating > 0 && $sumCountRating > 0){
           $TotalratingAvg = $sumTotalCountRating/$sumCountRating;
           }else{
             $TotalratingAvg = 0;
           }
             
             $stars_rating="";
             $newwholeRating = floor($TotalratingAvg);
             $fractionRating = $TotalratingAvg - $newwholeRating;
             if($newwholeRating > 0){
             for($s=1;$s<=$newwholeRating;$s++){
               $stars_rating .= '<i class="icon-23"></i>';	
             }
             if($fractionRating >= 0.25){
               $stars_rating .= '<i class="icon-23 n50"></i>';	
             }
           }
             else{
               for($s=1;$s<=5;$s++){
                $stars_rating .= '<i class="icon-23 text-secondary"></i>';
               }
             }
        ?>
      <div class="item" data-sal-delay="150" data-sal="slide-up" data-sal-duration="800">
        <div class="edu-course course-style-5 inline" data-tipped-options="inline: 'inline-tooltip-<?=$instCourse->id?>'">
          <div class="inner">
            <div class="thumbnail">
              <a href="<?=base_url('course-details/'.base64_encode($instCourse->id))?>">
              <img src="<?=base_url($instCourse->image)?>" alt="Course Meta" style="height:269px;">
              </a>
            </div>
            <div class="content">
              <div class="course-price price-round"><?php if($price_more > 0){?>₹ <?=$price_more?><?php }else{?>Free<?php }?></div>
              <h5 class="title">
                <a href="<?=base_url('course-details/'.base64_encode($instCourse->id))?>"><?=$instCourse->course?></a>
              </h5>
              <div class="course-rating">
                <div class="rating">
                  <!-- <i class="icon-23"></i>
                    <i class="icon-23"></i>
                    <i class="icon-23"></i>
                    <i class="icon-23"></i>
                    <i class="icon-23"></i> -->
                  <?=$stars_rating?>
                </div>
                <span class="rating-count">(<?=$TotalratingAvg?>)</span>
              </div>
              <p><?=substr($instCourse->description,0,100)?></p>
              <ul class="course-meta">
                <li><i class="icon-24"></i><?=$instCourse->seats?> Total Seats</li>
                <li class="text-danger"><i class="icon-24 text-danger"></i><?=$instCourse->remaning_seats?> Remaning Seats</li>
              </ul>
            </div>
          </div>
        </div>
        <div id="inline-tooltip-<?=$instCourse->id?>" style="display:none">
          <div class="course-layout-five-tooltip-content">
            <div class="content">
              <!-- <span class="course-level">Cooking</span> -->
              <h5 class="title">
                <a href="<?=base_url('user-profile/'.base64_encode($instCourse->userID).'/'.base64_encode($instCourse->userType))?>">
                  <p><strong>Institute Name :-</strong> <?=$instCourse->user_name?></p>
                </a>
              </h5>
              <div class="course-rating">
                <div class="rating">
                  <!-- <i class="icon-23"></i>
                    <i class="icon-23"></i>
                    <i class="icon-23"></i>
                    <i class="icon-23"></i>
                    <i class="icon-23"></i> -->
                  <?=$stars_rating?>
                </div>
                <span class="rating-count">(<?=$TotalratingAvg?>)</span>
              </div>
              <ul class="course-meta">
                <!-- <li>15 Lessons</li>
                  <li>35 hrs</li>
                  <li>Beginner</li> -->
              </ul>
              <div class="course-feature">
                <p><?=substr($instCourse->description,0,200)?></p>
              </div>
              <?php //if($this->session->userdata('user_type')=='Student'){?>
              <div class="button-group">
                <a href="<?=base_url('course-details/'.base64_encode($instCourse->id))?>" class="edu-btn btn-secondary btn-small"><?= $this->session->userdata('user_type')=='Institute'? 'Course Detail' :($this->session->userdata('user_type')=='Experts'? 'Course Detail' : (in_array($instCourse->id,$booked_student_course) ? '<span ><b>Already Booked</b></span>' : 'Book Now' )) ?> <i class="icon-4"></i></a>
              </div>
              <?php //} ?>
            </div>
          </div>
        </div>
      </div>
      <?php }?>
      <!-- End Single Course  -->
    </div>
  </div>
</div>
</div>
<script>
  function checkedStar(val){
    var i;
    for(j = 1; j <= 5; j++){
         $('.rating-color'+j).css('color','');
      }
      for(i = 1; i <= val; i++){
         $('.rating-color'+i).css('color','#f8b81f');
      }
  }
  
  $("form#addRating").submit(function(e) {
    $(':input[type="submit"]').prop('disabled', true);
    e.preventDefault();    
    var formData = new FormData(this);
    formData.append("courseID", <?php echo $course->id?>);
    $.ajax({
    url: $(this).attr('action'),
    type: 'POST',
    data: formData,
    cache: false,
    contentType: false,
    processData: false,
    dataType: 'json',
    success: function (data) {
      if(data.status==200) {
      //$('.modal').modal('hide');
      //toastr.success(data.message);
      Swal.fire({
        icon: 'success',
        title: 'Success...',
        text: data.message,
        //footer: '<a href="">Why do I have this issue?</a>'
      })
      $(':input[type="submit"]').prop('disabled', false);
          setTimeout(function(){
            location.reload();
      }, 1000) 
  
      }else if(data.status==403) {
        Swal.fire({
          icon: 'error',
          title: 'Oops...',
          text: data.message,
          //footer: '<a href="">Why do I have this issue?</a>'
        })
     // toastr.error(data.message);
      $(':input[type="submit"]').prop('disabled', false);
      }else{
        Swal.fire({
          icon: 'error',
          title: 'Oops...',
          text: 'Something went wrong',
          //footer: '<a href="">Why do I have this issue?</a>'
        })
      $(':input[type="submit"]').prop('disabled', false);
      }
    },
    error: function(){} 
    });
  });
  
  
  function addStudentCourse(){
  var courseID = <?=$course->id?>;
  var price = <?=$price?>;
  var batch  = $('#batch').val();
  
  $.ajax({
    url: "<?=base_url('Courses/addStudentCourse')?>",
    type: 'POST',
    data: {courseID,price,batch},
    // cache: false,
    // contentType: false,
    // processData: false,
    dataType: 'json',
    success: function (data) {
      if(data.status==200) {
        Swal.fire({
        icon: 'success',
        title: 'Success...',
        text: data.message,
        //footer: '<a href="">Why do I have this issue?</a>'
      })
      $(':input[type="submit"]').prop('disabled', false);
          setTimeout(function(){
            location.reload();
      }, 1000) 
  
      }else if(data.status==403) {
        Swal.fire({
          icon: 'error',
          title: 'Oops...',
          text: data.message,
          //footer: '<a href="">Why do I have this issue?</a>'
        })
      //toastr.error(data.message);
      $(':input[type="submit"]').prop('disabled', false);
      }else{
        Swal.fire({
          icon: 'error',
          title: 'Oops...',
          text: 'Something went wrong',
          //footer: '<a href="">Why do I have this issue?</a>'
        })
      $(':input[type="submit"]').prop('disabled', false);
      }
    },
    error: function(){} 
    });
  
  }
  
  function redirectSigin(){
  Swal.fire({
  icon: 'error',
  title: 'Oops...',
  text: 'Please login to book courses thanks',
  //footer: '<a href="">Why do I have this issue?</a>'
  })
  setTimeout(function(){
      window.location="<?=base_url('my-account')?>";
  }, 2000) 
  }
</script>