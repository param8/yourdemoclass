<style>

.notification{
  height: 41px!important;
    line-height: 29px!important;
    padding: 0 24px!important;

}
</style>

<div class="edu-breadcrumb-area">
  <div class="container">
    <div class="breadcrumb-inner">
      <div class="page-title">
        <h1 class="title"><?=$page_title?></h1>
      </div>
      <ul class="edu-breadcrumb">
        <li class="breadcrumb-item"><a href="<?=base_url('home')?>">Home</a></li>
        <li class="separator"><i class="icon-angle-right"></i></li>
        <li class="breadcrumb-item"><a href="#"><?=$page_title?></a></li>
      </ul>
      <div class="name text-center">
        <?php if($this->session->userdata('user_type')=='Institute'){?>
          <div class="col-lg-12">
          <button type="button" class=" edu-btn btn-medium btn btn-primary notification" data-toggle="modal" data-target="#notification">Send Notification</button>
          </div>
          <?php } ?>
      </div>
    </div>
  </div>
  <ul class="shape-group">
    <li class="shape-1">
      <span></span>
    </li>
    <li class="shape-2 scene"><img data-depth="2" src="<?=base_url('public/website/assets/images/about/shape-13.png')?>" alt="shape"></li>
    <li class="shape-3 scene"><img data-depth="-2" src="<?=base_url('public/website/assets/images/about/shape-15.png')?>" alt="shape"></li>
    <li class="shape-4">
      <span></span>
    </li>
    <li class="shape-5 scene"><img data-depth="2" src="<?=base_url('public/website/assets/images/about/shape-07.png')?>" alt="shape"></li>
  </ul>
</div>
<!--=====================================-->
<!--=        Courses Area Start         =-->
<!--=====================================-->
<div class="edu-course-area course-area-1 section-gap-equal">
  <div class="container">
 <!----remove-this-inpue-23-bk--->
 
 
 
    <div class="row g-5">
      <div class="col-lg-2 order-lg-2">

      </div>
      <div class="col-lg-12 col-pr--35 order-lg-1">
        <div class="edu-sorting-area">
          <div class="sorting-left">
            <!-- <h6 class="showing-text">We found <span><span><?//=count($booked_courses)?></span> <?//=strtolower($page_title)?> available</h6> -->
          </div>
     
        </div>

        <table id="bookedCourseDataTable" class="table table-responsive  table-borderless" style="width:100%">
        <thead>
        <tr>
          <td></td>
        </tr>
          </thead>
        <tbody>

        </tbody>
        </table>

      </div>
    </div>
    <!-- <ul class="edu-pagination ">
      <li><a href="#" aria-label="Previous"><i class="icon-west"></i></a></li>
      <li class="active"><a href="#">1</a></li>
      <li><a href="#">2</a></li>
      <li><a href="#">3</a></li>
      <li class="more-next"><a href="#"></a></li>
      <li><a href="#">8</a></li>
      <li><a href="#" aria-label="Next"><i class="icon-east"></i></a></li>
      </ul> -->
  </div>
</div>

<!-- ved-modal -->
<div class="modal fade" id="notification" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
      <h5 class="modal-title" id="exampleModalLabel">Send Notification</h5>
        <button type="button" class="close btn-secondary" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form action="<?=base_url('setting/sendNotification')?>" method="post" id="notificationForm">
          <div class="form-group">
          <label for="message-text" class="col-form-label"><b>Courses:</b></label>
          <select id="courses" class="dropdown_css" name="courses" >
          <option value="">Select Course</option>
          <?php foreach($courses as $course){?>
            <option value="<?=$course->id?>"  <?=$this->session->userdata('courses_booked') == $course->id ? 'selected' : ''?>><?=$course->course?></option>
            <?php } ?>
          </select>
          </div>
            <div class="form-group">
              <label for="message-text" class="col-form-label"><b>Message:</b></label>
              <textarea class="form-control" id="message" name="message"></textarea>
            </div>
          
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-danger btn-lg  " data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary btn-lg  ">Send Notification</button>
        </div>
      </div>
  </form>
  </div>
</div>
<!-- ved -->



<script>
      function resetCourseBooked(){
      $.ajax({
       url: '<?=base_url("setting/resetCourseBooked")?>',
       type: 'POST',
       data: {ResetSesession:'ResetSesession'},  
       success: function (data) {
        location.reload();
       },
     });
    }

    function setBookedCourses(courses_booked){
      $.ajax({
       url: '<?=base_url("setting/setBookedCourses")?>',
       type: 'POST',
       data: {courses_booked},  
       success: function (data) {
        location.reload();
       },
     });
    }

    function changeStatus(id){
                $.ajax({
            url: "<?=base_url('Courses/studentCourseStatus')?>",
            type: 'POST',
            data: {id},
            // cache: false,
            // contentType: false,
            // processData: false,
            dataType: 'json',
            success: function (data) {
              if(data.status==200) {
                Swal.fire({
                icon: 'success',
                title: 'Success...',
                text: data.message,
                //footer: '<a href="">Why do I have this issue?</a>'
              })
              $(':input[type="submit"]').prop('disabled', false);
                  setTimeout(function(){
                    location.reload();
              }, 1000) 
          
              }else if(data.status==403) {
                Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: data.message,
                //footer: '<a href="">Why do I have this issue?</a>'
              })
              $(':input[type="submit"]').prop('disabled', false);
              }else{
                Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: 'Something went wrong',
                //footer: '<a href="">Why do I have this issue?</a>'
              })
              $(':input[type="submit"]').prop('disabled', false);
              }
            },
            error: function(){} 
            });
            }

            // function openModal(){
            //   $('#notificationModal').modal('show');
            // }
            // function closeModal(){
            //   $('#notificationModal').modal('hide');
            // }

     $("form#notificationForm").submit(function(e) {
			$(':input[type="submit"]').prop('disabled', true);
			e.preventDefault();    
			var formData = new FormData(this);
			$.ajax({
			url: $(this).attr('action'),
			type: 'POST',
			data: formData,
			cache: false,
			contentType: false,
			processData: false,
			dataType: 'json',
			success: function (data) {
				if(data.status==200) {
        Swal.fire({
          icon: 'success',
          title: 'Success...',
          text: data.message,
          //footer: '<a href="">Why do I have this issue?</a>'
        })
				$(':input[type="submit"]').prop('disabled', false);
						setTimeout(function(){
							location.reload();
				}, 2000) 
		
				}else if(data.status==403) {
          Swal.fire({
            icon: 'error',
            title: 'Oops...',
            text: data.message,
            //footer: '<a href="">Why do I have this issue?</a>'
          })
				$(':input[type="submit"]').prop('disabled', false);
				}else{
          Swal.fire({
            icon: 'error',
            title: 'Oops...',
            text: 'Something went wrong',
            //footer: '<a href="">Why do I have this issue?</a>'
          })
				//toastr.error('Something went wrong');
				$(':input[type="submit"]').prop('disabled', false);
				}
			},
			error: function(){} 
			});
		});
</script>