<style>
  .s_date {
  border: 1px solid #bab1b1!important;
  height: 42px!important;
  padding: 0 8px!important;
  border-radius: 0px!important;
  line-height: normal!important;
  }
</style>
<div class="edu-breadcrumb-area" >
  <div class="container">
    <div class="breadcrumb-inner">
      <div class="page-title">
        <h1 class="title "></h1>
      </div>
      <ul class="edu-breadcrumb">
        <li class="breadcrumb-item"><a href="<?=base_url('home')?>">Home</a></li>
        <li class="separator"><i class="icon-angle-right"></i></li>
        <li class="breadcrumb-item"><a href="javascript:void(0)"><?=$page_title?></a></li>
      </ul>
      
      
    </div>
  </div>
  <ul class="shape-group">
    <li class="shape-1">
      <span></span>
    </li>
    <li class="shape-2 scene"><img data-depth="2" src="<?=base_url('public/website/assets/images/about/shape-13.png')?>" alt="shape"></li>
    <li class="shape-3 scene"><img data-depth="-2" src="<?=base_url('public/website/assets/images/about/shape-15.png')?>" alt="shape"></li>
    <li class="shape-4">
      <span></span>
    </li>
    <li class="shape-5 scene"><img data-depth="2" src="<?=base_url('public/website/assets/images/about/shape-07.png')?>" alt="shape"></li>
  </ul>
</div>
<!--=====================================-->
<!--=        Courses Area Start         =-->
<!--=====================================-->
<div class="edu-course-area course-bg  " id="filterRefresh">
  
</div>
<div class="edu-course-area course-area-1 pt-0 section-gap-equal">
  <div class="container">
    <div class="row g-5">

      <div class="col-lg-12 col-pl--35">
        
        <table id="courseDataCityTable" class="table table-responsive  table-borderless" style="width:100%">
        <thead>
        <tr>
          <td></td>
        </tr>
          </thead>
        <tbody>

        </tbody>
        </table>
      </div>
    </div>
  </div>
  <p class="edu-pagination active"><?//= $links; ?></p>
  <!-- <ul class="edu-pagination ">
    <li><a href="#" aria-label="Previous"><i class="icon-west"></i></a></li>
    <li class="active"><a href="#"></a></li>
    <li><a href="#">2</a></li>
    <li><a href="#">3</a></li>
    <li class="more-next"><a href="#"></a></li>
    <li><a href="#">8</a></li>
    <li><a href="#" aria-label="Next"><i class="icon-east"></i></a></li> 
    </ul> -->
</div>
</div>
<script>

  
</script>
<script>
  // 		$(document).ready(function(){
  //       var session_user_type = '<?//=$this->session->userdata('user_type')?>';
  //       if(session_user_type == 'Institute'){
  //         $(window).scrollTop(400);
  //       }else{
  //         $(window).scrollTop(600);
  //       }
  		   
  // 	   });
  // 	  
</script>          
<script>
  function getComment(courseID){
    
    $.ajax({
      url: '<?=base_url('Ajax_controller/getcomment')?>',
      type: 'POST',
      data: {courseID},
      success: function (data) {
        $('#show_comment'+courseID).html(data);
      }
    });
  }

  $(document).ready(function () {
    
    var courseDataCityTable = $('#courseDataCityTable').DataTable({
      "processing":true,
      "serverSide":true,
      buttons: [
      { extend: 'excelHtml5', text: 'Download Excel' }
    ],
      "order":[],
      "ajax":{
          url:"<?=base_url('Ajax_controller/ajax_course_datatable_city/'.$cityName)?>",
          type:"POST"
      },
      "columnDefs":[
       {
           "targets":[0],
           "orderable":false,
       },
     ],
  
 });
});
</script>