<!DOCTYPE html>
<html class="no-js" lang="zxx">


<!-- Mirrored from edublink.html.devsblink.com/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 16 Dec 2022 06:10:51 GMT -->
<head>
    <!-- Meta Data -->
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?=$page_title?></title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Favicon -->
    <link rel="shortcut icon" type="image/x-icon" href="public/favicon.ico">
    <!-- CSS
	============================================ -->
	 <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.2/css/bootstrap.css">
	 <link rel="stylesheet" href="https://cdn.datatables.net/1.13.4/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" href="<?//=base_url('public/website/assets/css/vendor/bootstrap.min.css')?>">
     
    
    	 <link rel="stylesheet" href="https://cdn.datatables.net/1.13.4/css/dataTables.bootstrap5.min.css">
    <link rel="stylesheet" href="<?=base_url('public/website/assets/css/vendor/icomoon.css')?>">
    <link rel="stylesheet" href="<?=base_url('public/website/assets/css/vendor/remixicon.css')?>">
    <link rel="stylesheet" href="<?=base_url('public/website/assets/css/vendor/magnifypopup.min.css')?>">
    <link rel="stylesheet" href="<?=base_url('public/website/assets/css/vendor/odometer.min.css')?>">
    <link rel="stylesheet" href="<?=base_url('public/website/assets/css/vendor/lightbox.min.css')?>">
    <link rel="stylesheet" href="<?=base_url('public/website/assets/css/vendor/animation.min.css')?>">
    <link rel="stylesheet" href="<?=base_url('public/website/assets/css/vendor/jqueru-ui-min.css')?>">
    <link rel="stylesheet" href="<?=base_url('public/website/assets/css/vendor/swiper-bundle.min.css')?>">
    <link rel="stylesheet" href="<?=base_url('public/website/assets/css/vendor/tipped.min.css')?>">
    <link rel="stylesheet" href="<?=base_url('public/website/assets/css/vendor/owl.carousel.min.css')?>">
 <link rel="stylesheet" href="<?=base_url('public/website/assets/js/vendor/jqueryowl.min.js')?>">
  <link rel="stylesheet" href="<?=base_url('public/website/assets/js/vendor/owl.carousel.js')?>">
    <!-- Site Stylesheet -->
    <link rel="stylesheet" href="<?=base_url('public/website/assets/css/app.css')?>">
 


  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.css" integrity="sha512-3pIirOrwegjM6erE5gPSwkUzO+3cTjpnV9lexlNZqvupR64iZBnOOTiiLPb9M36zpMScbmUNIcHUqKD47M719g==" crossorigin="anonymous" referrerpolicy="no-referrer" />
  <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js" integrity="sha512-VEd+nq25CkR676O+pLBnDW09R7VQX9Mdiij052gVCp5yVH3jGtH70Ho/UUv4mJDsEdTvqRCFZg0NKGiojGnUCw==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
  <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
  <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" ></script>
  <script src="https://cdn.jsdelivr.net/npm/popper.js@1.12.9/dist/umd/popper.min.js" ></script>
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/js/bootstrap.min.js" ></script>
 
 
 
 
  <style>
    .rating-width{
      width:15px;
      height:20px;
    }
    .n50{
      width: 11.5%;
      display: inline-block;
      overflow: hidden;
    }
    </style>
     <script>
          $(document).ready(function () {
            $('.owl-carousel').owlCarousel({
              loop: true,
              margin: 10,
              responsiveClass: true,
              autoplay: true,
              responsive: {
                0: {
                  items: 1,
                nav: false,
                  navigator:false,
                  loop: true,
                  dots: false,
                  pagination: false,
                },
                600: {
                  items: 2,
                nav: false,
                  navigator:false,
                  loop: true,
                  dots: false,
                  pagination: false,
                },
                1000: {
                  items: 4,
                  nav: false,
                  navigator:false,
                  loop: true,
                  dots: false,
                  pagination: false,
                  margin: 20
                }
              }
            })
          })
        </script>

</head>