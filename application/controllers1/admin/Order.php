<?php 
class Order extends Admin_Controller 
{
	public function __construct()
	{
		parent::__construct();
		 if($this->session->userdata('user_type')!='Admin'){
          redirect('home');
      }
		$this->not_admin_logged_in();
        $this->load->model('Order_model');
        $this->load->model('Common_model');
	}

	public function index()
	{	
		$data['page_title'] = 'Orders';
		$data['siteinfo'] = $this->siteinfo();
	    $data['schools'] = $this->School_model->get_all_schools();
		$data['orders'] = $this->Order_model->get_all_orders();
		$this->admin_template('order',$data);
	}

    public function view_invoice($order_id)
    {
        $data['page_title'] = 'Orders';
		$data['siteinfo'] = $this->siteinfo();
        $data['order_details'] = $this->Order_model->get_order_details(array('orders.id'=>$order_id));
		$data['order_items'] = $this->Order_model->get_order_items(array('order_items.oid'=>$order_id));
        $data['shipping_details'] = $this->Order_model->get_shipping_details(array('uid'=>$data['order_details']->user_id));
		$this->admin_template('view_invoice',$data);
    }

	public function edit()
    {
        $order_id       = $this->input->post('order_id');
        $order_status  = $this->input->post('order_status');

        if(empty($order_status)){
            echo json_encode(['status'=>403, 'message'=>'Invalid Request']);
                exit();
        }

        $data = array(
			'status' => $order_status,	
			'modify_date' 	=> date('Y-m-d H:i:s')
		);

        $update = $this->Order_model->update_order($data, $order_id);
		if($update)
		{
			echo json_encode(['status'=>200, 'message'=>'Order status updated successfully!']);
      		exit();
		}else{
			echo json_encode(['status'=>403, 'message'=>'Record not updated. please try again!']);
      		exit();
		}

    }

	public function setSchoolFilter(){
		$school = $this->input->post('val');
		$this->session->set_userdata('school', $school);
	}

	public function resetSchoolFilter(){
		$this->session->unset_userdata('school');
	}

	public function setFromDateFilter(){
		$fromDate = $this->input->post('val');
		$this->session->set_userdata('fromDate', $fromDate);
	}

	public function resetFromDateFilter(){
		$this->session->unset_userdata('fromDate');
	}

	public function setToDateFilter(){
		$toDate = $this->input->post('val');
		$this->session->set_userdata('toDate', $toDate);
	}

	public function resetToDateFilter(){
		$this->session->unset_userdata('toDate');
	}
		
}