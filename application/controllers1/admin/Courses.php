<?php 
class Courses extends MY_Controller 
{
	public function __construct()
	{
		parent::__construct();
	if($this->session->userdata('user_type')!='Admin'){
          redirect('home');
      }
		$this->not_admin_logged_in();
		$this->load->model('course_model');
        $this->load->model('category_model');
        $this->load->model('teachers_model');
	}

	public function index()
	{	
		$data['page_title'] = 'New  Courses';
		$condition =  array('courses.status'=>0) ;
        $data['courses'] = $this->course_model->get__admin_courses($condition);
		$this->admin_template('courses',$data);
	}

	public function approved_courses()
	{	
		$data['page_title'] = 'Approved  Courses';
		$condition =  array('courses.status'=>1) ;
        $data['courses'] = $this->course_model->get__admin_courses($condition);
		$this->admin_template('courses',$data);
	}

  public function update_status()
  {
	  $id = $this->input->post('userid');

	  $data =  array(
		'status' => 1
	  );
	  $update_status =  $this->course_model->updateCourse($data,$id);
    $course = $this->course_model->get_course(array('courses.id' =>$id));
	  $message = "Admin Accepted your course request for ".$course->course;
    $data = array(
      'userID' => $course->userID,
      'send_by' => $this->session->userdata('id'),
      'message' => $message
    );
    $store_notification = $this->notification_model->store_notification($data);
  }

  public function average_price(){
	$conditionCourse = $this->session->userdata('user_type')=='Institute' ? array('courses.userID'=>$this->session->userdata('id'),'courses.status'=>1) : ($this->session->userdata('user_type')=='Student' ? array('courses.locationID'=>$this->session->userdata('city'),'courses.status'=>1):array('courses.status'=>1));
	$data['totalCourses'] = $this->course_model->get_courses($conditionCourse);
	$data['page_title'] = "Average Price Form";
	//$data['avg_courses'] = $this->course_model->get_avg_course(array('status'=>1));
	//print_r($data['totalCourses']);die;
    $this->admin_template('average_price',$data);
  }

  public function average_courses(){
	$data['page_title'] = "Average Courses";
	$data['avg_courses'] = $this->course_model->get_avgCourses(array('status' => 1),$value=null);
    $this->admin_template('average_courses',$data);
  }
  
  public function update_Dealstatus()
  {
	  $id = $this->input->post('userid');
	  $data =  array(
		'deal_status' => 1
	  );
	  $update_status =  $this->course_model->updateAvgCourse($data,$id);
      } 
  
	  public function avg_form(){
		//print_r($_POST); die;
		
		$user_type = $this->session->userdata('user_type'); 
		$name = $this->session->userdata('user_type');
		$course_name = $this->input->post('course');
		$institute = $this->input->post('institute');
		$city = $this->input->post('city');
		$price = $this->input->post('course_price');
		$mode = $this->input->post('mode');
	
		 if(empty($name)){
		  echo json_encode(['status'=>403, 'message'=>'Please Enter Name']);  	
		  exit();
		}
	
		if(empty($course_name)){
		  echo json_encode(['status'=>403, 'message'=>'Please Enter Course']);  	
		  exit();
		}
	  
		if(empty($institute)){
		  echo json_encode(['status'=>403, 'message'=>'Please Enter Institute']);  	
		  exit();
		}
		if(empty($city)){
		  echo json_encode(['status'=>403, 'message'=>'Please Enter city']); 	
		  exit();
		}
		if(empty($price)){
			echo json_encode(['status'=>403, 'message'=>'Please Enter price']);   	
			exit();
		  }
		if(empty($mode)){
		  echo json_encode(['status'=>403, 'message'=>'Please Select Mode']); 	
		  exit();
		}      
		
	  
		$store_institute = array(
		  'name' => $name,
		  'user_type' => $user_type,
		  'course' => $course_name,
		  'institute' => $institute,
		  'city' => $city,
		  'mode' => $mode,
		  'price' => $price,
		);
	  //print_r($store_institute);die;
		$store = $this->course_model->store_course_instittute($store_institute);
		  if($store){
		 echo json_encode(['status'=>200, 'message'=>'Detail added successfully!']);
		 }else{
			echo json_encode(['status'=>302, 'message'=>'Something wrong happened']);   
		  }
		 }
  
	
}