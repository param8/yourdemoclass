<?php 
class Ajax_controller extends MY_Controller 
{
	public function __construct()
	{
		parent::__construct();
    //$this->not_admin_logged_in();
     $this->load->model('course_model');
     $this->load->model('user_model');
     $this->load->model('setting_model');


	}

	public function get_city()
    {
       $stateID =  $this->input->post('stateID');

       $cities = $this->Common_model->get_state_wise_city($stateID);
       if(count($cities) > 0)
       {
        ?>
        <option value="">Select City</option>
        <?php foreach($cities as $city){ ?>
           <option value="<?=$city->id?>" <?=$this->session->userdata('location_city')==$city->id ? 'selected' : ''?>><?=$city->name?></option>
        <?php
        }
       }else
       {
        ?>
        <option value="">No City found</option>
        <?php
       }
    }

public function getInstitute_city(){
    $userName =  $this->input->post('userName');
    $userDetailed = $this->user_model->get_userDetail(array('users.name' => $userName));
    
    echo $userDetailed->cityName;
  
      }
   //print_r($userDetailed) ;die;
  //$cities = $this->Common_model->get_state_wise_city($stateID);

    public function ajax_course_datatable(){
      $location = !empty($this->session->userdata('location_city')) ? $this->session->userdata('location_city') :$this->session->userdata('city');
      if(!empty($this->session->userdata('course')) OR !empty($this->session->userdata('start_date')) OR !empty($this->session->userdata('mode')) OR !empty($this->session->userdata('location_state')) OR !empty($this->session->userdata('location_city'))){ 
        $condition = array('courses.status'=>1);
      }else{
  
        $lcation_array = !empty($this->session->userdata('location_city')) ? array('courses.status'=>1,'courses.locationID' =>$this->session->userdata('location_city'))  :array('courses.status'=>1);
        $condition = $this->session->userdata('user_type')=='Institute' ? array('courses.userID'=>$this->session->userdata('id'),'courses.status'=>1) : ($this->session->userdata('user_type')=='Student' ? array('courses.status'=>1):$lcation_array);
      }
      
      
      if($this->session->userdata('user_type')=='Student'){
          
          $booked = $this->course_model->get_count_booked_course(array('studentID' =>$this->session->userdata('id'),'status'=>1));
      }

      $fetch_data = $this->course_model->make_datatables($condition);
      
      $data = array();

      $bookedCourses =array();
      foreach($booked as $book){
         $bookedCourses[] = $book->courseID; 
      }
      //print_r($fetch_data);
      foreach($fetch_data as $key=>$course) // Loop over the data fetched and store them in array
        {
          $sub_array =array();
             $id = $course['id'];
              $earlier = new DateTime($course['start_date']);
              $later = new DateTime($cours['end_date']);
              $totalDays =  $later->diff($earlier)->format("%a") +1;
              if($course['fees'] > 0 && $course['discount'] > 0){
                  $price =$course['fees'] - ($course['discount']*$course['fees'])/100;
              }else{
                  $price =$course['fees'];
              }
              $this->db->where(array('course_rating.courseID' => $id));
              $allOverRating =  $this->db->get('course_rating')->result();
          
              $this->db->where(array('course_rating.courseID' => $id,'course_rating.rating'=>5));
              $five =  $this->db->get('course_rating')->result();
          
              $this->db->where(array('course_rating.courseID' => $id,'course_rating.rating'=>4));
              $four =  $this->db->get('course_rating')->result();
          
              $this->db->where(array('course_rating.courseID' => $id,'course_rating.rating'=>3));
              $three =  $this->db->get('course_rating')->result();
          
              $this->db->where(array('course_rating.courseID' => $id,'course_rating.rating'=>2));
              $two =  $this->db->get('course_rating')->result();
          
              
              $this->db->where(array('course_rating.courseID' => $id,'course_rating.rating'=>1));
              $one =  $this->db->get('course_rating')->result();

              $count_Five = 0;
              $count_Foure = 0;
              $count_Three = 0;
              $count_Two = 0;
              $count_One = 0;
              $total_CountFive = 0;
              $total_CountFoure = 0;
              $total_CountThree = 0;
              $total_CountTwo = 0;
              $total_CountOne = 0;
              if(count($five) > 0){
               $count_Five = count($five);
               $total_CountFive = $count_Five*5;
              }
              if(count($four) > 0){
               $count_Foure = count($four);
               $total_CountFoure = $count_Foure*4;
              }
              if(count($three) > 0){
               $count_Three = count($three);
               $total_CountThree = $count_Three*3;
              }
              if(count($two) > 0){
               $count_Two = count($two);
               $total_CountTwo = $count_Two*2;
              }
              if(count($one) > 0){
               $count_One = count($one);
               $total_CountOne = $count_One*1;
              }
              $sumTotalCountRating = $total_CountFive + $total_CountFoure + $total_CountThree + $total_CountTwo + $total_CountOne;
              $sumCountRating = $count_Five + $count_Foure + $count_Three + $count_Two + $count_One;
              if($sumTotalCountRating > 0 && $sumCountRating > 0){
              $TotalratingAvg = $sumTotalCountRating/$sumCountRating;
              }else{
                $TotalratingAvg = 0;
              }
                
                $stars_rating="";
                $newwholeRating = floor($TotalratingAvg);
                $fractionRating = $TotalratingAvg - $newwholeRating;
                if($newwholeRating > 0){
                for($s=1;$s<=$newwholeRating;$s++){
                  $stars_rating .= '<i class="icon-23"></i>';	
                }
                if($fractionRating >= 0.25){
                  $stars_rating .= '<i class="icon-23 n50"></i>';	
                }
              }
                else{
                  for($s=1;$s<=5;$s++){
                   $stars_rating .= '<i class="icon-23 text-secondary"></i>';
                  }
                }  
                $price_fecth = $price > 0 ?  '₹ '.$price.' / <del>₹ '.$course['fees'].'</del> ': 'Free'; 
                $free_demo_class= $course['free_demo'] > 0 ? 'text-success' : 'text-danger float-right';
                $login_url = !empty($this->session->userdata('email')) ? base_url('course-details/'.base64_encode($id)) :base_url('my-account');

              $button_url =  !empty($this->session->userdata('email')) ? base_url('course-details/'.base64_encode($id)) :base_url('my-account');
              $button_name =   $this->session->userdata('user_type')=='Institute'? 'Course Detail' :($this->session->userdata('user_type')=='Experts'? 'Course Detail' : (in_array($id,$bookedCourses) ? '<span class="text-danger"><b>Already Booked</b></span>' : 'Book Now' ));
              // echo '<pre>';
              // print_r($course);
          $sub_array[] = '<div class="edu-course eud_course_bg course-style-4 course-style-8">
          <div class="inner">
            <div class="thumbnail cou_rse">
              <a href="javascript:void(0)">
              <img src="'.base_url($course['image']).'" alt="Course Meta" style="">
              </a>
              <div class="time-top">
                <span class="duration"><i class="icon-61"></i>'.$totalDays.' Days</span>
              </div>
            </div>
            <div class="content">
              <div class="course-price">'.$price_fecth.'<span class="'.$free_demo_class.'" style="float:right!important;">
                  Free demo classes are ('.$course['free_demo'].')
                  <div class="course-rating">
                    <div class="rating">
                     '.$stars_rating.'
                    </div>
                    <span class="rating-count">('.$TotalratingAvg .'/'.count($allOverRating).' Rating)</span>
                  </div>
                </span>
              </div>
              <h6 class="title">
                <a href="javascript:void(0)">'.$course['course'].'</a>
              </h6>
              <p>'.substr($course['description'],0,200).'</p>
              <ul class="course-meta cours_e">
                <li><i class="icon-25"></i>'.$course['seats'].' Total Seats</li>
                <li><i class="icon-25"></i><span class="text-danger">'.$course['remaning_seats'].'Remaning Seats</span></li>
                <li><i class="icon-28"></i><a  onclick="getComment('.$course['id'].')" style="cursor:pointer">Comment</a></li>
              </ul>
              <div id="show_comment'.$id.'" class="commen_t"></div>
            </div>
          </div>
          <div class="hover-content-aside">
            <div class="content">
              <div class="c_n">
                <span class="course-level" title="'.$course['category'].'">'.$course['category'].'</span>
                <h5 class="title m0">
                  <a href="'.base_url('user-profile/'.base64_encode($course['userID']).'/'.base64_encode('Institute')).' " title="'.$course['user_name'].'">'.$course['user_name'].'</a>
                </h5>
               </div>
              <ul class="course-meta course">
                <li class="text-success">'.date('d M Y',strtotime($course['start_date'])).' <span >Batch Start</span></li>
                <li class="text-danger">'.date('d M Y',strtotime($course['end_date'])).' <span>Batch End</span></li>
                <li class="text-warning">
                  <span>'.$totalDays.' Days</span>  <span class="text-primary"> Mode :- '.$course['mode'].'</span>
                </li>
              </ul>
           
              <div class="button-group boo_k ">
              <a  href="'.$button_url.'" class="edu-btn btn-medium">'.$button_name .'</a>
              </div>
            </div>
          </div>
        </div>';
           
            $data[] = $sub_array;
        }

        $output = array(
          "draw"                    =>     intval($_POST["draw"]),
          "recordsTotal"            =>     $this->course_model->get_all_data($condition),
          "recordsFiltered"         =>     $this->course_model->get_filtered_data($condition),
          "data"                    =>     $data
      );
      
      echo json_encode($output);
    }


    public function ajax_bookedCourses_datatable(){
    $userID = $this->session->userdata('id');
    $condition = $this->session->userdata('user_type')=='Institute' ? array('courses.userID'=>$this->session->userdata('id')) : ($this->session->userdata('user_type')=='Student' ? array('booked_courses.studentID'=>$this->session->userdata('id')):array('booked_courses.status'=>1)) ;
    //$data['booked_courses'] = $this->course_model->get_student_booked_course($condition);
    // $condition_courses = $this->session->userdata('user_type')=='Institute' ? array('courses.userID'=>$this->session->userdata('id'),'courses.status'=>1) : ($this->session->userdata('user_type')=='Student' ? array('courses.locationID'=>$this->session->userdata('city'),'courses.status'=>1):array('courses.status'=>1));
    // $data['courses'] = $this->course_model->get_courses($condition_courses);
      $fetch_data = $this->course_model->make_datatables_booked_course($condition); // this will call modal function for fetching data
     // print_r($fetch_data);
      $data = array();
      foreach($fetch_data as $key=>$booked) // Loop over the data fetched and store them in array
      {
        $sub_array =array();
        $earlier = new DateTime($booked['start_date']);
        $later = new DateTime($booked['end_date']);
        $totalDays =  $later->diff($earlier)->format("%a");
        if($booked['fees'] > 0 && $booked['discount'] > 0){
            $price =$booked['fees'] - ($booked['discount']*$booked['fees'])/100;
        }else{
            $price =$booked['fees'];
        }
        $price_fecth = $price > 0 ?  '₹ '.$price.' / <del>₹ '.$booked['fees'].'</del> ': 'Free'; 

        $url =$this->session->userdata('user_type')=='Student' ? 'user-profile/'.base64_encode($booked['institute_id']).'/'.base64_encode($booked['institute_user_type']) : ($this->session->userdata('user_type')=='Institute' ? 'user-profile/'.base64_encode($booked['student_id']).'/'.base64_encode($booked['Student_user_type']) : '');
        $user_name =$this->session->userdata('user_type')=='Student' ? $booked['institute_name'] : ($this->session->userdata('user_type')=='Institute' ? $booked['student_name'] : '');
        $user_email = $this->session->userdata('user_type')=='Student' ? $booked['institute_email'] : ($this->session->userdata('user_type')=='Institute' ? $booked['student_email'] : '');
        $user_contact = $this->session->userdata('user_type')=='Student' ? $booked['institute_contact'] : ($this->session->userdata('user_type')=='Institute' ? $booked['student_contact'] : ''); 
        $approve_button = $this->session->userdata('user_type')=='Institute' ? 'onclick="changeStatus('.$booked['id'].')"' : '' ;
        $status_button =  $booked['status']==0 ? 'Pending' : 'Active';
        $status_button_class =  $booked['status']==0 ? 'btn-danger' : 'btn-success';
       
     
         
       $sub_array[] = '<div class="edu-course course-style-4 course-style-8">
          <div class="inner">
            <div class="thumbnail">
              <a href="javascript:void(0)">
              <img src="'.base_url($booked['profile_pic']).'" alt="Course Meta" style="height:200px;widht:200px;">
              </a>
            </div>
            <div class="content all_d"> 
              <span class="detail_f">
              <div class="course-price">'.$price_fecth.'</div>
              <h6 class="title">
                <a href="'.$url.'"><i class="icon-25"></i> '.$user_name.'</a>
              </h6>
              <h6 class="title">
                <a><i class="icon-envelope"> </i>'.$user_email.'</a>
              </h6>
              <h6 class="title">
                <a><i class="icon-phone"> </i>'.$user_contact.'</a>
              </h6>
       
        </span>
              <span class="detail_s">
              <p><b>Batch</b> '.$booked['course_batch'] .' '.$booked['time_formate'].'</p>
              <p title="'.$booked['course_discription'].'">'.substr($booked['course_discription'],0,200).'</p>
              <ul class="course-meta">
                <li><i class="icon-24"></i>
                <button class="btn btn-lg '.$status_button_class.'"'. $approve_button.' >'.$status_button.'</button>
                </li>
              </ul>
              <span>

            </div>
          </div>
          <div class="hover-content-aside">
            <div class="content">
              <span class="course-level">'.$booked['category'].'</span>
              <h5 class="title">
                <a href="#">'.$booked['course_name'].'</a>
              </h5>
          
              <ul class="course-meta">
                <li class="text-success">'.date('d M Y',strtotime($booked['start_date'])).' <span >Batch Start</span></li>
                <li class="text-danger">'.date('d M Y',strtotime($booked['end_date'])).' <span>Batch End</span></li>
                <li class="text-warning">'.$totalDays.' Days</li>
                <li class="text-primary">Mode :- '.$booked['mode'].'</li>
              </ul>
           
              <div class="button-group">
                <a href="'.base_url('course-details/'.base64_encode($booked['id'])).'" class="edu-btn btn-medium">Course Detail</a>
              </div>
            </div>
          </div>
        </div>';
          $data[] = $sub_array;
      }

      $output = array(
          "draw"                    =>     intval($_POST["draw"]),
          "recordsTotal"            =>     $this->course_model->get_all_data_booked_course($condition),
          "recordsFiltered"         =>     $this->course_model->get_filtered_data_booked_course($condition),
          "data"                    =>     $data
      );
      
      echo json_encode($output);
    }


    public function ajax_notification_datatable(){
      if($this->session->userdata('user_type')=='Admin'){
    		$condition = array('admin_status'=>1); 
		  }else{
        $id =$this->session->userdata('id');
        $condition = array('userID' =>$id);
		 }
      $fetch_data = $this->notification_model->make_datatables($condition); // this will call modal function for fetching data
      $data = array();
      foreach($fetch_data as $key=>$notification) // Loop over the data fetched and store them in array
      {
        $sub_array =array();
          $sub_array[] = $key+1;
          $sub_array[] = $notification['message'];;
          $sub_array[] = date('d-M-Y A',strtotime($notification['created_at']));
          $data[] = $sub_array;
      }

      $output = array(
          "draw"                    =>     intval($_POST["draw"]),
          "recordsTotal"            =>     $this->notification_model->get_all_data($condition),
          "recordsFiltered"         =>     $this->notification_model->get_filtered_data($condition),
          "data"                    =>     $data
      );
      
      echo json_encode($output);
    }


    public function load_more(){
       $id = $this->input->post('id');
       $output = "";
      if(!empty($this->input->post('categoryID'))){
        $categoryID = $this->input->post('categoryID');
        $category = $this->category_model->get_category(array('id' => $categoryID));
        $condition = $this->session->userdata('user_type')=='Institute' ? array('courses.userID'=>$this->session->userdata('id'),'courses.categoryID'=>$categoryID,'courses.status'=>1,'courses.id >'=>$id) : ($this->session->userdata('user_type')=='Student' ? array('courses.locationID'=>$this->session->userdata('city'),'courses.categoryID'=>$categoryID,'courses.status'=>1,'courses.id >'=>$id):array('courses.categoryID'=>$categoryID,'courses.status'=>1,'courses.id >'=>$id));
      }else{
        $categoryID = 0;
        $condition = $this->session->userdata('user_type')=='Institute' ? array('courses.userID'=>$this->session->userdata('id'),'courses.status'=>1,'courses.id >'=>$id) : ($this->session->userdata('user_type')=='Student' ? array('courses.locationID'=>$this->session->userdata('city'),'courses.status'=>1,'courses.id >'=>$id):array('courses.status'=>1,'courses.id >'=>$id));
      }
        $courses = $this->course_model->get_courses($condition);
        $courseID = "";
          
          foreach($courses as $course){
            $courseID = $course->id;
            $earlier = new DateTime($course->start_date);
            $later = new DateTime($course->end_date);
            $totalDays =  $later->diff($earlier)->format("%a");
            if($course->fees > 0 && $course->discount > 0){
                $price =$course->fees - ($course->discount*$course->fees)/100;
            }else{
                $price =$course->fees;
            }
            $this->db->where(array('course_rating.courseID' => $course->id));
            $allOverRating =  $this->db->get('course_rating')->result();
        
            $this->db->where(array('course_rating.courseID' => $course->id,'course_rating.rating'=>5));
            $five =  $this->db->get('course_rating')->result();
        
            $this->db->where(array('course_rating.courseID' => $course->id,'course_rating.rating'=>4));
            $four =  $this->db->get('course_rating')->result();
        
            $this->db->where(array('course_rating.courseID' => $course->id,'course_rating.rating'=>3));
            $three =  $this->db->get('course_rating')->result();
        
            $this->db->where(array('course_rating.courseID' => $course->id,'course_rating.rating'=>2));
            $two =  $this->db->get('course_rating')->result();
        
            
            $this->db->where(array('course_rating.courseID' => $course->id,'course_rating.rating'=>1));
            $one =  $this->db->get('course_rating')->result();
            $count_Five = 0;
            $count_Foure = 0;
            $count_Three = 0;
            $count_Two = 0;
            $count_One = 0;
            $total_CountFive = 0;
            $total_CountFoure = 0;
            $total_CountThree = 0;
            $total_CountTwo = 0;
            $total_CountOne = 0;
            if(count($five) > 0){
             $count_Five = count($five);
             $total_CountFive = $count_Five*5;
            }
            if(count($four) > 0){
             $count_Foure = count($four);
             $total_CountFoure = $count_Foure*4;
            }
            if(count($three) > 0){
             $count_Three = count($three);
             $total_CountThree = $count_Three*3;
            }
            if(count($two) > 0){
             $count_Two = count($two);
             $total_CountTwo = $count_Two*2;
            }
            if(count($one) > 0){
             $count_One = count($one);
             $total_CountOne = $count_One*1;
            }
            $sumTotalCountRating = $total_CountFive + $total_CountFoure + $total_CountThree + $total_CountTwo + $total_CountOne;
            $sumCountRating = $count_Five + $count_Foure + $count_Three + $count_Two + $count_One;
            if($sumTotalCountRating > 0 && $sumCountRating > 0){
            $TotalratingAvg = $sumTotalCountRating/$sumCountRating;
            }else{
              $TotalratingAvg = 0;
            }
              
              $stars_rating="";
              $newwholeRating = floor($TotalratingAvg);
              $fractionRating = $TotalratingAvg - $newwholeRating;
              if($newwholeRating > 0){
              for($s=1;$s<=$newwholeRating;$s++){
                $stars_rating .= '<i class="icon-23"></i>';	
              }
              if($fractionRating >= 0.25){
                $stars_rating .= '<i class="icon-23 n50"></i>';	
              }
            }
              else{
                for($s=1;$s<=5;$s++){
                 $stars_rating .= '<i class="icon-23 text-secondary"></i>';
                }
              }  
        
      $output .= '<div class="col-md-6 col-lg-4 col-xl-3 sal-animate" data-sal-delay="100" data-sal="slide-up" data-sal-duration="800">
        <div class="edu-course course-style-1 course-box-shadow hover-button-bg-white">
          <div class="inner">
            <div class="thumbnail">
              <a href="base_url(course-details/'.base64_encode($course->id).')">
              <img src="'.base_url($course->image).'" alt="Course Meta" style="height:269px">
              </a>
              <div class="time-top">
                <span class="duration"><i class="icon-61"></i>'.$totalDays.' Days</span>
              </div>
            </div>
            <div class="content">
              <!-- <span class="course-level">Beginner</span> -->
              <h6 class="title">
                <a href="#">'.$course->course.'</a>
              </h6>
              <div class="course-rating">
                <div class="rating">
                  <!-- <i class="icon-23"></i>
                    <i class="icon-23"></i>
                    <i class="icon-23"></i>
                    <i class="icon-23"></i>
                    <i class="icon-23"></i> -->
                  '. $stars_rating.'
                </div>
                <span class="rating-count">('.$TotalratingAvg .'/'.count($allOverRating).' Rating)</span>
              </div>
              <div class="course-price">₹ '.$price.' / <del>₹ '.$course->fees.'</del></div>
              <div class="title">Mode <span class="text-success">'.$course->mode.'</span> </div>
              <ul class="course-meta">
                <li><i class="icon-24"></i>'.$course->seats.' Total Seats</li>
                <li class="text-danger"><i class="icon-24 text-danger"></i>'.$course->remaning_seats.' Remaning Seats</li>
              </ul>
            </div>
          </div>
          <div class="course-hover-content-wrapper">
            <button class="wishlist-btn"><i class="icon-22"></i></button>
          </div>
          <div class="course-hover-content-wrapper">
            <button class="wishlist-btn"><i class="icon-22"></i></button>
          </div>
          <div class="course-hover-content">
            <div class="content">
              <!-- <span class="course-level">Advanced</span> -->
              <h6 class="title">
                <a href="'.base_url('course-details/'.base64_encode($course->id)).'">'.$course->course.'</a>
              </h6>
              <div class="course-rating">
                <div class="rating">
                  <!-- <i class="icon-23"></i>
                    <i class="icon-23"></i>
                    <i class="icon-23"></i>
                    <i class="icon-23"></i>
                    <i class="icon-23"></i> -->
                  '.$stars_rating.'
                </div>
                <span class="rating-count">('.$TotalratingAvg .'/'.count($allOverRating).' Rating)</span>
              </div>
              <div class="course-price">₹ '.$price.' / <del>₹ '.$course->fees.'</del></div>
              <p>'.$course->description.'</p>
              <ul class="course-meta">
                <li><i class="icon-24"></i>'.$course->seats.'Total Seats</li>
                <li class="text-danger"><i class="icon-24 text-danger"></i>'.$course->remaning_seats.' Remaning Seats</li>
              </ul>
              <a href="'.base_url('course-details/'.base64_encode($course->id)).'" class="edu-btn btn-secondary btn-small">Enrolled <i class="icon-4"></i></a>
            </div>
          </div>
        </div>
      </div>';
       } 
       //echo $courseID;
       if($courseID){
     $output .=  '<div class="load-more-btn sal-animate" id="remove_row" data-sal-delay="100" data-sal="slide-up" data-sal-duration="1200">
      <a href="#" data-id ="'.$courseID.'" id="load_more" class="edu-btn loadButton">Load More <i class="icon-56"></i></a>
     </div>';
      }
     
    echo $output;

    }

    public function getcomment(){
      $courseID = $this->input->post('courseID');
      $comments = $this->course_model->getcomments(array('course_rating.courseID'=>$courseID));
      //print_r($comments);
      if(count($comments)){
        foreach ($comments as $comment){
          ?>
       
          <div class="c_text" ><img src="<?=base_url($comment->profile_pic)?>" width="40" hright="40" alt="" class="img_circle">  <span><?=$comment->description?></span></div>

          <?php }
      }else{
      ?>
    
       <div class="text-danger c_text"> No Comment Found</div>
      <?php
      }
    }

    public function registration_form(){
       $user_type = $this->input->post('user_type');
       $states =$this->stateinfo();
       ?>
              <div class="form-group">
                 <label for="name"><?=$user_type?> Name *</label>
                 <input type="text"  id="name" name="name" placeholder="Please Enter <?=$user_type?> Name ">
               </div>
               <div class="form-group">
                   <label for="email"><?=$user_type?> Email *</label>
                   <input type="email"  id="email" name="email" placeholder="Please Enter <?=$user_type?> Email ">
               </div>
               <div class="form-group">
                   <label for="contact"><?=$user_type?> Mobile No. *</label>
                   <input type="text"  id="contact" name="contact" placeholder="Please Enter <?=$user_type?> Mobile No. " maxlength="10" minlength="10"  oninput="this.value = this.value.replace(/[^0-9]/g, '').replace(/(\..*)\./g, '$1');">
               </div>
               <div class="form-group">
                   <label for="address"><?=$user_type?> Address *</label>
                   <textarea type="text" class="dropdown_css" id="address" name="address" placeholder="Please Enter Address"></textarea>
               </div>
               <div class="form-group">
                   <label for="state">State*</label>
                   <select class="dropdown_css"  id="state" name="state" onchange="getCity(this.value)">
                       <option value="">Choose State</option>
                       <?php foreach($states as $state){?>
                       <option value="<?=$state->id?>"><?=$state->name?></option>
                       <?php  } ?>
                   </select>
               </div>
               <div class="form-group">
                   <label for="city">City*</label>
                   <select class="city dropdown_css" id="city" name="city">
                     <option value="">Choose City</option>
                   </select>
               </div>
               <div class="form-group">
                   <label for="profile_pic"><?=$user_type?> Profile Pic</label>
                   <input type="file" class="form-control" id="profile_pic" name="profile_pic" >
               </div>
               <div class="form-group">
                   <label for="password">Password*</label>
                   <input type="password" class="" id="password" name="password" placeholder="Please Enter Password">
                   <span class="password-show"><i class="icon-76"></i></span>
               </div>
               <!-- <div class="form-group chekbox-area">
                   <div class="edu-form-check">
                       <input type="checkbox" id="terms-condition">
                       <label for="terms-condition">I agree the User Agreement and <a href="terms-condition.html">Terms & Condition.</a> </label>
                   </div>
               </div> -->
               <div class="form-group">
                   <button type="submit" class="edu-btn btn-medium">Create Account <i class="icon-4"></i></button>
               </div>
       <?php
     }

     public function checkEmail(){
      $email = $this->input->post('email');
      $user = $this->user_model->get_user(array('users.email' => $email,'users.status' => 1));
      if($user){  
             echo json_encode(['status'=>200, 'message'=>'Email Id Match please..']);
      }else{
            echo json_encode(['status'=>302, 'message'=>'Invalid email id!']);
      }
     }
     
     
      public function checkOTP(){
        $email = $this->session->userdata('otp_email');
        $otp = $this->input->post('otp');
 
      $user = $this->user_model->get_user(array('users.email' => $email,'users.status' => 1,'otp'=>$otp));
      if($user){ 
          if($this->session->userdata('otp_email')){
            echo json_encode(['status'=>200, 'message'=>'OTP Match Success']);
          }
      }else{
          echo json_encode(['status'=>302, 'message'=>'Invalid OTP!']);
      }
     }

     public function sendOTP(){
      $email = $this->input->post('email');
      if(empty($email)){
        echo json_encode(['status'=>403, 'message'=>'Please enter email ID!']);
        exit();
      }
      $user = $this->user_model->get_user(array('users.email' => $email,'users.status' => 1));
      if($user){
        $otp = rand ( 100000 , 999999 );
      // echo $user->id;
          $data = array(
            'otp' => $otp,
        );
         $update_otp = $this->user_model->update_user_status($data,array('id'=>$user->id));
          if($update_otp){
          $email = $user->email;
          //$site_name  =   $this->input->post('site_name');
          $subject  =  "Reset Password" ;
          $html = "Your reset password otp is ".$otp ;
          $sendmail = sendEmail($email,$subject,$html);
          if($sendmail){
            $session = array(
             'otp_email' => $email,
            );
            $this->session->set_userdata($session);
    
            echo json_encode(['status'=>200, 'message'=>'OTP Send Successfully Please check email ID..!']);
        }else{
            echo json_encode(['status'=>403, 'message'=>'Mail not send please try again!']);    
        }  
      } else{
        echo json_encode(['status'=>403, 'message'=>'something went wrong']);
      }
     }else{
      echo json_encode(['status'=>403, 'message'=>'Invalid email id!']);
      }
     }

     public function resetPassword(){
      $email = $this->input->post('email');
      $otp = $this->input->post('otp');
      $password = $this->input->post('password');
      $confirmpassword = $this->input->post('confirmpassword');
      $user = $this->user_model->get_user(array('users.email' => $email,'users.status' => 1));
      if(empty($otp)){
        echo json_encode(['status'=>403, 'message'=>'Please enter OTP!']);
        exit();
      }
      if(empty($password)){
        echo json_encode(['status'=>403, 'message'=>'Please enter Password!']);
        exit();
      }
      if(empty($confirmpassword)){
        echo json_encode(['status'=>403, 'message'=>'Please enter COnfirm Password!']);
        exit();
      }
      if($user->otp!=$otp){
        echo json_encode(['status'=>403, 'message'=>'Please enter valid otp!']);
        exit();
      }
      if($password!=$confirmpassword){
        echo json_encode(['status'=>403, 'message'=>'Confirm Password and password not match!']);
        exit();
      }

      $data =array(
        'password'=>md5($password),
        'otp'=>0
      );
      $update = $this->user_model->update_user_status($data,array('id'=>$user->id));
      if($update){
      
        echo json_encode(['status'=>200, 'message'=>'Password Change successfully please login!']);
    }else{
        echo json_encode(['status'=>403, 'message'=>'Password Change failed']);    
    }
      
     }

     public function getCourses(){
      $courseText = $this->input->post('course');
     if(!empty($courseText)){
      $courses = $this->course_model->get_course_by_text(array('courses.course' => $courseText));
      if($courses){
     foreach($courses as  $course){
    ?>
    <p><a href="javascript:void(0)" onclick="fill('<?=$course->course?>')"><?=$course->course?></a></p>
    <?php
     }
      }
     }
   
     }

     public function getavgCourses(){
       $courseText = $this->input->post('course');
      if($courseText =='other'){
       ?>
       
           <input class="form-control" type="text" name="category_name" id="category_name"  placeholder="Course Name">
       
       <?php
      }
     if(!empty($courseText)){
      $avgcourses = $this->course_model->get_course_by_text(array('courses.course' => $courseText));
      if($avgcourses){
     foreach($avgcourses as  $course){
    ?>
    
    <p><a href="javascript:void(0)" id="<?=$course->id?>" onclick="avgfill('<?=$course->course?>')"><?=$course->course?></a></p>
    
    <?php
     } ?>
     <p><a href="javascript:void(0)" onclick="avgfill('<?='other'?>')"><?='Other'?></a></p>
     <?php }
     }
   
     }

     public function getInstitutes(){
       $courseText = $this->input->post('course');
      if(!empty($courseText)){
       $institutes = $this->course_model->get_institute_bycourse(array('courses.course' => $courseText));
       if($institutes){
        ?>
         <option value="">Select Institute</option>
        <?php
      foreach($institutes as  $institute){
      ?>
      <option value="<?=$institute->name?>"><?=$institute->name?></option>
      <?php
      }
       }
      }


}


public function avglistForm(){
  $location = $this->input->post('location');
  $courses = $this->setting_model->get_avg_courses(array('city' => $location));
  //print_r($courses);
  ?>
   <div class="row">
    <div class="col-md-6">
      <select class="" name="courses" id="courses" onchange="get_avg_course_price_list(this.value,'<?=$location?>')">
        <option value="">Select Course</option>
         <?php
         if(count($courses) > 0){
          foreach( $courses as $course){
          ?>
          <option value="<?=$course->course?>"><?=$course->course?></option>
          <?php } } else{
            ?>
            <option value="">No Course Found</option>
            <?php } ?>
      </select>

    </div>

   </div>
   <hr>
   <div id="show_price_list_data">
    </div>
  <?php
}

public function avgCoursePriceList(){
  $location = $this->input->post('location');
  $course = $this->input->post('course');
  
  $avgs = $this->setting_model->get_avg_course_price(array('course' => $course,'city'=>$location));
  
  ?>
  <div class="row">
    <div class="col-md-12">
      <table>
        <thead>
          <th>Sno</th>
          <th>Institue</th>
          <th>Actual Price</th>
          <th>Avg Price</th>
        </thead>
        <tbody>
          <?php if(count($avgs) > 0){ 
             foreach($avgs as $key=>$avg){
            ?>
          <tr>
            <td><?=$key+1?></td>
            <td><?=$avg->institute?></td>
            <td><?=$avg->price?></td>
            <td><?=$avg->avg_price?></td>
          </tr>
          <?php } } ?>
        </tbody>
      </table>
    </div>

  </div>

  <?php
  
}


}