<?php 
class Users extends MY_Controller 
{
	public function __construct()
	{
	  parent::__construct();
    //$this->not_admin_logged_in();
    $this->load->model('course_model');
    $this->load->model('user_model');
    $this->load->model('setting_model');
	}

	public function index()
  {

    $data['page_title'] = 'Profile';
    $data['user'] = $this->user_model->get_user(array('users.id'=>$this->session->userdata('id')));
    if($this->session->userdata('user_type')=='Student'){
      $data['student'] = $this->user_model->get_student(array('studentID'=>$this->session->userdata('id')));
    }
		$this->website_template('user/profile',$data);
  }
  
  	public function successfull_candidate()
  {

    $data['page_title'] = 'Successfull Candidate';
    $data['users'] = $this->user_model->get_sucessfull_candidate(array('success_candidate.instituteID'=>$this->session->userdata('id'),'success_candidate.status'=>1));
		$this->website_template('user/sucessfull_candidate',$data);
  }
  
  public function create_successfull_candidate(){
      $data['page_title'] = 'Create Successfull Candidate';
	 $this->website_template('user/create_successfull_candidate',$data);
  }
  
  public function store_candidate(){
    $id= $this->session->userdata('id');  
    $name = $this->input->post('name');
    $exam_qualified = $this->input->post('exam_qualified');
    $year = $this->input->post('year');
    $rank = $this->input->post('rank');
    
    	if(empty($name)){
			echo json_encode(['status'=>403, 'message'=>'Please enter candidate name']); 	
			exit();
		}
		
		if(empty($exam_qualified)){
			echo json_encode(['status'=>403, 'message'=>'Please enter candidate exam qualified']); 	
			exit();
		}
		
		if(empty($year)){
			echo json_encode(['status'=>403, 'message'=>'Please enter candidate year']); 	
			exit();
		}
		
	  if(empty($rank)){
			echo json_encode(['status'=>403, 'message'=>'Please enter candidate rank']); 	
			exit();
		}
    
    $this->load->library('upload');
    if($_FILES['picture']['name'] != '')
		{
    $config = array(
      'upload_path' 	=> 'uploads/candidate',
      'file_name' 	=> str_replace(' ','',$name).uniqid(),
      'allowed_types' => 'jpg|jpeg|png|gif|webp',
      'max_size' 		=> '10000000',
    );
        $this->upload->initialize($config);
    if ( ! $this->upload->do_upload('picture'))
      {
          $error = $this->upload->display_errors();
          echo json_encode(['status'=>403, 'message'=>$error]);
          exit();
      }
      else
      {
        $type = explode('.',$_FILES['picture']['name']);
        $type = $type[count($type) - 1];
        $image = 'uploads/candidate/'.$config['file_name'].'.'.$type;
      }
     }else{
      $image = 'public/website/assets/dummy_image.jpg';
    }
    $data = array(
        'instituteID' =>$id,
        'name' =>$name,
        'picture' =>$image,
        'exam_qualified' =>$exam_qualified,
        'year' =>$year,
        'rank' =>$rank,
      );
      
      $store = $this->user_model->store_candidate($data);
      
      if($store){
   
			echo json_encode(['status'=>200, 'message'=>'Candidate added successfully!']);
		}else{
			echo json_encode(['status'=>302, 'message'=>'Something went wrong']);   
		}
  }

  public function update(){
    $id= $this->session->userdata('id');
    $user_type= $this->session->userdata('user_type');
    $name = $this->input->post('name');
	$address= $this->input->post('address');
	

    $user = $this->user_model->get_user(array('users.id'=>$id));

		if(empty($name)){
			echo json_encode(['status'=>403, 'message'=>'Please enter your name']); 	
			exit();
		}

   
		if(empty($address)){
			echo json_encode(['status'=>403, 'message'=>'Please enter your address']); 	
			exit();
		}


    $this->load->library('upload');
    if($_FILES['profile_pic']['name'] != '')
		{
    $config = array(
      'upload_path' 	=> 'uploads/users',
      'file_name' 	=> str_replace(' ','',$name).uniqid(),
      'allowed_types' => 'jpg|jpeg|png|gif|webp',
      'max_size' 		=> '10000000',
    );
        $this->upload->initialize($config);
    if ( ! $this->upload->do_upload('profile_pic'))
      {
          $error = $this->upload->display_errors();
          echo json_encode(['status'=>403, 'message'=>$error]);
          exit();
      }
      else
      {
        $type = explode('.',$_FILES['profile_pic']['name']);
        $type = $type[count($type) - 1];
        $image = 'uploads/users/'.$config['file_name'].'.'.$type;
      }
     }elseif(!empty($user->profile_pic)){
        $image = $user->profile_pic;
     }
    else{
      $image = 'public/website/assets/dummy_image.jpg';
    }
		$data = array(
			'name'        => $name,
			'address'     => $address,
      'profile_pic' => $image,
		);
		$update = $this->user_model->update_user_status($data,array('id'=>$id));
    if($user_type=='Institute'){
      $discription = $this->input->post('discription');
      $facebook_url= $this->input->post('facebook_url');
      $insta_url = $this->input->post('insta_url');
      $twitter_url = $this->input->post('twitter_url');
      $linkedin_url= $this->input->post('linkedin_url');
      $youtube_url= $this->input->post('youtube_url');
      $data = array(
        'site_name'   => $name,
        'discription' => $discription,
        'facebook_url'=> $facebook_url ,
        'insta_url'   => $insta_url ,
        'twitter_url' => $twitter_url,
        'linkedin_url'=> $linkedin_url,
        'youtube_url' => $youtube_url,
      );
      $this->setting_model->update_siteInfo($data);
    }

    if($user_type=='Student'){
      $dob = $this->input->post('dob');
      $whatsappNo = $this->input->post('whatsappNo');
      $current_domain = $this->input->post('current_domain');
      $school_collage_name = $this->input->post('school_collage_name');
      $interested_free_demo = $this->input->post('interested_free_demo');
      $free_demo_class_mode = $this->input->post('free_demo_class_mode');
      $target_exam = $this->input->post('target_exam');
      $target_exam_other = $this->input->post('target_exam_other');
      $skill = $this->input->post('skill');
      $scholarship = $this->input->post('scholarship');

      if(empty($dob)){
        echo json_encode(['status'=>403, 'message'=>'Please enter date of birth']); 	
        exit();
      } 
      if(empty($whatsappNo)){
        echo json_encode(['status'=>403, 'message'=>'Please enter your whatsapp number']); 	
        exit();
      }

      if(empty($current_domain)){
        echo json_encode(['status'=>403, 'message'=>'Please select current domain']); 	
        exit();
      } 
      
      if($current_domain=='Other'){
        if(empty($school_collage_name)){
          echo json_encode(['status'=>403, 'message'=>'Please Enter other domain name']); 	
          exit();
        } 
      }

      if(empty($interested_free_demo)){
        echo json_encode(['status'=>403, 'message'=>'Please Select a free demo']); 	
        exit();
      }

      if(empty($free_demo_class_mode)){
        echo json_encode(['status'=>403, 'message'=>'Please free demo class mode']);  	
        exit();
      } 
      if(empty($target_exam)){
        echo json_encode(['status'=>403, 'message'=>'Please select a target exam']);  	
        exit();
      }

      if($target_exam == 'Other'){
        if(empty($target_exam_other)){
          echo json_encode(['status'=>403, 'message'=>'Please target from other exam']); 	
          exit();
        }
      } 

      if(empty($skill)){
        echo json_encode(['status'=>403, 'message'=>'Please Mention Subject/Course/Skills']); 	
        exit();
      } 
      if(empty($scholarship)){
        echo json_encode(['status'=>403, 'message'=>'Please select a scholarship']); 	
        exit();
      }

      $studentData =array(
        'dob' => $dob,
        'whatsappNo' => $whatsappNo,
        'current_domain' => $current_domain,
        'school_collage_name' => $school_collage_name,
        'interested_free_demo' => $interested_free_demo,
        'free_demo_class_mode' => $free_demo_class_mode,
        'target_exam' => $target_exam,
        'target_exam_other' => $target_exam_other,
        'skill' => $skill,
        'scholarship' => $scholarship,
        'status' => 1,
      );
   
      $this->user_model->update_student($studentData,array('studentID'=>$id));

    }

    $session = array(
       'name' =>$name,
       'profile_pic' =>$image,
    );
    $this->session->set_userdata($session);
		if($update){
   
			echo json_encode(['status'=>200, 'message'=>'Update Profile  successfully!']);
		}else{
			echo json_encode(['status'=>302, 'message'=>'Something went wrong']);   
		}
  }


	public function user_profile()
  {
    $id = base64_decode($this->uri->segment(2));
    $user_type = base64_decode($this->uri->segment(3));
    $data['page_title'] = $user_type.' Profile';
    $data['user'] = $this->user_model->get_user_profile(array('users.id'=>$id),$user_type);
    $data['candidates'] = $this->user_model->get_sucessfull_candidate(array('success_candidate.instituteID'=>$id,'success_candidate.status'=>1));
    //print_r($data['user']);die;
		$this->website_template('user/user-detail',$data);
  }



}