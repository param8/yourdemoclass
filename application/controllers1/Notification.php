<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Notification extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->not_logged_in();
	
	
	}

	public function index(){
		$data['page_title'] = 'Notification';
		if($this->session->userdata('user_type')=='Admin'){
    		$this->admin_template('notification',$data);  
		}else{
		  $this->website_template('notification',$data);
		}
        
	}

    public function redirect_notification(){
        $id =$this->session->userdata('id');
        if($this->session->userdata('user_type')=='Admin'){
          $data=array(
            'admin_status' => 1
        );
      }else{
          $data=array(
            'status' => 1
        );
      }
        $status = $this->notification_model->update_notification($data,$id);
    }

	
}