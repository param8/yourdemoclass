<?php 
class Category_model extends CI_Model 
{

  public function __construct()
  {
      parent::__construct();

  }

  public function get_categories($condition){
    $this->db->select('category.*,users.name');
    $this->db->join('users', 'users.id = category.userID','left');
    $this->db->where($condition);
    return $this->db->get('category')->result();
  }

  public function get_categories_with_counter($condition){
    $this->db->select('category.*,users.name');
    $this->db->join('users', 'users.id = category.userID','left');
    $this->db->where($condition);
    $query = $this->db->get('category')->result_array();
    foreach($query as $key => $value)
    {
       $this->db->select('count(id) as totalCourses');
       $this->db->where('categoryID', $value['id']);
      $query[$key]['course_count'] = $this->db->get('courses')->row_array();
    }  
     return $query;

   }

  public function get_category($condition){
    $this->db->where($condition);
    return $this->db->get('category')->row();
     //echo $this->db->last_query();
  }

  public function store_category($data){
     $this->db->insert('category', $data);
     return $this->db->insert_id();
  }

  public function update_category($data,$condition){
    $this->db->where($condition);
    return $this->db->update('category',$data);
  }


}