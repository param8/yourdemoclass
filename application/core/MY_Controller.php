<?php 

// class MY_Controller extends CI_Controller
// {
// 	public function __construct()
// 	{
// 		parent::__construct();
	
// 	}
// }

class MY_Controller extends CI_Controller 
{
	var $permission = array();

	public function __construct() 
	{
	   parent::__construct();
 
        $this->load->model('Common_model');
		$this->load->model('course_model');
		$this->load->model('category_model');
		$this->load->model('notification_model');
		$this->load->helper('mail');
		if(empty($this->session->userdata('logged_in'))) {
			$session_data = array('logged_in' => FALSE);
			$this->session->set_userdata($session_data);
		}
	}

	public function logged_in()
	{
		$session_data = $this->session->userdata();
		if($session_data['logged_in'] == TRUE) {
			if($session_data['user_type'] == 'Admin')
			{
				redirect('dashboard', 'refresh');	
			}else{
				redirect('home', 'refresh');
			}	
		}
	}

	public function not_logged_in()
	{
		$session_data = $this->session->userdata();
		if($session_data['logged_in'] == FALSE) {
			redirect('home', 'refresh');
		}
	}

	public function not_admin_logged_in()
	{
		$session_data = $this->session->userdata();
		if($session_data['logged_in'] == FALSE) {
			redirect('login', 'refresh');
		}
	}

	public function siteinfo(){
		$siteinfo = $this->Common_model->get_site_info();
	  return $siteinfo;
    
	 }

	 public function allCategory(){
		//$condition = $this->session->userdata('user_type')=='Institute' ? array('courses.userID'=>$this->session->userdata('id'),'courses.status'=>1) : ($this->session->userdata('user_type')=='Student' ? array('courses.locationID'=>$this->session->userdata('city'),'courses.status'=>1):array('courses.status'=>1));
		$allCategory = $this->category_model->get_categories(array('category.status'=>1));
	  return $allCategory;
    
	 }

	 public function aboutinfo(){
		$about_info = $this->Common_model->get_about();
		return $about_info;
	 }

	 public function stateinfo(){
		$stateinfo = $this->Common_model->get_states(array('country_id'=>101));
		return $stateinfo;
	 }

	 public function cityinfo(){
		$cityinfo = $this->Common_model->get_cities();
		return $cityinfo;
	 }

	 
	 public function notifications(){
	     $id = $this->session->userdata('id');
	     $condition = $this->session->userdata('user_type') == 'Admin' ? array('admin_status'=>0) : array('userID' =>$id,'status'=>0);

		$notifications = $this->notification_model->get_notifications($condition);
		//print_r(count($notifications));die;
		return $notifications;
	 }

	 public function totalCourses(){
		$condition = $this->session->userdata('user_type')=='Institute' ? array('courses.userID'=>$this->session->userdata('id'),'courses.status'=>1) : ($this->session->userdata('user_type')=='Student' ? array('courses.locationID'=>$this->session->userdata('city'),'courses.status'=>1):array('courses.status'=>1));
		$totalCourses = $this->course_model->get_courses($condition);
		return $totalCourses;
	 }

	 public function totalBookedCourses(){
		$userID = $this->session->userdata('id');
		$condition = $this->session->userdata('user_type')=='Institute' ? array('courses.userID'=>$this->session->userdata('id')) : ($this->session->userdata('user_type')=='Student' ? array('booked_courses.studentID'=>$this->session->userdata('id')):array('booked_courses.status'=>1)) ;
		$totalBookedCourses = $this->course_model->get_student_booked_course($condition);
		return $totalBookedCourses;
	 }

	

	 public function admin_template($page = null, $data = array())
	 {
		$data['siteinfo'] = $this->siteinfo();
		$data['notifications'] = $this->notifications();
		$this->load->view('admin/layout/head',$data);
		$this->load->view('admin/layout/header');
		$this->load->view('admin/layout/sidebar');
		$this->load->view('admin/'.$page);
		$this->load->view('admin/layout/footer');
	 }

	 public function website_template($page = null, $data = array())
	 {
		$data['siteinfo'] = $this->siteinfo();
		$data['about'] = $this->aboutinfo();
		$data['states'] = $this->stateinfo();
		$data['allCategories'] = $this->allCategory();
		$data['notifications'] = $this->notifications();
		$data['totalCourses'] = $this->totalCourses();
		$data['totalBookedCourses'] = $this->totalBookedCourses();
		$this->load->view('layout/head',$data);
		$this->load->view('layout/header');
		$this->load->view($page);
		$this->load->view('layout/footer');
	 }

}