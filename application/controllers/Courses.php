<?php 
class Courses extends MY_Controller 
{
	public function __construct()
	{
	  parent::__construct();
    //$this->not_admin_logged_in();
    $this->load->model('category_model');
    //$this->load->model('teachers_model');
    $this->load->library("pagination");
    
	}

	public function index()
  {
    // $cat = !empty($this->uri->segment(2)) ? $this->uri->segment(2) : '0';
    // $config = array();
    // $config["base_url"] = base_url().'courses/'.$cat;
    $location = !empty($this->session->userdata('location_city')) ? $this->session->userdata('location_city') :$this->session->userdata('city');
    if(!empty($this->session->userdata('course')) OR !empty($this->session->userdata('start_date')) OR !empty($this->session->userdata('mode')) OR !empty($this->session->userdata('location_state')) OR !empty($this->session->userdata('location_city'))){ 
       $data['category'] = 'All Courses';
      $data['category_id'] = 0;
      //$condition = array('courses.status'=>1);
    }else{
    if(!empty($this->uri->segment(2))){
      $categoryID = base64_decode($this->uri->segment(2));
      $category = $this->category_model->get_category(array('id' => $categoryID));
      //print_r($category);
      $lcation_array = !empty($this->session->userdata('location_city')) ? array('courses.categoryID'=>$categoryID,'courses.status'=>1,'courses.locationID' =>$this->session->userdata('location_city'))  :array('courses.categoryID'=>$categoryID,'courses.status'=>1);
      $data['category'] = $category->category;
      $data['category_id'] = $category->id;
      //$condition = $this->session->userdata('user_type')=='Institute' ? array('courses.userID'=>$this->session->userdata('id'),'courses.categoryID'=>$categoryID,'courses.status'=>1) : ($this->session->userdata('user_type')=='Student' ? array('courses.categoryID'=>$categoryID,'courses.status'=>1):$lcation_array);
    }else{
      $lcation_array = !empty($this->session->userdata('location_city')) ? array('courses.status'=>1,'courses.locationID' =>$this->session->userdata('location_city'))  :array('courses.status'=>1);
      $data['category'] = 'All Courses';
      $data['category_id'] = 0;
      //$condition = $this->session->userdata('user_type')=='Institute' ? array('courses.userID'=>$this->session->userdata('id'),'courses.status'=>1) : ($this->session->userdata('user_type')=='Student' ? array('courses.status'=>1):$lcation_array);
    }
    
    }
   
    $data['page_title'] = 'Courses';

		$this->website_template('courses/courses',$data);
  }

  public function course_detail(){
    $this->not_logged_in();
    $userID = $this->session->userdata('id');
    $courseID = base64_decode($this->uri->segment(2));
    $condition = $this->session->userdata('user_type')=='Institute' ? array('courses.userID'=>$this->session->userdata('id'),'courses.status'=>1,'courses.id <>' => $courseID) : ($this->session->userdata('user_type')=='Student' ? array('courses.locationID'=>$this->session->userdata('city'),'courses.status'=>1,'courses.id <>' => $courseID):array('courses.status'=>1,'courses.id <>' => $courseID)) ;
    $data['page_title'] = 'Courses Deatil';
    $course = $this->course_model->get_course(array('courses.id' => $courseID));
    $data['course'] = $course;
    $data['ratings'] = $this->course_model->get_ratings(array('course_rating.courseID' => $courseID));
    $data['ratings_five'] = $this->course_model->get_ratings(array('course_rating.courseID' => $courseID,'course_rating.rating'=>5));
    $data['ratings_four'] = $this->course_model->get_ratings(array('course_rating.courseID' => $courseID,'course_rating.rating'=>4));
    $data['ratings_three'] = $this->course_model->get_ratings(array('course_rating.courseID' => $courseID,'course_rating.rating'=>3));
    $data['ratings_two'] = $this->course_model->get_ratings(array('course_rating.courseID' => $courseID,'course_rating.rating'=>2));
    $data['ratings_one'] = $this->course_model->get_ratings(array('course_rating.courseID' => $courseID,'course_rating.rating'=>1));
    $data['courses'] = $this->course_model->get_more_courses($condition);
    $data['instituteCourses'] = $this->course_model->get_more_courses(array('users.id'=>$course->userID,'courses.status'=>1,'courses.id!='=>$courseID));
    $data['booked_courses'] = $this->course_model->get_count_booked_course(array('courseID' => $courseID,'status'=>1));
    $data['booked'] = $this->session->userdata('user_type')=='Student' ? $this->course_model->get_count_booked_course(array('studentID' =>$this->session->userdata('id'),'status'=>1)) : '';
        //print_r($data['booked']);die;

    $data['dublicateRatings'] = $this->course_model->get_dubliccate_ratings(array('course_rating.courseID' => $courseID,'course_rating.userID'=>$userID));
   // $data['teacher'] = $this->teachers_model->get_teacher(array('teachers.subjectID'=>$data['course']->subject));
    $this->website_template('courses/courseDetail',$data);
  }

	public function booked_course()
  {
    $this->not_logged_in();
    
    $data['page_title'] = 'Booked Courses';
    $userID = $this->session->userdata('id');
    $condition_courses = $this->session->userdata('user_type')=='Institute' ? array('courses.userID'=>$this->session->userdata('id'),'courses.status'=>1) : ($this->session->userdata('user_type')=='Student' ? array('courses.locationID'=>$this->session->userdata('city'),'courses.status'=>1):array('courses.status'=>1));
    $data['courses'] = $this->course_model->get_courses($condition_courses);
		$this->website_template('courses/booked-courses',$data);
  }

  public function course_form(){
    //print_r($_POST); die;
    
    $user_type = 'Student'; 
    $name = $this->value->post('name');
    $course_name = $this->input->post('course_name');
    $institute = $this->input->post('institute_name');
    $city = $this->input->post('institute_city');
    $mode = $this->input->post('classemode');
    $price = $this->input->post('course_price');

     if(empty($name)){
      echo json_encode(['status'=>403, 'message'=>'Please Enter Name']);  	
      exit();
    }

    if(empty($course_name)){
      echo json_encode(['status'=>403, 'message'=>'Please Enter Course']);  	
      exit();
    }
  
    if(empty($institute)){
      echo json_encode(['status'=>403, 'message'=>'Please Enter Institute']);  	
      exit();
    }
    if(empty($city)){
      echo json_encode(['status'=>403, 'message'=>'Please Enter city']); 	
      exit();
    }
    if(empty($mode)){
      echo json_encode(['status'=>403, 'message'=>'Please Select Mode']); 	
      exit();
    }      
    if(empty($price)){
      echo json_encode(['status'=>403, 'message'=>'Please Enter price']);   	
      exit();
    }
  
    $store_institute = array(
      'name' => $name,
      'user_type' => $user_type,
      'course' => $course_name,
      'institute' => $institute,
      'city' => $city,
      'mode' => $mode,
      'price' => $price,
    );
  //print_r($store_institute);die;
    $store = $this->course_model->store_course_instittute($store_institute);
      if($store){
     echo json_encode(['status'=>200, 'message'=>'Detail added successfully!']);
     }else{
        echo json_encode(['status'=>302, 'message'=>'Something wrong happened']);   
      }
     }

  public function createCourse(){
    $this->not_logged_in();
    $data['page_title'] = 'Create Course';
    $data['categories'] = $this->category_model->get_categories(array('category.status'=>1));
    $data['batchs'] = $this->Common_model->get_batch();
		$this->website_template('courses/create-course',$data);
  }

  public function store(){
    $userID = $this->session->userdata('id');
    $locationID = $this->session->userdata('city');
    $course = $this->input->post('course');
    $free_demo = $this->input->post('free_demo');
    $subject = $this->input->post('subject');
    $categoryID = $this->input->post('category');
    $mode = $this->input->post('mode');
    $start_date = $this->input->post('start_date');
    $end_date = $this->input->post('end_date');
    $seats = $this->input->post('seats');
    $fees = $this->input->post('fees');
    $discount = $this->input->post('discount');
    $teacher = $this->input->post('teacher');
    $description = $this->input->post('description');
    $batch = $this->input->post('batch');
    $check = $this->course_model->get_course(array('courses.course'=>$course,'courses.userID'=>$userID));
    // if($check){
    //   echo json_encode(['status'=>403,'message'=>'This course already exists']);
    //   exit();
    // }

    if(empty($categoryID)){
      echo json_encode(['status'=>403,'message'=>'Please select a category']);
      exit();
    }

    if($categoryID=='Other'){
      if(empty($this->input->post('category_name'))){
        echo json_encode(['status'=>403,'message'=>'Please Enter category Name']);
        exit();
      }
    }
    if(empty($course)){
      echo json_encode(['status'=>403,'message'=>'Please enter a course']);
      exit();
    }
  
    if(empty($seats)){
      echo json_encode(['status'=>403,'message'=>'Please enter a seats']);
      exit();
    }
    if(empty($start_date)){
      echo json_encode(['status'=>403,'message'=>'Please enter a start date']);
      exit();
    }
    if(empty($end_date)){
      echo json_encode(['status'=>403,'message'=>'Please enter a end date']);
      exit();
    }
    if(empty($batch)){
      echo json_encode(['status'=>403,'message'=>'Please enter  batch time']);
      exit();
    }
    if(empty($description)){
      echo json_encode(['status'=>403,'message'=>'Please enter a description']);
      exit();
    }
    $this->load->library('upload');
    if($_FILES['image']['name'] != '')
		{
    $config = array(
      'upload_path' 	=> 'uploads/courses',
      'file_name' 	=> str_replace(' ','',$course.$userID).uniqid(),
      'allowed_types' => 'jpg|jpeg|png|gif',
      'max_size' 		=> '10000000',
    );
        $this->upload->initialize($config);
    if ( ! $this->upload->do_upload('image'))
      {
          $error = $this->upload->display_errors();
          echo json_encode(['status'=>403, 'message'=>$error]);
          exit();
      }
      else
      {
        $type = explode('.',$_FILES['image']['name']);
        $type = $type[count($type) - 1];
        $image = 'uploads/courses/'.$config['file_name'].'.'.$type;
      }
    }else{
      $image = 'public/dummy_image.jpg';
    }
   
    //$batchs = implode(',',$batch);

    if($categoryID=='Other'){
     $categoryName  =  $this->input->post('category_name');
     $category = $this->category_model->get_category(array('category'=>$categoryName));
     if($category){
      echo json_encode(['status'=>403, 'message'=>'Category already exits!']);
      exit();
     }else{
      $catData = array(
        'userID' =>$this->session->userdata('id'),
        'category'=>$categoryName,
        'status' =>1,
      ) ;
      $categoryID = $this->category_model->store_category($catData);
    }
       
    }

   $data = array(

    'userID' => $userID,
    'locationID' => $locationID,
    'course' => $course,
    'free_demo' => $free_demo,
    'subject' => $subject,
    'categoryID' => $categoryID,
    'mode' => $mode,
    'start_date' => $start_date,
    'end_date' => $end_date,
    'seats' => $seats,
    'remaning_seats' => $seats,
    'fees' => $fees,
    'discount' => $discount,
    'description' => $description,
    'teacher' => $teacher,
    'batch' => $batch,
    'image' => $image,
   );

   $store = $this->course_model->store_course($data);
   if($store){
         $email = $this->session->userdata('email');
          //$site_name  =   $this->input->post('site_name');
          $subject  =   "Course Add " ;
          $html = "<h1> Your ".$course." is submimited </h1>
          <p>Please wait for approve.. </p>";
          $sendmail = sendEmail($email,$subject,$html);

          $email = 'param.chandel@gmail.com';
          //$site_name  =   $this->input->post('site_name');
          $subject  =  $this->session->userdata('name')." is Course Add " ;
          $html = "<h1>  ".$course." is added by ".$this->session->userdata('name')." wait for approve</h1>";
          $sendmail = sendEmail($email,$subject,$html);
     echo json_encode(['status'=>200, 'message'=>'Course added successfully!']);
	 }else{
			echo json_encode(['status'=>302, 'message'=>'Something wrong happened']);   
		}

  }

  public function store_rating(){
    $userID = $this->session->userdata('id');
    $courseID = $this->input->post('courseID');
    //$title = $this->input->post('title');
    $description = $this->input->post('description');
    $rating = $this->input->post('rating_course');

    if(empty($rating)){
      echo json_encode(['status'=>403,'message'=>'Please select rating']);
      exit();
    }

    // if(empty($title)){
    //   echo json_encode(['status'=>403,'message'=>'Please enter title']);
    //   exit();
    // }

    if(empty($description)){
      echo json_encode(['status'=>403,'message'=>'Please enter description']);
      exit();
    }

    $data = array(
      'userID'      => $userID,
      'courseID'    => $courseID,
      //'title'       => $title,
      'description' => $description,
      'rating'      => $rating,
    );
   
    $store = $this->course_model->store_rating($data);
    if($store){
      echo json_encode(['status'=>200, 'message'=>'Your rating send successfully!']);
    }else{
       echo json_encode(['status'=>302, 'message'=>'something wrong happened']);   
     }
  }

  public function addStudentCourse(){
    $userID = $this->session->userdata('id');
    $user_type = $this->session->userdata('user_type');
    $courseID = $this->input->post('courseID');
    $price = $this->input->post('price');
    $batch = $this->input->post('batch');
    $course = $this->course_model->get_course(array('courses.id' =>$courseID));
    if(empty($batch)){
      echo json_encode(['status'=>403,'message'=>'Please select batch']);
      exit();
    }
    $data = array(
      'studentID' =>$userID,
      'courseID' =>$courseID,
      'price' =>$price,
      'batch' =>$batch,
    );
  
    $store = $this->course_model->store_student_course($data);
    if($store){
      $message = '<a href="'.base_url('user-profile/'.base64_encode($userID).'/'.base64_encode($user_type)).'">'.$this->session->userdata('name').'</a>'." has sent request to book course ".$course->course;
      $notification = array(
        'userID' => $course->userID,
        'send_by' => $this->session->userdata('id'),
        'message' => $message
      );
      
      $store_notification = $this->notification_model->store_notification($notification);
      $email = $this->session->userdata('email');
      //$site_name  =   $this->input->post('site_name');
      $subject  =   "Course Add " ;
      $html = "<h1>Thanks for booked course.</h1>
      <p>We approve some time please wait </p>";
      $sendmail = sendEmail($email,$subject,$html);
      echo json_encode(['status'=>200, 'message'=>'Course booked  successfully!']);
    }else{
       echo json_encode(['status'=>302, 'message'=>'something wrong happened']);   
     }

  }

  public function studentCourseStatus(){
    $id = $this->input->post('id');
    $courseID = $this->course_model->get_booked_course(array('id' =>$id));
    $course = $this->course_model->get_course(array('courses.id'=>$courseID->courseID));
    $data = array(
      'status' => 1
    );

    $update= $this->course_model->updateStudentCourse($data,$id);
    if($update){
      if($course->remaning_seats > 0){
        $seats = $course->remaning_seats-1;
        $courseData = array(
          'remaning_seats' => $seats,
        );

        $this->course_model->updateCourse($courseData,$course->id);
        $message = $this->session->userdata('name')." has accepted your booking request course ".$course->course;
        $notification = array(
          'userID' => $courseID->studentID,
          'send_by' => $this->session->userdata('id'),
          'message' => $message
        );
        
        $store_notification = $this->notification_model->store_notification($notification);
          $email = $course->user_email;
          //$site_name  =   $this->input->post('site_name');
          $subject  =   "Your Course Approved " ;
          $html = "<h1>".$course->course." is approved </h1>
          <p>Please continue to study.. </p>";
          //$sendmail = sendEmail($email,$subject,$html);
      }
      echo json_encode(['status'=>200, 'message'=>'Booked status active successfully!']);
    }else{
       echo json_encode(['status'=>302, 'message'=>'something wrong happened']);   
     }
  }

  public function average_courses(){
    $data['page_title'] = 'Average Courses';
    $this->website_template('courses/average_courses',$data);
    }

    public function get_avgCourses(){
    
      $value = "";
      if(!empty($this->input->post('value'))){
        $value = $this->input->post('value');
      }

      $onlineAvg_courses= $this->course_model->get_avgCourses(array('status' => 1,'mode' => 'online'),$value);
       $offlineAvg_courses = $this->course_model->get_avgCourses(array('status' => 1, 'mode' => 'offline'),$value);
      ?>
<div class=" Online_Courses  p-3">
    <h3> Online Courses</h3>


    <div class="tabledesign">
        <div class="course_data border">
            <div class="course top">
                <span>Name </span>
                <span>Course Name </span>
                <span>Institute </span>
                <span>City </span>
                <span>Price </span>
                <span>Deal</span>
            </div>
            <?php foreach($onlineAvg_courses as $onlineAvg_course){?>
            <div class="course">
                <span><?=$onlineAvg_course->name;?> </span>
                <span><?=$onlineAvg_course->course;?> </span>
                <span><?=$onlineAvg_course->institute;?> </span>
                <span><a class="btn btnlg" href="<?=base_url('city-courses/'.base64_encode($onlineAvg_course->city))?>"><?=$onlineAvg_course->city;?></a> </span>
                <span><?=$onlineAvg_course->price;?> </span>
                <span>
                    <?=$onlineAvg_course->deal_status == 1 ? '<a href=""  class="btn btnbg" > <span class="blink">Best Deal</span></a>' : '' ?></span>
            </div>
            <?php } ?>
            <!--  -->
        </div>
    </div>

</div>
<div class="Offline_Courses  p-3">
    <h3> Offline Courses</h3>
    <div class="tabledesign">
        <div class="course_data border">
            <div class="course top">
                <span>Name </span>
                <span>Course Name </span>
                <span>Institute </span>
                <span>City </span>
                <span>Price </span>
                <span>Deal</span>
            </div>
            <?php foreach($offlineAvg_courses as $offlineAvg_course){?>
            <div class="course">
                <span><?=$offlineAvg_course->name;?> </span>
                <span><?=$offlineAvg_course->course;?> </span>
                <span><?=$offlineAvg_course->institute;?> </span>
                <span><a class="btn btnlg" href="<?=base_url('city-courses/'.base64_encode($offlineAvg_course->city))?>"><?=$offlineAvg_course->city;?></a> </span>
                <span><?=$offlineAvg_course->price;?> </span>
                <span><?=$offlineAvg_course->deal_status == 1 ? '<a href=""  class="btn btnbg" > <span class="blink">Best Deal</span></a>' : '' ?></span>
            </div>
            <?php } ?>
            <!--  -->
        </div>
        <!-- <div class="table-responsive">
            <table class="table">
                <thead>
                    <tr>
                        <th>Name </th>
                        <th>Course Name </th>
                        <th>Institute </th>
                        <th>City </th>
                        <th>Price </th>
                        <th>Deal </th>
                    </tr>
                </thead>
                <tbody>
                    <?php //foreach($offlineAvg_courses as $offlineAvg_course){?>
                    <tr>
                        <td><?//=$offlineAvg_course->name;?> </td>
                        <td><?//=$offlineAvg_course->course;?> </td>
                        <td><?//=$offlineAvg_course->institute;?> </td>
                        <td> <a href=""><?//=$offlineAvg_course->city;?></a> </td>
                        <td><?//=$offlineAvg_course->price;?> </td>
                        <td><?//=$offlineAvg_course->deal_status == 1 ? ' <span class="blink">Best Deal</span>' : '' ?>
                        </td>
                    </tr>
                    <?//php } ?>
                </tbody>
            </table>
        </div> -->
    </div>

</div>
<?php

    } 

    public function city_course(){
     
      $data['page_title'] = 'City Courses';
      $data['cityName'] = $this->uri->segment(2);
      $this->website_template('courses/city-courses',$data);
      
    }
  

}