<?php 
class User extends MY_Controller 
{
	public function __construct()
	{
		parent::__construct();
	 if($this->session->userdata('user_type')!='Admin'){
          redirect('home');
      }
		$this->not_admin_logged_in();
		$this->load->model('user_model');
	}

	public function index()
	{	$data['page_title'] = 'Institutes';
		$data['siteinfo'] = $this->siteinfo();
        $data['users'] = $this->user_model->get_all_users(array('users.user_type' => 'Institute'));
		$this->admin_template('users',$data);
	}

	public function students()
	{	$data['page_title'] = 'Students';
		$data['siteinfo'] = $this->siteinfo();
        $data['users'] = $this->user_model->get_all_users(array('users.user_type' => 'Student'));
		$this->admin_template('users',$data);
	}

	public function experts()
	{	$data['page_title'] = 'Experts';
		$data['siteinfo'] = $this->siteinfo();
        $data['users'] = $this->user_model->get_all_users(array('users.user_type' => 'Experts'));
		$this->admin_template('users',$data);
	}

	public function update_status()
	{
		$user_id = $this->input->post('userid');
		$user_status = ($this->input->post('user_status')=='Active')?'1':'0';

        $userdata =  array(
          'status' => $user_status
        );
	
        $update_status =  $this->user_model->update_user_status($userdata,array('id'=>$user_id));
       if($user_status==1){
          $message = "Admin approved you";
          $data = array(
            'userID' => $user_id,
            'send_by' => $this->session->userdata('id'),
            'message' => $message
          );
          $store_notification = $this->notification_model->store_notification($data);
        }
	}

	public function viewUser(){
		$userid = $this->input->post('userid');
		$user = $this->user_model->get_user_details(array('users.id'=>$userid));
		?>
			<div class="box mb-0">
				<div class="box-body box-profile">            
					<div class="row">
						<div class="col-12">
							<h4 class="box-inverse p-2">Personal Information</h4>
							<div>
								<p><strong>Name</strong> :<span class="text-gray pl-10"><?=$user->name;?></span> </p>
								<p><strong>Email</strong> :<span class="text-gray pl-10"><?=$user->email;?></span> </p>
								<p><strong>Phone</strong> :<span class="text-gray pl-10"><?=$user->contact;?></span></p>
								<p><strong>Address</strong> :<span class="text-gray pl-10"><?=$user->address;?> </span></p>
								<p><strong>State</strong> :<span class="text-gray pl-10"><?=$user->stateName;?></span></p>
								<p><strong>City</strong> :<span class="text-gray pl-10"><?=$user->cityName;?></span></p>
							</div>
							
							
						</div>
					</div>
				</div>
				<!-- /.box-body -->
			</div>
		<?php
	  }
	
}