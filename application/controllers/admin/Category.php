<?php 
class Category extends MY_Controller 
{
	public function __construct()
	{
		parent::__construct();
		 if($this->session->userdata('user_type')!='Admin'){
          redirect('home');
      }
    $this->not_admin_logged_in();
		$this->load->model('category_model');
    
	}

	public function index()
	{	
		$data['page_title'] = 'Category';
		$data['categories'] = $this->category_model->get_categories(array());
    $this->admin_template('category',$data);
	}

	public function store(){
		$category = $this->input->post('category');

    if(empty($category)){
      echo json_encode(['status'=>403,'message'=>'Please enter category']);
      exit();
    }

    $categoryCheck= $this->category_model->get_category(array('category' =>$category));
    if($categoryCheck){
      echo json_encode(['status'=>403,'message'=>'Category already exists']);
      exit();
    }
	  $status = $this->session->userdata('user_type')=='Admin' ? 1 : 0;
    $data = array(
      'userID' =>$this->session->userdata('id'),
      'category'  => $category,
      'status' =>$status
    );

    $store = $this->category_model->store_category($data);
    if($store){
      echo json_encode(['status'=>200, 'message'=>'Add category successfully...']);
    }else{
      echo json_encode(['status'=>302, 'message'=>mysqli_error()]);
    }
    
	}

  public function editForm(){
    $id = base64_decode($this->input->post('id'));
    $category = $this->category_model->get_category(array('id'=>$id));
    ?>
          <div class="form-group">
            <label for="name" class="col-form-label">Category:</label>
            <input type="text" class="form-control" name="category" id="category" value="<?=$category->category ?>">
          </div>
       
          <input type="hidden" name="id" id="id" value="<?=$id?>">
   
    <?php
  }

  public function update(){
    $id = $this->input->post('id');
    $category = $this->input->post('category');
    $categoryCheck= $this->category_model->get_category(array('category' =>$category, 'id <>' =>$id));
		
		if(empty($category)){
		  echo json_encode(['status'=>403, 'message'=>'Please enter a category']);
      exit();
		}

    if($categoryCheck){
      echo json_encode(['status'=>403,'message'=>'Category already exists']);
      exit();
    }

    $data = array(
      'category' => $category,
    );
  
    $update = $this->category_model->update_category($data,array('id'=>$id));
    if($update){
      echo json_encode(['status'=>200, 'message'=>'Update category successfully...']);
    }else{
      echo json_encode(['status'=>302, 'message'=>mysqli_error()]);
    }
    
	}

  public function delete(){
    $id = base64_decode($this->input->post('id'));
    $status = $this->input->post('status');
    $data =  array(
      'status' => $status
    );
    $update = $this->category_model->update_category($data,array('id'=>$id));
    
    if($update){
      echo true;
    }


  }


	
}