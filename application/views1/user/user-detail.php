<style>
    body{
    background: -webkit-linear-gradient(left, #3931af, #00c6ff);
}
.emp-profile{
    padding: 3%;
    margin-top: 3%;
    margin-bottom: 3%;
    border-radius: 0.5rem;
    background: #fff;
}
.profile-img{
    text-align: center;
}
.profile-img img {
       width: 200px;
    height: 200px;
    box-shadow: 0 1px 2px 0px #bbafaf;
}
.profile-img .file {
    position: relative;
    overflow: hidden;
    margin-top: -20%;
    width: 70%;
    border: none;
    border-radius: 0;
    font-size: 15px;
    background: #212529b8;
}
.profile-img .file input {
    position: absolute;
    opacity: 0;
    right: 0;
    top: 0;
}
.profile-head h5{
    color: #333;
}
.profile-head h6{
    color: #0062cc;
}
.profile-edit-btn{
    border: none;
    border-radius: 1.5rem;
    width: 70%;
    padding: 2%;
    font-weight: 600;
    color: #6c757d;
    cursor: pointer;
}
.proile-rating{
    font-size: 12px;
    color: #818182;
    margin-top: 5%;
}
.proile-rating span{
    color: #495057;
    font-size: 15px;
    font-weight: 600;
}
.profile-head .nav-tabs{
    margin-bottom:3%;
}
.profile-head .nav-tabs .nav-link{
    font-weight:600;
    border: none;
}
.profile-head .nav-tabs .nav-link.active{
    border: none;
    border-bottom:2px solid #0062cc;
}
.profile-work{
    padding: 14%;
    margin-top: -15%;
}
.profile-work p{
    font-size: 12px;
    color: #818182;
    font-weight: 600;
    margin-top: 10%;
}
.profile-work a{
    text-decoration: none;
    color: #495057;
    font-weight: 600;
    font-size: 14px;
}
.profile-work ul{
    list-style: none;
}
.profile-tab label{
    font-weight: 600;
}
.profile-tab p{
    font-weight: 600;
    color: #0062cc;
}
.u_img{
        background-color: #1ab69d;    padding-top: 11px;
}

.p_l{
    padding-left: 21px;
}
.m_l{
    margin-left: 21px;
}
.profile-img .social-share li a:hover {
    background-color: var(--color-tertiary)!important;
}

</style>


<div class="edu-breadcrumb-area">
  <div class="container">
    <div class="breadcrumb-inner">
      <div class="page-title">
        <h1 class="title"><?=$page_title?></h1>
      </div>
      <ul class="edu-breadcrumb">
        <li class="breadcrumb-item"><a href="<?=base_url('home')?>">Home</a></li>
        <li class="separator"><i class="icon-angle-right"></i></li>
        <li class="breadcrumb-item"><a href="#"><?=$page_title?></a></li>
      </ul>
    </div>
  </div>
  <ul class="shape-group">
    <li class="shape-1">
      <span></span>
    </li>
    <li class="shape-2 scene"><img data-depth="2" src="<?=base_url('public/website/assets/images/about/shape-13.png')?>" alt="shape"></li>
    <li class="shape-3 scene"><img data-depth="-2" src="<?=base_url('public/website/assets/images/about/shape-15.png')?>" alt="shape"></li>
    <li class="shape-4">
      <span></span>
    </li>
    <li class="shape-5 scene"><img data-depth="2" src="<?=base_url('public/website/assets/images/about/shape-07.png')?>" alt="shape"></li>
  </ul>
</div>
 <!---new-detial---->
                
             <div class="container emp-profile">
            <form method="post">
                <div class="row">
                    <div class="col-md-4 u_img">
                        <div class="profile-img">
                           
                              <img src="<?=base_url($user->profile_pic)?>" alt="user" class="rounded-circle"   height="200" width="200" >
                            <!--<div class="file btn btn-lg btn-primary">-->
                            <!--    Change Photo-->
                            <!--    <input type="file" name="file"/>-->
                            <!--</div>-->
                              <?php if($user->user_type=='Institute'){?>
                            <ul class="social-share justify-content-center mt-3 ">
                                <!-- <li><a href=""><i class="icon-share-alt"></i></a></li> -->
                                <li><a class="text-white" href="<?=!empty($user->userFacebookURL) ? $user->userFacebookURL : 'javascript:void(0)' ?>"><i class="icon-facebook"></i></a></li>
                                <li><a class="text-white" href="<?=!empty($user->userTwitterURL) ? $user->userTwitterURL : 'javascript:void(0)' ?>"><i class="icon-twitter"></i></a></li>
                                <li><a class="text-white" href="<?=!empty($user->userLinkedInURL) ? $user->userLinkedInURL : 'javascript:void(0)' ?>"><i class="icon-linkedin2"></i></a></li>
                            </ul>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="profile-head p_l">
                                    <h5 class="m-0">
                                       <?=$user->name?>
                                    </h5>
                                    <h6 class="m-0">
                                       <?=$user->user_type?>
                                    </h6>
                                    
                                      
                                    <!--<p class="proile-rating m-0">RANKINGS : <span>8/10</span></p>-->
                            <ul class="nav nav-tabs" id="myTab" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">About</a>
                                </li>
                                <!--<li class="nav-item">-->
                                <!--    <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Timeline</a>-->
                                <!--</li>-->
                            </ul>
                        </div>
                          <!----detail-sec---->
                       <div class="tab-content m_l profile-tab card p-3" id="myTabContent">
                            <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <label>User Type</label>
                                            </div>
                                            <div class="col-md-6">
                                                <p><?=$user->user_type ?></p>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <label>Name</label>
                                            </div>
                                            <div class="col-md-6">
                                                <p><?=$user->name?></p>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <label>Email</label>
                                            </div>
                                            <div class="col-md-6">
                                                <p><a href="mailto:<?=$user->email?>" target="_blank"><?=$user->email?></a></p>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <label>Phone</label>
                                            </div>
                                            <div class="col-md-6">
                                                <p><a href="tel:<?=$user->contact?>"> <?=$user->contact?></a></p>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <label>Address</label>
                                            </div>
                                            <div class="col-md-6">
                                                <p><?=$user->address?></p>
                                            </div>
                                        </div>
                            </div>
                            <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <label>Experience</label>
                                            </div>
                                            <div class="col-md-6">
                                                <p>Expert</p>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <label>Hourly Rate</label>
                                            </div>
                                            <div class="col-md-6">
                                                <p>10$/hr</p>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <label>Total Projects</label>
                                            </div>
                                            <div class="col-md-6">
                                                <p>230</p>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <label>English Level</label>
                                            </div>
                                            <div class="col-md-6">
                                                <p>Expert</p>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <label>Availability</label>
                                            </div>
                                            <div class="col-md-6">
                                                <p>6 months</p>
                                            </div>
                                        </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <label>Your Bio</label><br/>
                                        <p>Your detail description</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                     <!----detail-sec---->
                    </div>
                    <!--<div class="col-md-2">-->
                    <!--    <input type="submit" class="profile-edit-btn" name="btnAddMore" value="Edit Profile"/>-->
                    <!--</div>-->
                  
                    
                </div>
              
            </form>           
        </div>
                
                 <!---new-detial---->


        <!--=====================================-->
        <!--=        Team Area Start            =-->
        <!--=====================================-->
  
        <?php if(count($candidates) > 0){ ?>
        <div class="edu-course-area course-area-1 edu-section-gap bg-lighten01">
            <div class="container">
                <div class="section-title section-center" data-sal-delay="150" data-sal="slide-up" data-sal-duration="800">
                    <h2 class="title">Successfull Candidate</h2>
                </div>
                <div class="row g-5">
                    <!-- Start Single Course  -->
              <div class="owl-carousel owl-theme">
                    
                    <?php 
                    if(count($candidates) > 0){
                    foreach($candidates as $candidate){
                    ?>
                    <div class="item" data-sal-delay="100" data-sal="slide-up" data-sal-duration="800">
                        <div class="edu-course course-style-3 course-box-shadow">
                            <div class="inner">
                                <div class="thumbnail">
                                    <a href="javascript:void(0)">
                                        <img src="<?=base_url($candidate->picture)?>" alt="Course Meta" style="width:100%;height:270px;">
                                    </a>
                                    <div class="time-top">
                                        <span class="duration"><i class="icon-64"></i><?=$candidate->rank?> Rank</span>
                                        
                                    </div>
                                </div>
                                <div class="content">
                                     <h5 class="title">
                                        <a href="javascript:void(0)"><?=$candidate->name?></a>
                                    </h5>
                                    <span class="course-level"><i class="icon-34"></i> <b><?=$candidate->exam_qualified?></b> Exam Qualified</span>
                                    <span class="course-level"><i class="icon-61"></i> <b><?=$candidate->year?></b> Year</span>
                                    <span class="course-level"><i class="icon-64"></i> <b><?=$candidate->rank?></b> Rank</span>
                                  
                           
                                    <!--<div class="read-more-btn">-->
                                    <!--    <a class="edu-btn btn-small btn-secondary" href="course-details.html">Learn More <i class="icon-4"></i></a>-->
                                    <!--</div>-->
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php } }else{ ?>
                      <h3 class="text-danger text-center">No Candidate Found</h3>
                    <?php } ?>
                    
                    
                    
                    
                    
                    <!-- End Single Course  -->
                    </div>
                </div>
            </div>
        </div>
        <?php } ?>