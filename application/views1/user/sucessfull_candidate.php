    <div class="edu-breadcrumb-area">
            <div class="container">
                <div class="breadcrumb-inner">
                    <div class="page-title">
                        <h1 class="title"><?=$page_title?></h1>
                    </div>
                    <ul class="edu-breadcrumb">
                        <li class="breadcrumb-item"><a href="<?=base_url('home')?>">Home</a></li>
                        <li class="separator"><i class="icon-angle-right"></i></li>
                        <li class="breadcrumb-item active" aria-current="page"><?=$page_title?></li>
                    </ul>
                <div>
                   <?php if($this->session->userdata('user_type')=='Institute'){?>
                    <a href="<?=base_url('create-successfull-candidate')?>" class="edu-btn btn-medium">Post New <?=$page_title?> +</a>
                  <?php } ?>
                </div>
                </div>
                </div>
          
            <ul class="shape-group">
                <li class="shape-1">
                    <span></span>
                </li>
                <li class="shape-2 scene"><img data-depth="2" src="<?=base_url('public/website/assets/images/about/shape-13.png')?>" alt="shape"></li>
                <li class="shape-3 scene"><img data-depth="-2" src="<?=base_url('public/website/assets/images/about/shape-15.png')?>" alt="shape"></li>
                <li class="shape-4">
                    <span></span>
                </li>
                <li class="shape-5 scene"><img data-depth="2" src="<?=base_url('public/website/assets/images/about/shape-07.png')?>" alt="shape"></li>
            </ul>
        </div>

        <!--=====================================-->
        <!--=        Courses Area Start         =-->
        <!--=====================================-->
        <div class="edu-course-area course-area-1 gap-tb-text">
            <div class="container">


                <div class="edu-sorting-area">
                    <div class="sorting-left">
                        <h6 class="showing-text">We found <span><?=count($users)?></span> <?=$page_title?> </h6>
                    </div>
                    <?php if($this->session->userdata('user_type')=='Institute'){?>
                  <div class="sorting-right">
                    <div class="layout-switcher">
                    
                      <label class="text-success"><a href="<?=base_url('create-successfull-candidate')?>" class="">Post New <?=$page_title?></a></label>
                      <ul class="switcher-btn">
                        <li><a href="<?=base_url('create-successfull-candidate')?>" class=""><i class="fi fi-minus-line"></i> +</a></li>
                        <!-- <li><a href="course-four.html" class="active"><i class="icon-54"></i></a></li> -->
                      </ul>
                      
                    </div>
                    <!-- <div class="edu-sorting">
                      <div class="icon"><i class="icon-55"></i></div>
                      <select class="edu-select">
                          <option>Filters</option>
                          <option>Low To High</option>
                          <option>High Low To</option>
                          <option>Last Viewed</option>
                      </select>
                      </div> -->
                  </div>
                  <?php } ?>
              
                </div>

                <div class="row g-5">
                    <!-- Start Single Course  -->
                    <?php 
                    if(count($users) > 0){
                    foreach($users as $user){
                    ?>
                    <div class="col-md-6 col-lg-3" data-sal-delay="100" data-sal="slide-up" data-sal-duration="800">
                        <div class="edu-course course-style-3 course-box-shadow">
                            <div class="inner">
                                <div class="thumbnail">
                                    <a href="javascript:void(0)">
                                        <img src="<?=base_url($user->picture)?>" alt="Course Meta" style="width:100%;height:270px;">
                                    </a>
                                    <div class="time-top">
                                        <span class="duration"><i class="icon-64"></i><?=$user->rank?> Rank</span>
                                        
                                    </div>
                                </div>
                                <div class="content">
                                    
                                     <h5 class="title">
                                        <a href="javascript:void(0)"><?=$user->name?></a>
                                    </h5>
                                    <span class="course-level"><i class="icon-34"></i> <b><?=$user->exam_qualified?></b> Exam Qualified</span>
                                    <span class="course-level"><i class="icon-61"></i> <b><?=$user->year?></b> Year</span>
                                    <span class="course-level"><i class="icon-64"></i> <b><?=$user->rank?></b> Rank</span>
                                 
                           
                                    <!--<div class="read-more-btn">-->
                                    <!--    <a class="edu-btn btn-small btn-secondary" href="course-details.html">Learn More <i class="icon-4"></i></a>-->
                                    <!--</div>-->
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php } }else{ ?>
                      <h3 class="text-danger text-center">No Candidate Found</h3>
                    <?php } ?>
                    <!-- End Single Course  -->
      
                </div>
                <!--<div class="load-more-btn" data-sal-delay="100" data-sal="slide-up" data-sal-duration="1200">-->
                <!--    <a href="course-one.html" class="edu-btn">Load More <i class="icon-56"></i></a>-->
                <!--</div>-->
            </div>
        </div>