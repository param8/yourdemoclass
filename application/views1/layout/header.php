<style>
  span.count {
  position: absolute;
  top: 26px;
  right: -7px;
  display: block;
  text-align: center;
 height: 18px;
    width: 18px;
    line-height: 18px;
  background-color: var(--color-primary);
  border-radius: 50%;
  color: var(--color-white);
  font-size: 11px;
  font-weight: 500;
  font-family: var(--font-secondary);
  }
  .inp_ut{
  position: relative;    width: 100%;
  }
  .s_data{
  display: block;
  position: absolute;
  background-color: #fff;
  padding: 12px 6px;
  border: 1px solid #d2d1d1;
  box-shadow: 0 0px 4px 0px #fff;
  max-height: 262px;
  overflow: auto;
  }
  .s_data p{
  margin: 0;
  }
  .s_data:before {
  border-right: 2px solid;
  content: '';
  display: block;
  height: 8px;
  margin-top: -6px;
  position: absolute;
  transform: rotate(142deg);
  left: 29%;
  top: -2px;
  width: 5px;
  }
  .s_data::after {
  border-right: 2px solid;
  content: '';
  display: block;
  height: 8px;
  margin-top: -6px;
  position: absolute;
  transform: rotate(36deg);
  right: 73%;
  top: -2px;
  width: 2px;
  }
</style>
<body class="sticky-header">
  <!--<div id="edublink-preloader">-->
  <!--  <div class="loading-spinner">-->
  <!--    <div class="preloader-spin-1"></div>-->
  <!--    <div class="preloader-spin-2"></div>-->
  <!--  </div>-->
  <!--  <div class="preloader-close-btn-wraper">-->
  <!--    <span class="btn btn-primary preloader-close-btn">-->
  <!--    Cancel Preloader</span>-->
  <!--  </div>-->
  <!--</div>-->
  <div id="main-wrapper" class="main-wrapper">
  <!--=====================================-->
  <!--=        Header Area Start       	=-->
  <!--=====================================-->
  <header class="edu-header header-style-11 header-fullwidth">
    <?php if(empty($this->session->userdata('email'))){?>
    <div class="header-top-bar">
      <div class="container-fluid">
        <div class="header-top">
          <div class="header-top-left">
            <div class="header-notify">
              First 20 students get 50% discount. <a href="#">Hurry up!</a>
            </div>
          </div>
          <div class="header-top-right">
            <ul class="header-info">
              <!-- onclick="loginModal()" -->
              <li><a href="<?=base_url('my-account')?>" >Login</a></li>
              <!-- <li><a href="#">Register</a></li> -->
              <li><a href="tel:+91 <?=$siteinfo->site_contact?>"><i class="icon-phone"></i>Call: +91 <?=$siteinfo->site_contact?></a></li>
              <li><a href="mailto:<?=$siteinfo->site_email?>" target="_blank"><i class="icon-envelope"></i>Email: <?=$siteinfo->site_email?></a></li>
              <li class="social-icon">
                <a href="<?=!empty($siteinfo->facebook_url) ? $siteinfo->facebook_url : '#';?>" target="_blank"><i class="icon-facebook"></i></a>
                <a href="<?=!empty($siteinfo->insta_url) ? $siteinfo->insta_url : '#';?>" target="_blank"><i class="icon-instagram"></i></a>
                <a href="<?=!empty($siteinfo->twitter_url) ? $siteinfo->twitter_url : '#';?>" target="_blank"><i class="icon-twitter"></i></a>
                <a href="<?=!empty($siteinfo->linkedin_url) ? $siteinfo->linkedin_url : '#';?>" target="_blank"><i class="icon-linkedin2"></i></a>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </div>
    <?php } ?>
    <div id="edu-sticky-placeholder"></div>
    <div class="header-mainmenu">
      <div class="container-fluid">
        <div class="header-navbar">
          <div class="header-brand">
            <div class="logo">
              <a href="<?=base_url('home')?>">
              <img class="logo-light" src="<?=base_url($siteinfo->site_logo)?>" style="width:100px;height:70px;"  alt="Corporate Logo">
              <img class="logo-dark" src="<?=base_url($siteinfo->site_logo)?>" style="width:100px;height:70px;"  alt="Corporate Logo">
              </a>
            </div>
            <div class="header-category">
              <nav class="mainmenu-nav">
                <ul class="mainmenu">
                  <li class="has-droupdown">
                    <a href="#"><i class="icon-1"></i>Category</a>
                    <ul class="submenu cat_gmenu">
                      <?php foreach($allCategories as $allCategory){?>
                      <li><a href="<?=base_url('courses/'.base64_encode($allCategory->id))?>"><?=$allCategory->category?></a></li>
                      <?php } ?>
                    </ul>
                  </li>
                </ul>
              </nav>
            </div>
          </div>
          <div class="header-mainnav">
            <nav class="mainmenu-nav">
              <ul class="mainmenu">
                <li class="">
                  <a href="<?=base_url('home')?>">Home</a>
                </li>
                <?php if($this->session->userdata('user_type')=='Institute'){?>
                <li class="">
                  <a href="<?=base_url('institute-home')?>">Institute Home</a>
                </li>
                <?php } ?>
                <li class="">
                  <a href="<?=base_url('courses')?>" class="">Courses<span class="count"><?=count($totalCourses)?></span></a>
                </li>
                <?php if($this->session->userdata('user_type')=='Institute' || $this->session->userdata('user_type')=='Student' ){ ?>
                <li class="">
                  <a href="<?=base_url('booked-courses')?>">Booked Course <span class="count"><?=count($totalBookedCourses)?></span></a>
                </li>
                <?php } ?>
                <li class="">
                  <a href="<?=base_url('average-courses')?>" class="">Price Alert<i class="fa-solid fa-coins"></i></a>
                </li>
                   <?php if($this->session->userdata('user_type')=='Institute'){?>
                <li class="">
                  <a href="<?=base_url('successfull-candidate')?>">Successfull Candidate</a>
                </li>
                <?php } ?>
         
                <li class="">
                  <a href="<?=base_url('contact-us')?>">Contact Us</a>
                </li>
              </ul>
            </nav>
          </div>
          <div class="header-right">
            <ul class="header-action">
              <li class="search-bar">
                <div class="input-group">
                  <input type="text" class="form-control" placeholder="Search">
                  <button class="search-btn" type="button"><i class="icon-2"></i></button>
                </div>
              </li>
              <li class="icon search-icon">
                <a href="javascript:void(0)" class="search-trigger">
                <i class="icon-2"></i>
                </a>
              </li>
                     <?php if(!empty($this->session->userdata('email'))){ ?>
                <li class="">
                  <a href="<?=base_url('Authantication/chat')?>" target="_blank"><i class="icon-28"></i></a>
                </li>
                <?php } ?>
              <?php if($this->session->userdata('user_type')=='Institute' OR $this->session->userdata('user_type')=='Student'){?>
              <li class="icon cart-icon">
                <a href="#" onclick="notification()" class="cart-icon">
                  <svg viewBox="0 0 1024 1024" style="margin-left: 0;height: 25px;">
                    <g>
                      <path d="M1025.5 800c0-288-256-224-256-448 0-18.56-1.788-34.42-5.048-47.928-16.83-113.018-92.156-203.72-189.772-231.36 0.866-3.948 1.32-8.032 1.32-12.21 0-33.278-28.8-60.502-64-60.502s-64 27.224-64 60.5c0 4.18 0.456 8.264 1.32 12.21-109.47 30.998-190.914 141.298-193.254 273.442-0.040 1.92-0.066 3.864-0.066 5.846 0 224.002-256 160.002-256 448.002 0 76.226 170.59 139.996 398.97 156.080 21.524 40.404 64.056 67.92 113.030 67.92s91.508-27.516 113.030-67.92c228.38-16.084 398.97-79.854 398.97-156.080 0-0.228-0.026-0.456-0.028-0.682l1.528 0.682zM826.246 854.096c-54.23 14.47-118.158 24.876-186.768 30.648-5.704-65.418-60.582-116.744-127.478-116.744s-121.774 51.326-127.478 116.744c-68.608-5.772-132.538-16.178-186.768-30.648-74.63-19.914-110.31-42.19-123.368-54.096 13.058-11.906 48.738-34.182 123.368-54.096 86.772-23.152 198.372-35.904 314.246-35.904s227.474 12.752 314.246 35.904c74.63 19.914 110.31 42.19 123.368 54.096-13.058 11.906-48.738 34.182-123.368 54.096z"></path>
                    </g>
                  </svg>
                  <span class="count"><?=count($notifications)?></span>
                </a>
              </li>
              <?php } ?>
              <li class="header-btn">
                <?php if(empty($this->session->userdata('email'))){?>
                <a href="<?=base_url('my-account')?>" class="edu-btn btn-medium">Post Courses <i class="icon-4"></i></a>
                <?php } else{?>
                <div class="header-category">
                  <nav class="mainmenu-nav">
                    <ul class="mainmenu">
                      <li class="has-droupdown">
                        <a href="#"><img src="<?=base_url($this->session->userdata('profile_pic'))?>" style="height:40px; width:40px;border-radius:50%;border: 2px solid #74bb1d;" alt=""></a>
                        <ul class="submenu">
                          <li><a href="#"><?=$this->session->userdata('name')?></a></li>
                          <hr>
                          <li><a href="<?=base_url('profile')?>">Profile</a></li>
                          <hr>
                          <li><a href="<?=base_url('authantication/logout')?>">Logout</a></li>
                        </ul>
                      </li>
                    </ul>
                  </nav>
                </div>
                <?php } ?>
              </li>
              <li class="mobile-menu-bar d-block d-xl-none">
                <button class="hamberger-button">
                <i class="icon-54"></i>
                </button>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </div>
    <!---mobile-menu---->
    <div class="popup-mobile-menu">
      <div class="inner">
        <div class="header-top">
          <div class="logo">
            <a href="index.html">
            <img class="logo-light" src="<?=base_url($siteinfo->site_logo)?>" alt="Corporate Logo">
            <img class="logo-dark" src="<?=base_url($siteinfo->site_logo)?>" alt="Corporate Logo">
            </a>
          </div>
          <div class="close-menu">
            <button class="close-button">
            <i class="icon-73"></i>
            </button>
          </div>
        </div>
        <ul class="mainmenu">
          <?php if(!empty($this->session->userdata('email'))){?>
          <li ><a href="javascript:void(0)" class="text-success"><b><img src="<?=base_url($this->session->userdata('profile_pic'))?>" style="width:30px;height:30px;border-radius:100%"> <?=$this->session->userdata('name')?></b></a></li>
          <?php } ?>
          <li class=""><a href="<?=base_url('home')?>"><i class="icon-16"></i> Home</a></li>
               <?php if($this->session->userdata('user_type')=='Institute'){?>
          <li class="">
            <a href="<?=base_url('institute-home')?>"><i class="icon-16"></i> Institute Home</a>
          </li>
          <?php } ?>
          <?php if(!empty($this->session->userdata('email'))){?>
          <li><a href="<?=base_url('profile')?>"><i class="icon-6"></i> Profile</a></li>
          <?php } ?>
     
          <li class="">
            <a href="<?=base_url('courses')?>" class=""><i class="icon-24"></i> Courses</a>
          </li>
          <?php if($this->session->userdata('user_type')=='Institute' || $this->session->userdata('user_type')=='Student' ){ ?>
          <li class="">
            <a href="<?=base_url('booked-courses')?>"><i class="icon-3"></i> Booked Course </a>
          </li>
          <?php } ?>
            <?php if($this->session->userdata('user_type')=='Institute'){?>
            <li class="">
              <a href="<?=base_url('successfull-candidate')?>"><i class="icon-48"> </i>Successfull Candidate</a>
            </li>
            <?php } ?>
          <?php if(!empty($this->session->userdata('email'))){ ?>
          <li class="">
            <a href="<?=base_url('Authantication/chat')?>" target="_blank"><i class="icon-28"> </i> Chat</a>
          </li>
          <?php } ?>
          <li class="">
            <a href="<?=base_url('contact-us')?>"><i class="icon-phone"> </i> Contact Us</a>
          </li>
          <li class="">
            <?php if(!empty($this->session->userdata('email'))){?>
            <a href="<?=base_url('authantication/logout')?>" class="edu-btn btn-medium">Logout</a>
            <?php }else{?>
            <a href="<?=base_url('my-account')?>" class="edu-btn btn-medium">Login / Register</a>
            <?php } ?>
          </li>
        </ul>
      </div>
    </div>
    <!---end-mob--->
    <!-- Start Search Popup  -->
    <div class="edu-search-popup">
      <div class="content-wrap">
        <div class="site-logo">
          <img class="logo-light" src="<?=base_url($siteinfo->site_logo)?>" style="width:100px;height:70px;" alt="Corporate Logo">
          <img class="logo-dark" src="<?=base_url($siteinfo->site_logo)?>" style="width:100px;height:70px;" alt="Corporate Logo">
        </div>
        <div class="close-button">
          <button class="close-trigger"><i class="icon-73"></i></button>
        </div>
        <div class="inner">
          <form class="search-form" action="#">
            <input type="text" class="edublink-search-popup-field" placeholder="Search Here...">
            <button class="submit-button"><i class="icon-2"></i></button>
          </form>
        </div>
      </div>
    </div>
    <!-- End Search Popup  -->
  </header>
  <?php if($this->session->userdata('email')){?>
  <div class="chaticon">
    <a href="<?=base_url('Authantication/chat')?>" target="_blank"><img src="https://yourdemoclass.com/public/chaticon.png"></a>
  </div>
  <?php } ?>