<footer class="edu-footer footer-lighten bg-image footer-style-1">
            <div class="footer-top">
                <div class="container">
                    <div class="row g-5">
                        <div class="col-lg-3   col-md-6">
                            <div class="edu-footer-widget">
                                <div class="logo">
                                <a href="<?=base_url('home')?>">
                                  <img class="logo-light" src="<?=base_url($siteinfo->site_logo)?>" style="width:100px;height:70px;" alt="Corporate Logo">
                                  <img class="logo-dark" src="<?=base_url($siteinfo->site_logo)?>" style="width:100px;height:70px;" alt="Corporate Logo">
                                 </a>
                                </div>
                                <p class="description"><?=$about->contant?></p>
                                <div class="widget-information">
                                    <ul class="information-list">
                                        <li><span>Add:</span><?=$siteinfo->site_address?></li>
                                        <li><span>Call:</span><a href="tel:+91<?=$siteinfo->site_contact?>">+91 <?=$siteinfo->site_contact?></a></li>
                                        <li><span>Email:</span><a href="mailto:<?=$siteinfo->site_email?>" target="_blank"><?=$siteinfo->site_email?></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-6 col-sm-6">
                            <div class="edu-footer-widget explore-widget">
                                <h4 class="widget-title">Online Platform</h4>
                                <div class="inner">
                                    <ul class="footer-link link-hover">
                                        <li><a href="#">About</a></li>
                                        <li><a href="<?=base_url('courses')?>">Courses</a></li>
                                        <li><a href="#">Instructor</a></li>
                                        <!-- <li><a href="event-grid.html">Events</a></li> -->
                                        <?php if($this->session->userdata('email')){?>
                                        <li><a href="<?=base_url('profile')?>">Profile</a></li>
                                        <?php } ?>
                                        <li><a href="#">Purchase Guide</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-2 col-6 col-sm-6">
                            <div class="edu-footer-widget quick-link-widget">
                                <h4 class="widget-title">Links</h4>
                                <div class="inner">
                                    <ul class="footer-link link-hover">
                                        <li><a href="<?=base_url('contact-us')?>">Contact Us</a></li>
                                        <!-- <li><a href="">Gallery</a></li>
                                        <li><a href="blog-standard.html">News & Articles</a></li> -->
                                        <li><a href="#">FAQ's</a></li>
                                        <?php if(empty($this->session->userdata('email'))){?>
                                        <li><a href="<?=base_url('my-account')?>">Sign In/Registration</a></li>
                                        <?php } ?>
                                        <li><a href="#">Coming Soon</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6 m_none1">
                            <div class="edu-footer-widget">
                                <h4 class="widget-title">Contacts</h4>
                                <div class="inner">
                                    <p class="description">Enter your email address to register to our newsletter subscription</p>
                                    <div class="input-group footer-subscription-form">
                                        <input type="email" class="form-control" placeholder="Your email">
                                        <button class="edu-btn btn-medium" type="button">Subscribe <i class="icon-4"></i></button>
                                    </div>
                                    <ul class="social-share icon-transparent">
                                        <li><a href="<?=!empty($siteinfo->facebook_url) ? $siteinfo->facebook_url : '#';?>" target="_blank" class="color-fb"><i class="icon-facebook"></i></a></li>
                                        <li><a href="<?=!empty($siteinfo->linkedin_url) ? $siteinfo->linkedin_url : '#';?>" target="_blank" class="color-linkd"><i class="icon-linkedin2"></i></a></li>
                                        <li><a href="<?=!empty($siteinfo->insta_url) ? $siteinfo->insta_url : '#';?>" target="_blank" class="color-ig"><i class="icon-instagram"></i></a></li>
                                        <li><a href="<?=!empty($siteinfo->twitter_url) ? $siteinfo->twitter_url : '#';?>" target="_blank" class="color-twitter"><i class="icon-twitter"></i></a></li>
                                        <li><a href="<?=!empty($siteinfo->youtube_url) ? $siteinfo->youtube_url : '#';?>" target="_blank" class="color-yt"><i class="icon-youtube"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="copyright-area">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="inner text-center">
                                <p><?=$siteinfo->footer_contant?></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
        <!-- End Footer Area  -->



    </div>

    <div class="rn-progress-parent">
        <svg class="rn-back-circle svg-inner" width="100%" height="100%" viewBox="-1 -1 102 102">
            <path d="M50,1 a49,49 0 0,1 0,98 a49,49 0 0,1 0,-98" />
        </svg>
    </div>

    

    <!-- JS
	============================================ -->
    <!-- Modernizer JS -->
    <script src="<?=base_url('public/website/assets/js/vendor/modernizr.min.js')?>"></script>
    <!-- Jquery Js -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.3/jquery.min.js"></script>
    <script src="<?=base_url('public/website/assets/js/vendor/bootstrap.min.js')?>"></script>
    <script src="<?=base_url('public/website/assets/js/vendor/sal.min.js')?>"></script>
    <script src="<?=base_url('public/website/assets/js/vendor/backtotop.min.js')?>"></script>
    <script src="<?=base_url('public/website/assets/js/vendor/magnifypopup.min.js')?>"></script>
    <script src="<?=base_url('public/website/assets/js/vendor/jquery.countdown.min.js')?>"></script>
    <script src="<?=base_url('public/website/assets/js/vendor/odometer.min.js')?>"></script>
    <script src="<?=base_url('public/website/assets/js/vendor/isotop.min.js')?>"></script>
    <script src="<?=base_url('public/website/assets/js/vendor/imageloaded.min.js')?>"></script>
    <script src="<?=base_url('public/website/assets/js/vendor/lightbox.min.js')?>"></script>
    <script src="<?=base_url('public/website/assets/js/vendor/paralax.min.js')?>"></script>
    <script src="<?=base_url('public/website/assets/js/vendor/paralax-scroll.min.js')?>"></script>
    <script src="<?=base_url('public/website/assets/js/vendor/jquery-ui.min.js')?>"></script>
    <script src="<?=base_url('public/website/assets/js/vendor/swiper-bundle.min.js')?>"></script>
    <script src="<?=base_url('public/website/assets/js/vendor/svg-inject.min.js')?>"></script>
    <script src="<?=base_url('public/website/assets/js/vendor/vivus.min.js')?>"></script>
    <script src="<?=base_url('public/website/assets/js/vendor/tipped.min.js')?>"></script>
    <script src="<?=base_url('public/website/assets/js/vendor/smooth-scroll.min.js')?>"></script>
    <script src="<?=base_url('public/website/assets/js/vendor/isInViewport.jquery.min.js')?>"></script>
    <script src="<?=base_url('public/website/assets/js/vendor/owl.carousel.js')?>"></script>
    <!-- Site Scripts -->
    <script src="<?=base_url('public/website/assets/js/app.js')?>"></script>

    <link href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/css/select2.min.css" rel="stylesheet" />
 <script src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/js/select2.min.js"></script>

<!---->



<!---->



   <script>
     $(document).ready(function() {
      $(".js-example-placeholder-multiple").select2({
        placeholder: "Select a batch"
    });
    
    });

		function closeModal(){
		  $('#modalLogin').modal('hide');
		  $('#modalRegister').modal('hide');
		  $('div').removeClass('modal-backdrop');
		}
		function loginModal(){
		  $('#modalLogin').modal('show');
		  $('#modalRegister').modal('hide');
		  $('div').removeClass('modal-backdrop');
		}
		
		function registerModal(){
		  $('#modalRegister').modal('show');
		  $('#modalLogin').modal('hide');
		  $('div').removeClass('modal-backdrop');
		}

    function getCity(stateID){
      //alert(stateID);
      $.ajax({
      url: '<?=base_url('Ajax_controller/get_city')?>',
      type: 'POST',
      data: {stateID},
      success: function (data) {
        $('#city').html(data);
              $('.city').html(data);
      }
      });
    }

    function getCity_institute_wise(userName){
      //alert(userID);
      $.ajax({
      url: '<?=base_url('Ajax_controller/getInstitute_city')?>',
      type: 'POST',
      data: {userName},
      success: function (data) {
        $('#institutecity').val(data);
      }
      });
    }


    $(document).ready(function() {
      getCity(<?=$this->session->userdata('location_state')?>)
    
    });

    function resetCourse(){
      $.ajax({
       url: '<?=base_url("setting/resetCourse")?>',
       type: 'POST',
       data: {ResetSesession:'ResetSesession'},  
       success: function (data) {
        location.reload();
       },
     });
    }

    function resetMode(){
      $.ajax({
       url: '<?=base_url("setting/resetMode")?>',
       type: 'POST',
       data: {ResetSesession:'ResetSesession'},  
       success: function (data) {
        location.reload();
       },
     });
    }

    function resetStartDate(){
      $.ajax({
       url: '<?=base_url("setting/resetStartDate")?>',
       type: 'POST',
       data: {ResetSesession:'ResetSesession'},  
       success: function (data) {
        location.reload();
       },
     });
    }

    function resetState(){
      $.ajax({
       url: '<?=base_url("setting/resetState")?>',
       type: 'POST',
       data: {ResetSesession:'ResetSesession'},  
       success: function (data) {
        location.reload();
       },
     });
    }

    function resetCity(){
      $.ajax({
       url: '<?=base_url("setting/resetCity")?>',
       type: 'POST',
       data: {ResetSesession:'ResetSesession'},  
       success: function (data) {
        location.reload();
       },
     });
    }

    function setCourseSession(page){
      var course= $('#course').val();
      var mode= $('#mode').val();
      var location_state= $('#state').val();
      var location_city= $('#city').val();
      var start_date= $('#start_date').val();
      $.ajax({
       url: '<?=base_url("setting/setSession")?>',
       type: 'POST',
       data: {course,mode,start_date,location_state,location_city},  
       success: function (data) {
        // if(page=='course_page'){
        //   //$( "#courseDataTable" ).load( "<?=base_url('courses')?> #courseDataTable" );
        //   $( "#filterRefresh" ).load( "<?=base_url('courses')?> #filterRefresh" );
        //   function() {
        //     courseDataTable.draw(true);
        //   };
        //   //$( "#courseAjaxRefresh" ).load( "<?=base_url('courses')?> #courseAjaxRefresh" );
        // }else{
         window.location="courses";
        //}
        
       },
     });
    }
	
    function notification(){
     $.ajax({
       url: '<?=base_url("notification/redirect_notification")?>',
       type: 'POST',
       data: {notification:'notification'},  
       success: function (data) {
        window.location="<?=base_url('notification')?>";
       },
     });
    }

    function getCoursesAjax(course){
      $.ajax({
       url: '<?=base_url("Ajax_controller/getCourses")?>',
       type: 'POST',
       data: {course},  
       success: function (data) {
        if(data!=""){
          $('#coursesData').show();
        $('#coursesData').html(data);
        }else{
          $('#coursesData').hide();
        }
       },
     });
    }

    function getAvgCoursesAjax(course){
      $.ajax({
       url: '<?=base_url("Ajax_controller/getavgCourses")?>',
       type: 'POST',
       data: {course},  
       success: function (data) {
        if(data!=""){
          $('#avgcoursesData').show();
        $('#avgcoursesData').html(data);
        }else{
          $('#avgcoursesData').hide();
        }
       },
     });
    }

    function fill(Value) {
   //Assigning value to "search" div in "search.php" file.
   $('#course').val(Value);
   //$('#coursesReset').show();
   //Hiding "display" div in "search.php" file.
   $('#coursesData').hide();
}

// function avgfill(course) {
//   //alert(course);
//   if(course=='other'){
//         $('#course_div').show();
//         //$('#avgcourse').hide();
//         $('#institute_div').show();
//         $('#institute_city').show();
//         $('#ins_city').hide();
        
//       }else{
//         $('#course_div').hide();
//         $('#institute_div').hide();
//         $('#avg_institute').show();
//       }
   //Assigning value to "search" div in "search.php" file.
   $('#avgcourse').val(course);
   //$('#coursesReset').show();
   //Hiding "display" div in "search.php" file.
   $('#avgcoursesData').hide();
   $.ajax({
       url: '<?=base_url("Ajax_controller/getInstitutes")?>',
       type: 'POST',
       data: {course},  
       success: function (data) {
        if(data!=""){
        $('#avginstitute').html(data);
        }else{
          $('#avginstitute').hide();
        }
       },
     });

    
	</script>
  
  <!------------------->
  <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.3/jquery.min.js"></script> -->
  <scrit src="https://cdn.datatables.net/1.13.4/js/dataTables.bootstrap4.min.js"></scrit>
  <script src="https://cdn.datatables.net/1.13.4/js/jquery.dataTables.min.js"></script>

  <script>
    $(document).ready(function () {
     // $('#notificationDataTable').DataTable();
      var notificationDataTable = $('#notificationDataTable').DataTable({
      "processing":true,
      "serverSide":true,
      buttons: [
      { extend: 'excelHtml5', text: 'Download Excel' }
    ],
      "order":[],
      "ajax":{
          url:"<?=base_url('Ajax_controller/ajax_notification_datatable')?>",
          type:"POST"
      },
      "columnDefs":[
       {
           "targets":[0],
           "orderable":false,
       },
     ],
  
 });
  });


  $(document).ready(function () {
    
    var courseDataTable = $('#courseDataTable').DataTable({
      "processing":true,
      "serverSide":true,
      buttons: [
      { extend: 'excelHtml5', text: 'Download Excel' }
    ],
      "order":[],
      "ajax":{
          url:"<?=base_url('Ajax_controller/ajax_course_datatable')?>",
          type:"POST"
      },
      "columnDefs":[
       {
           "targets":[0],
           "orderable":false,
       },
     ],
  
 });
});

$(document).ready(function () {
    
    var datatable = $('#bookedCourseDataTable').DataTable({
      "processing":true,
      "serverSide":true,
      buttons: [
      { extend: 'excelHtml5', text: 'Download Excel' }
    ],
      "order":[],
      "ajax":{
          url:"<?=base_url('Ajax_controller/ajax_bookedCourses_datatable')?>",
          type:"POST"
      },
      "columnDefs":[
       {
           "targets":[0],
           "orderable":false,
       },
     ],
  
 });
});
  
$("form#courseForm").submit(function(e) {
  //alert("hello");
  $(':input[type="submit"]').prop('disabled', true);
  e.preventDefault();    
  var formData = new FormData(this);
  $.ajax({
  url: $(this).attr('action'),
  type: 'POST',
  data: formData,
  cache: false,
  contentType: false,
  processData: false,
  dataType: 'json',
  success: function (data) {
  if(data.status==200) {
  //$('.modal').modal('hide');
  //toastr.success(data.message);
    Swal.fire({
    icon: 'success',
    title: 'Success',
    text: data.message,
    //footer: '<a href="">Why do I have this issue?</a>'
    })
  $(':input[type="submit"]').prop('disabled', false);
  setTimeout(function(){
          if(data.user_type=='Institute'){
            location.href="<?=base_url('institute-home')?>";
          }else{
            location.href="<?=base_url('home')?>";
          }
  	
  }, 1000) 
  
  }else if(data.status==403) {
  //toastr.error(data.message);
     Swal.fire({
      icon: 'error',
      title: 'Oops...',
      text: data.message,
      //footer: '<a href="">Why do I have this issue?</a>'
    })
  $(':input[type="submit"]').prop('disabled', false);
  }else{
      Swal.fire({
      icon: 'error',
      title: 'Oops...',
      text: 'Something went wrong',
      //footer: '<a href="">Why do I have this issue?</a>'
    })
  $(':input[type="submit"]').prop('disabled', false);
  }
  },
  error: function(){} 
  });
  });
  
  </script>
  
</body>

</html>