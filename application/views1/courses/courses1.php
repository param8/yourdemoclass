<!--=====================================-->
<!--=       Breadcrumb Area Start      =-->
<!--=====================================-->
<div class="edu-breadcrumb-area">
<div class="container">
  <div class="breadcrumb-inner">
    <div class="page-title">
      <h1 class="title"><?=$category?></h1>
    </div>
    <ul class="edu-breadcrumb">
      <li class="breadcrumb-item"><a href="<?=base_url('home')?>">Home</a></li>
      <li class="separator"><i class="icon-angle-right"></i></li>
      <li class="breadcrumb-item"><a href="#"><?=$page_title?></a></li>
    </ul>
  </div>
</div>
<ul class="shape-group">
  <li class="shape-1">
    <span></span>
  </li>
  <li class="shape-2 scene"><img data-depth="2" src="<?=base_url('public/website/assets/images/about/shape-13.png')?>" alt="shape"></li>
  <li class="shape-3 scene"><img data-depth="-2" src="<?=base_url('public/website/assets/images/about/shape-15.png')?>" alt="shape"></li>
  <li class="shape-4">
    <span></span>
  </li>
  <li class="shape-5 scene"><img data-depth="2" src="<?=base_url('public/website/assets/images/about/shape-07.png')?>" alt="shape"></li>
</ul>
<!--=====================================-->
<!--=        Courses Area Start         =-->
<!--=====================================-->
<div class="edu-course-area course-area-1 gap-tb-text">
  <div class="container">
  <div class="row ">
    <div class="col-lg-6">
      <input type="text" class="" name="course" id="course" placeholder="Search Course" value="<?=$this->session->userdata('course')?>">
      <p class="text-danger" onclick="resetCourse()" style="cursor:pointer"> Reset Course</p>
    </div>
    <div class="col-lg-3">
    <select class="" name="mode"  id="mode">
    <option value="" >Select Mode</option>
      <?php 
        $modes = array('Offline','Online','Both');
        foreach($modes as $mode){
      ?>
        <option value="<?=$mode?>" <?=$this->session->userdata('mode')==$mode ? 'selected' : '';?>><?=$mode?></option>
        <?php } ?>
    </select>
    <p class="text-danger" onclick="resetMode()" style="cursor:pointer"> Reset Mode</p>
   </div>
   <div class="col-lg-3">
      <button type="button" onclick="setCourseSession()" class="edu-btn" >Search Course <i class="icon-2"></i></button>
    </div>
   
  </div>

    <div class="edu-sorting-area">
      <div class="sorting-left">
        <h6 class="showing-text">We found <span><?=count($courses)?></span> <?=strtolower($page_title)?> available </h6>
      </div>
      <?php if($this->session->userdata('user_type')=='Institute'){?>
      <div class="sorting-right">
        <div class="layout-switcher">
          <label>Add <?=$page_title?></label>
          <ul class="switcher-btn">
            <li><a href="<?=base_url('create-course')?>" class=""><i class="fi fi-minus-line"></i> +</a></li>
            <!-- <li><a href="course-four.html" class="active"><i class="icon-54"></i></a></li> -->
          </ul>
        </div>
        <!-- <div class="edu-sorting">
          <div class="icon"><i class="icon-55"></i></div>
          <select class="edu-select">
              <option>Filters</option>
              <option>Low To High</option>
              <option>High Low To</option>
              <option>Last Viewed</option>
          </select>
          </div> -->
      </div>
      <?php } ?>
    </div>
    <div class="row g-5" id="appendClass">
      <!-- Start Single Course  -->
      <?php 
        $id = "";
          foreach($courses as $course){
            $id = $course->id;
            $earlier = new DateTime($course->start_date);
            $later = new DateTime($course->end_date);
            $totalDays =  $later->diff($earlier)->format("%a");
            if($course->fees > 0 && $course->discount > 0){
                $price =$course->fees - ($course->discount*$course->fees)/100;
            }else{
                $price =$course->fees;
            }
            $this->db->where(array('course_rating.courseID' => $course->id));
            $allOverRating =  $this->db->get('course_rating')->result();
        
            $this->db->where(array('course_rating.courseID' => $course->id,'course_rating.rating'=>5));
            $five =  $this->db->get('course_rating')->result();
        
            $this->db->where(array('course_rating.courseID' => $course->id,'course_rating.rating'=>4));
            $four =  $this->db->get('course_rating')->result();
        
            $this->db->where(array('course_rating.courseID' => $course->id,'course_rating.rating'=>3));
            $three =  $this->db->get('course_rating')->result();
        
            $this->db->where(array('course_rating.courseID' => $course->id,'course_rating.rating'=>2));
            $two =  $this->db->get('course_rating')->result();
        
            
            $this->db->where(array('course_rating.courseID' => $course->id,'course_rating.rating'=>1));
            $one =  $this->db->get('course_rating')->result();
            $count_Five = 0;
            $count_Foure = 0;
            $count_Three = 0;
            $count_Two = 0;
            $count_One = 0;
            $total_CountFive = 0;
            $total_CountFoure = 0;
            $total_CountThree = 0;
            $total_CountTwo = 0;
            $total_CountOne = 0;
            if(count($five) > 0){
             $count_Five = count($five);
             $total_CountFive = $count_Five*5;
            }
            if(count($four) > 0){
             $count_Foure = count($four);
             $total_CountFoure = $count_Foure*4;
            }
            if(count($three) > 0){
             $count_Three = count($three);
             $total_CountThree = $count_Three*3;
            }
            if(count($two) > 0){
             $count_Two = count($two);
             $total_CountTwo = $count_Two*2;
            }
            if(count($one) > 0){
             $count_One = count($one);
             $total_CountOne = $count_One*1;
            }
            $sumTotalCountRating = $total_CountFive + $total_CountFoure + $total_CountThree + $total_CountTwo + $total_CountOne;
            $sumCountRating = $count_Five + $count_Foure + $count_Three + $count_Two + $count_One;
            if($sumTotalCountRating > 0 && $sumCountRating > 0){
            $TotalratingAvg = $sumTotalCountRating/$sumCountRating;
            }else{
              $TotalratingAvg = 0;
            }
              
              $stars_rating="";
              $newwholeRating = floor($TotalratingAvg);
              $fractionRating = $TotalratingAvg - $newwholeRating;
              if($newwholeRating > 0){
              for($s=1;$s<=$newwholeRating;$s++){
                $stars_rating .= '<i class="icon-23"></i>';	
              }
              if($fractionRating >= 0.25){
                $stars_rating .= '<i class="icon-23 n50"></i>';	
              }
            }
              else{
                for($s=1;$s<=5;$s++){
                 $stars_rating .= '<i class="icon-23 text-secondary"></i>';
                }
              }  
        ?>
      <div class="col-md-6 col-lg-4 col-xl-3" data-sal-delay="100" data-sal="slide-up" data-sal-duration="800">
        <div class="edu-course course-style-1 course-box-shadow hover-button-bg-white">
          <div class="inner">
            <div class="thumbnail">
              <a href="<?=base_url('course-details/'.base64_encode($course->id))?>">
              <img src="<?=base_url($course->image)?>" alt="Course Meta" style="height:269px">
              </a>
              <div class="time-top">
                <span class="duration"><i class="icon-61"></i><?=$totalDays?> Days</span>
              </div>
            </div>
            <div class="content">
              <!-- <span class="course-level">Beginner</span> -->
              <h6 class="title">
                <a href="#"><?=$course->course?></a>
              </h6>
              <div class="course-rating">
                <div class="rating">
                  <!-- <i class="icon-23"></i>
                    <i class="icon-23"></i>
                    <i class="icon-23"></i>
                    <i class="icon-23"></i>
                    <i class="icon-23"></i> -->
                  <?= $stars_rating?>
                </div>
                <span class="rating-count">(<?=$TotalratingAvg .'/'.count($allOverRating)?> Rating)</span>
              </div>
              <div class="course-price">₹ <?=$price?> / <del>₹ <?=$course->fees?></del></div>
              <div class="title">Mode <span class="text-success font-weight-bold"><?=$course->mode?></span> </div>
              <ul class="course-meta">
                <li><i class="icon-24"></i><?=$course->seats?> Total Seats</li>
                <li class="text-danger"><i class="icon-24 text-danger"></i><?=$course->remaning_seats?> Remaning Seats</li>
              </ul>
            </div>
          </div>
          <div class="course-hover-content-wrapper">
            <button class="wishlist-btn"><i class="icon-22"></i></button>
          </div>
          <div class="course-hover-content-wrapper">
            <button class="wishlist-btn"><i class="icon-22"></i></button>
          </div>
          <div class="course-hover-content">
            <div class="content">
              <!-- <span class="course-level">Advanced</span> -->
              <h6 class="title">
                <a href="<?=base_url('course-details/'.base64_encode($course->id))?>"><?=$course->course?></a>
              </h6>
              <div class="course-rating">
                <div class="rating">
                  <!-- <i class="icon-23"></i>
                    <i class="icon-23"></i>
                    <i class="icon-23"></i>
                    <i class="icon-23"></i>
                    <i class="icon-23"></i> -->
                  <?= $stars_rating?>
                </div>
                <span class="rating-count">(<?=$TotalratingAvg .'/'.count($allOverRating)?> Rating)</span>
              </div>
              <div class="course-price">₹ <?=$price?> / <del>₹ <?=$course->fees?></del></div>
              <p><?=$course->description?></p>
              <ul class="course-meta">
                <li><i class="icon-24"></i><?=$course->seats?> Total Seats</li>
                <li class="text-danger"><i class="icon-24 text-danger"></i><?=$course->remaning_seats?> Remaning Seats</li>
              </ul>
              <a href="<?=base_url('course-details/'.base64_encode($course->id))?>" class="edu-btn btn-secondary btn-small">Enrolled <i class="icon-4"></i></a>
            </div>
          </div>
        </div>
      </div>
      <?php } ?>
      <!-- End Single Course  -->    
    </div>

    <div class="load-more-btn" id="remove_row" data-sal-delay="100" data-sal="slide-up" data-sal-duration="1200">
      <a href="#" data-id ="<?=$id?>" id="load_more" class="edu-btn loadButton">Load More <i class="icon-56"></i></a>
    </div>
  </div>
</div>
<!-- End Course Area -->
<!--=====================================-->
<!--=        Footer Area Start          =-->
<!--=====================================-->
<!-- Start Footer Area  -->
<script>
  $( document ).ready(function() {
    $(document).on('click','#load_more',function() {
      event.preventDefault();
  //function load_more(id,categoryID){
    var id = $('#load_more').data('id');
    var categoryID = <?=$category_id?>;
    $.ajax({
    url: '<?=base_url('Ajax_controller/load_more')?>',
    type: 'POST',
    data: {id,categoryID},
    dataType:'text',
    success: function (responce) {
      //alert(responce)
      console.log(responce);
     if(responce!=""){
        $('#remove_row').remove();
        $('#appendClass').append(responce);
     }else{
      $('#loadButton').prop('disabled', true);
     }
    }
    });
  //}
});
});




 
</script>