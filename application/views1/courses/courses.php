<style>
  .s_date {
  border: 1px solid #bab1b1!important;
  height: 42px!important;
  padding: 0 8px!important;
  border-radius: 0px!important;
  line-height: normal!important;
  }
</style>
<div class="edu-breadcrumb-area" >
  <div class="container">
    <div class="breadcrumb-inner">
      <div class="page-title">
        <h1 class="title "><?=$category?></h1>
      </div>
      <ul class="edu-breadcrumb">
        <li class="breadcrumb-item"><a href="<?=base_url('home')?>">Home</a></li>
        <li class="separator"><i class="icon-angle-right"></i></li>
        <li class="breadcrumb-item"><a href="javascript:void(0)"><?=$page_title?></a></li>
      </ul>
      <div>
        <?php if($this->session->userdata('user_type')=='Institute'){?>
        <a href="<?=base_url('create-course')?>" class="edu-btn btn-medium">Post New <?=$page_title?> +</a>
        <?php } ?>
      </div>
    </div>
  </div>
  <ul class="shape-group">
    <li class="shape-1">
      <span></span>
    </li>
    <li class="shape-2 scene"><img data-depth="2" src="<?=base_url('public/website/assets/images/about/shape-13.png')?>" alt="shape"></li>
    <li class="shape-3 scene"><img data-depth="-2" src="<?=base_url('public/website/assets/images/about/shape-15.png')?>" alt="shape"></li>
    <li class="shape-4">
      <span></span>
    </li>
    <li class="shape-5 scene"><img data-depth="2" src="<?=base_url('public/website/assets/images/about/shape-07.png')?>" alt="shape"></li>
  </ul>
</div>
<!--=====================================-->
<!--=        Courses Area Start         =-->
<!--=====================================-->
<div class="edu-course-area course-bg  " id="filterRefresh">
  <div class="container">
    <?php if($this->session->userdata('user_type')!='Institute'){?>
    <div class="card en_query_c" >
      <div class="row  ">
        <div class="col-md-12 ">
          <div class="en_form ">
            <div class="c2 c_f">
              <input type="text" class="s_date" name="course" id="course" placeholder="Search Course"  autocomplete="off" onkeyup="getCoursesAjax(this.value)" value="<?=$this->session->userdata('course')?>">
              <span class="re_s" onclick="resetCourse()" id="coursesReset" <?=empty($this->session->userdata('course')) ? 'style="display:none;"' : 'style="display:block;"'?>>&#10539;</span>
              <span class="s_data" id="coursesData" style="display:none"></span>
              <!--<p class="text-danger" onclick="resetCourse()" style="cursor:pointer"> Reset Course</p>-->
            </div>
            <div class="c2 c_f">
              <input type="text"  onfocus="(this.type='date')" min="<?= date('Y-m-d'); ?>"  class="s_date" name="start_date" id="start_date" placeholder="Select Start Date" value="<?=$this->session->userdata('start_date')?>">
              <span class="re_s" onclick="resetStartDate()"  <?=empty($this->session->userdata('start_date')) ? 'style="display:none;"' : 'style="display:block;"'?>>&#10539;</span>
              <!--<p class="text-danger" onclick="resetStartDate()" style="cursor:pointer"> Reset Start Date</p>-->
            </div>
            <div class="c3 c_f">
              <select class="s_date" name="mode"  id="mode">
                <option value="" >Select Mode</option>
                <?php 
                  $modes = array('Offline','Online','Both');
                  foreach($modes as $mode){
                  ?>
                <option value="<?=$mode?>" <?=$this->session->userdata('mode')==$mode ? 'selected' : '';?>><?=$mode?></option>
                <?php } ?>
              </select>
              <span class="re_s" onclick="resetMode()" <?=empty($this->session->userdata('mode')) ? 'style="display:none;"' : 'style="display:block;"'?>>&#10539;</span>
              <!--<p class="text-danger" onclick="resetMode()" style="cursor:pointer"> Reset Mode</p>-->
            </div>
            <div class="c4 c_f">
              <select class="s_date" name="state"  id="state" onchange="getCity(this.value)">
                <option value="" >Select Sate</option>
                <?php 
                  foreach($states as $state){
                  ?>
                <option value="<?=$state->id?>" <?=$this->session->userdata('location_state')==$state->id ? 'selected' : '';?>><?=$state->name?></option>
                <?php } ?>
              </select>
              <span class="re_s" onclick="resetState()" <?=empty($this->session->userdata('location_state')) ? 'style="display:none;"' : 'style="display:block;"'?>>&#10539;</span>
              <!--<p class="text-danger" onclick="resetState()" style="cursor:pointer"> Reset State</p>-->
            </div>
            <div class="c5 c_f">
              <select class="s_date" name="city"  id="city">
                <option value="" >Select City</option>
              </select>
              <!--<p class="text-danger" onclick="resetCity()" style="cursor:pointer"> Reset City</p>-->
              <span class="re_s"  onclick="resetCity()" <?=empty($this->session->userdata('location_city')) ? 'style="display:none;"' : 'style="display:block;"'?>>&#10539;</span>
            </div>
            <div class="c6 c_f">
              <button type="button" onclick="setCourseSession('course_page')" class="edu-btn s_date" >Search Course <i class="icon-2"></i></button>
            </div>
          </div>
        </div>
        <?php } ?>
      </div>
    </div>
  </div>
</div>
<div class="edu-course-area course-area-1 pt-0 section-gap-equal">
  <div class="container">
    <div class="row g-5">

      <div class="col-lg-12 col-pl--35">
        <div class="edu-sorting-area">
          <!-- <div class="sorting-left">
            <h6 class="showing-text">We found <span><?//=count($courses)?></span> <?//=strtolower($page_title)?> available </h6>
          </div> -->
          <?php if($this->session->userdata('user_type')=='Institute'){?>
          <div class="sorting-right">
            <div class="layout-switcher">
              <label class="text-success"><a href="<?=base_url('create-course')?>" class="">Post New <?=$page_title?></a></label>
              <ul class="switcher-btn">
                <li><a href="<?=base_url('create-course')?>" class=""><i class="fi fi-minus-line"></i> +</a></li>
                <!-- <li><a href="course-four.html" class="active"><i class="icon-54"></i></a></li> -->
              </ul>
            </div>
     
          </div>
          <?php } ?>
        </div>
        <table id="courseDataTable" class="table table-responsive  table-borderless" style="width:100%">
        <thead>
        <tr>
          <td></td>
        </tr>
          </thead>
        <tbody>

        </tbody>
        </table>
      </div>
    </div>
  </div>
  <p class="edu-pagination active"><?//= $links; ?></p>
  <!-- <ul class="edu-pagination ">
    <li><a href="#" aria-label="Previous"><i class="icon-west"></i></a></li>
    <li class="active"><a href="#"></a></li>
    <li><a href="#">2</a></li>
    <li><a href="#">3</a></li>
    <li class="more-next"><a href="#"></a></li>
    <li><a href="#">8</a></li>
    <li><a href="#" aria-label="Next"><i class="icon-east"></i></a></li> 
    </ul> -->
</div>
</div>
<script>

  
</script>
<script>
  // 		$(document).ready(function(){
  //       var session_user_type = '<?//=$this->session->userdata('user_type')?>';
  //       if(session_user_type == 'Institute'){
  //         $(window).scrollTop(400);
  //       }else{
  //         $(window).scrollTop(600);
  //       }
  		   
  // 	   });
  // 	  
</script>          
<script>
  function getComment(courseID){
    
    $.ajax({
      url: '<?=base_url('Ajax_controller/getcomment')?>',
      type: 'POST',
      data: {courseID},
      success: function (data) {
        $('#show_comment'+courseID).html(data);
      }
    });
  }
</script>