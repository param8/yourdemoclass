  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
	  <div class="container-full">
		<!-- Content Header (Page header) -->
		<div class="content-header">
			<div class="d-flex align-items-center">
				<div class="mr-auto">
					<h3 class="page-title"><?=$page_title?></h3>
					<div class="d-inline-block align-items-center">
						<nav>
							<ol class="breadcrumb">
								<li class="breadcrumb-item"><a href="#"><i class="mdi mdi-home-outline"></i></a></li>
								<li class="breadcrumb-item active" aria-current="page"><?=$page_title?></li>
							</ol>
						</nav>
					</div>
				</div>
				
			</div>
		</div>

		<!-- Main content -->
		<section class="content pb-5">
		  <div class="row">
			<div class="col-12">          
              
			  <div class="box">
				<div class="box-header">
				  <h4 class="box-title">About Us Contents</h4>
				</div>
				<!-- /.box-header -->
        <form action="<?=base_url('admin/setting/store_about_us')?>" id="addAboutUs" method="POST" enctype="multipart/form-data">
            <div class="box-body">
                <div class="form-group">
                    <label for="title" class="col-form-label">Title:</label>
                    <input type="text" class="form-control" name="title" id="title" value="<?=!empty($about_us->title)?$about_us->title:'';?>">
                </div>
                <div class="form-group">
                    <label for="title" class="col-form-label">Description:</label>
                    <textarea id="editor1" name="description" rows="10" cols="80"><?=!empty($about_us->description)?$about_us->description:'';?></textarea>
                </div>
                <div class="form-group">
                    <label for="title" class="col-form-label">Short Description:</label>
                    <input type="text" class="form-control" name="short_description" id="short_description" value="<?=!empty($about_us->short_description)?$about_us->short_description:'';?>">
                    <p class="text-danger">Only 200 characters allow...</p>
                  </div>
              
                <div class="form-group">
                    <label for="image" class="col-form-label">About Us Image:</label>
                    <input type="file" class="form-control" name="image" id="image">
                </div>
                <div class="form-group">
                    <img src="<?=$about_us->image != "" ? base_url($about_us->image) : '' ;?>" width="100" height="100">
                </div>
                <input type="hidden" name="aboutus_image" id="aboutus_image" value="<?=!empty($about_us->image)?$about_us->image:'';?>">
            </div>
            <div class="box-footer pb-40 text-center">
                <button type="submit" class="btn btn-rounded btn-success">Update</button>
            </div>
        </form>
			  </div>
			  <!-- /.box -->

			</div>
			<!-- /.col-->
		  </div>
		  <!-- ./row -->
		</section>
		<!-- /.content -->
	  </div>
  </div>
  <!-- /.content-wrapper -->

  <script src="<?=base_url('public/admin/vendor_components/ckeditor/ckeditor.js')?>"></script>
  <script src="<?=base_url('public/admin/js/pages/editor.js')?>"></script>
  <script type="text/javascript">
  $("form#addAboutUs").submit(function(e) {
     $(':input[type="submit"]').prop('disabled', true);
     e.preventDefault();    
     var formData = new FormData(this);
     const textareaValue = CKEDITOR.instances.editor1.getData();
     formData.append('desc', textareaValue);
     $.ajax({
       url: $(this).attr('action'),
       type: 'POST',
       data: formData,
       cache: false,
       contentType: false,
       processData: false,
       dataType: 'json',
       success: function (data) {
         if(data.status==200) {
           toastr.success(data.message);
  				setTimeout(function(){
                      location.reload();
           }, 1000) 
  
         }else if(data.status==403) {
           toastr.error(data.message);
           $(':input[type="submit"]').prop('disabled', false);
         }else{
           toastr.error('Unable to add slider');
           $(':input[type="submit"]').prop('disabled', false);
         }
       },
       error: function(){} 
     });
   });
   </script>