
  
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
	  <div class="container-full">
		<!-- Content Header (Page header) -->
		<div class="content-header">
			<div class="d-flex align-items-center">
				<div class="mr-auto">
					<h3 class="page-title"><i class="fa fa-university"> <?=$page_title?></i></h3>
					<div class="d-inline-block align-items-center">
						<nav>
							<ol class="breadcrumb">
								<li class="breadcrumb-item"><a href="#"><i class="mdi mdi-home-outline"></i></a></li>
								<li class="breadcrumb-item" aria-current="page"><?=$page_title?></li>
								<li class="breadcrumb-item active" aria-current="page"><?=$page_title?></li>
							</ol>
						</nav>
					</div>
				</div>
				
			</div>
		</div>

		<!-- Main content -->
		<section class="content">
		  <div class="row">
       <div class="col-md-6 col-lg-6">
			   <div class="box"> 
            <div class="box-header with-border">
                <h3 class="box-title">All <?=$page_title?></h3>
            </div>
          </div>
        </div>
        <div class="col-md-6 col-lg-6 ">
          <div class="box "> 
            <div class="box-header with-border">
              <a href="#" class="btn btn-primary btn-sm float-right" data-toggle="modal" data-target="#exampleModal" data-whatever="@mdo">Add Language <i class="fa fa-plus"></i></a>
            </div>
          </div>
				  <!-- <h6 class="box-subtitle">Export data to Copy, CSV, Excel, PDF & Print</h6> -->
				</div>
				<!-- /.box-header -->
				<div class="box-body">
					<div class="table-responsive">
					  <table id="example" class="table table-bordered table-hover display nowrap margin-top-10 w-p100">
						<thead>
							<tr>
                                <th>SNO</th>
                                <th>Language Name</th>
                                <th>Created Date</th>
                                <th>Modified Date</th>
                                <th>Action</th>
							</tr>
						</thead>
						<tbody>
                <?php foreach($languages as $key=>$language){?>
							<tr>
								<td><?=$key+1;?></td>
								<td><?= $language->language_name?></td>
                                <td><?= date('d-m-Y',strtotime($language->create_date));?></td>
                                <td><?=!empty($language->modify_date)?date('d-m-Y',strtotime($language->modify_date)):'';?></td>
                                <td>
                                <a href="javascript:void(0);" class="btn btn-warning btn-sm btn-edit" data-uid="<?= $language->lang_uid;?>" data-name="<?= $language->language_name;?>" data-toggle="tooltip" title="Edit Language"><i class="fa fa-edit"></i></a>
                                <a href="javascript:void(0);" class="btn btn-danger btn-sm" data-toggle="tooltip" title="Delete Language" onclick="return deleteCustomer('<?=$language->lang_uid;?>');"><i class="fa fa-trash"></i></a>
                                </td>
							</tr>
                <?php } ?>
				
						</tbody>				  
					
					</table>
					</div>              
				</div>
				<!-- /.box-body -->
			  </div>
			  <!-- /.box -->          
			</div>
			<!-- /.col -->
		  </div>
		  <!-- /.row -->
		</section>
		<!-- /.content -->
	  
	  </div>
  </div>
  <!-- /.content-wrapper -->

  <!-- Add Schoo Modal Start -->
  <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Add Language</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="<?=base_url('admin/language/store')?>" id="addLanguage" method="POST" enctype="multipart/form-data">
      <div class="modal-body">
          <div class="form-group">
            <label for="language_name" class="col-form-label">Language name:</label>
            <input type="text" class="form-control" name="language_name" id="language_name">
          </div>
          
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" name="submit" class="btn btn-primary">Submit</button>
      </div>
      </form>
    </div>
  </div>
</div>
  <!-- Add School Modal End -->

  <!-- edit form modal -->
  <div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Edit Language</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <form action="<?=base_url('admin/language/edit')?>" id="editLanguage" method="POST">
        <div class="modal-body">
            <input type="hidden" name="lang_uid" class="lang_uid" value="">
            <div class="form-group">
                <label for="language_name" class="col-form-label">Language name:</label>
                <input type="text" class="form-control language_name" name="language_name" id="language_name" value="">
            </div>
            
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="submit" name="submit" class="btn btn-primary">Update</button>
        </div>
        </form>
        </div>
    </div>
</div>

  <script type="text/javascript">
  $("form#addLanguage").submit(function(e) {
     $(':input[type="submit"]').prop('disabled', true);
     e.preventDefault();    
     var formData = new FormData(this);
     $.ajax({
       url: $(this).attr('action'),
       type: 'POST',
       data: formData,
       cache: false,
       contentType: false,
       processData: false,
       dataType: 'json',
       success: function (data) {
         if(data.status==200) {
           toastr.success(data.message);
  				setTimeout(function(){
                      location.reload();
           }, 1000) 
  
         }else if(data.status==403) {
           toastr.error(data.message);
           $(':input[type="submit"]').prop('disabled', false);
         }else{
           toastr.error('Unable to add language');
           $(':input[type="submit"]').prop('disabled', false);
         }
       },
       error: function(){} 
     });
   });
  
   $('.btn-edit').on('click',function(){
        // get data from button edit
        const uid = $(this).data('uid');
        const name = $(this).data('name');
        
        // Set data to Form Edit
        $('.lang_uid').val(uid);
        $('.language_name').val(name);
        // Call Modal Edit
        $('#editModal').modal('show');
    });

    $("form#editLanguage").submit(function(e) {
     $(':input[type="submit"]').prop('disabled', true);
     e.preventDefault();    
     var formData = new FormData(this);
     $.ajax({
       url: $(this).attr('action'),
       type: 'POST',
       data: formData,
       cache: false,
       contentType: false,
       processData: false,
       dataType: 'json',
       success: function (data) {
         if(data.status==200) {
           toastr.success(data.message);
  				setTimeout(function(){
                      location.reload();
           }, 1000) 
  
         }else if(data.status==403) {
           toastr.error(data.message);
           $(':input[type="submit"]').prop('disabled', false);
         }else{
           toastr.error('Unable to edit language');
           $(':input[type="submit"]').prop('disabled', false);
         }
       },
       error: function(){} 
     });
   });

   function deleteLanguage(u_id){
      var action = "Delete";
      Swal.fire({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!'
        }).then((result) => {
        if (result.isConfirmed) {
            $.ajax({
                url: "<?=base_url('admin/language/delete/')?>", 
                method: 'POST',
                data: {u_id,action},
                // cache: false,
                // contentType: false,
                // processData: false,
                //dataType: 'json',
                success: function(result){
                  if(result==1) {
                  toastr.success("Language Deleted Successfully");
                  setTimeout(function(){
                      location.href="<?=base_url('languages')?>";
                  }, 1000) 
          
                }else{
                  toastr.error('Something went wrong');
                }
        }});
          
        }
        });
    }
</script>
  