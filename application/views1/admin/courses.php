
  
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
	  <div class="container-full">
		<!-- Content Header (Page header) -->
		<div class="content-header">
			<div class="d-flex align-items-center">
				<div class="mr-auto">
					<h3 class="page-title"><i class="fa fa-graduation-cap"></i> <?=$page_title?></h3>
					<div class="d-inline-block align-items-center">
						<nav>
							<ol class="breadcrumb">
								<li class="breadcrumb-item"><a href="#"><i class="mdi mdi-home-outline"></i></a></li>
								<li class="breadcrumb-item" aria-current="page"><?=$page_title?></li>
								<li class="breadcrumb-item active" aria-current="page"><?=$page_title?></li>
							</ol>
						</nav>
					</div>
				</div>
				
			</div>
		</div>

		<!-- Main content -->
		<section class="content">
		  <div class="row">
			  <div class="box">
				<div class="box-header with-border">
				  <h3 class="box-title">All <?=$page_title?></h3>
				  <!-- <h6 class="box-subtitle">Export data to Copy, CSV, Excel, PDF & Print</h6> -->
				</div>
				<!-- /.box-header -->
				<div class="box-body">
					<div class="table-responsive">
					  <table id="example" class="table table-bordered table-hover display nowrap margin-top-10 w-p100">
						<thead>
							<tr>
                                <th>SNO</th>
								<th>Image</th>
								<th>Institute/Coching</th>
								<th>Course</th>
								<th>Category</th>
								<th>Mode</th>
								<th>Start Date</th>
								<th>End Date</th>
								<th>Seats</th>
								<th>Fees</th>
								<th>Discount</th>
								<th>Discription</th>
								<th>Location</th>
								<th>Created Date</th>
								<th>Status</th>
								<!-- <th>Action</th> -->
							</tr>
						</thead>
						<tbody>
                            <?php foreach($courses as $key=>$course){?>
							<tr>
								<td><?=$key+1;?></td>
								<td><img src="<?=base_url($course->image)?>" width="100" height="70"></td>
								<td><?= $course->user_name?></td>
								<td><?= $course->course?></td>
								<td><?= $course->category?></td>
								<td><?= $course->mode?></td>
								<td><?= date('d-m-Y',strtotime($course->start_date))?></td>
								<td><?= date('d-m-Y',strtotime($course->end_date))?></td>
								<td><?= $course->seats?></td>
								<td><?= $course->fees?></td>
								<td><?= $course->discount?></td>
								<td><span title="<?=$course->description?>" data-toggle="tooltip" style="cursor:pointer"><?= substr($course->description,0,50)?></span></td>
								<td><?= $course->city_name?></td>
                                <td><?= date('d-m-Y',strtotime($course->created_at));?></td>
								<td><a  href="javascript:void(0);"  <?= $course->status==0 ? 'onclick="updateCourseStatus('.$course->id.')" title="Click to Change Status"' : NULL;?>  data-toggle="tooltip" ><?= $course->status == 1 ? '<span class="text-success">Active</span>' : '<span class="btn btn-danger">De-Active</span>'?></a></td>
							</tr>
                            <?php } ?>
				
						</tbody>				  
					
					</table>
					</div>              
				</div>
				<!-- /.box-body -->
			  </div>
			  <!-- /.box -->          
			</div>
			<!-- /.col -->
		  </div>
		  <!-- /.row -->
		</section>
		<!-- /.content -->
	  
	  </div>
  </div>
  <!-- /.content-wrapper -->

<!-- View Class Modal Start -->
<!-- <div class="modal fade" id="viewUserModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">View User</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" id="viewUserData">
  
      </div>
      <div class="modal-footer text-center">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div> -->
<!-- Edit School Class Modal End -->

<script>
function updateCourseStatus(user_id){
     var messageText  = "You want to Active this course?";
     var confirmText =  'Yes, Change it!';
     var message  ="Course status changed Successfully!";
    Swal.fire({
        title: 'Are you sure?',
        text: messageText,
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: confirmText
        }).then((result) => {
        if (result.isConfirmed) {
            $.ajax({
                url: '<?=base_url('admin/courses/update_status')?>', 
                method: 'POST',
                data: {userid: user_id},
                success: function(result){
                toastr.success(message);
                setTimeout(function(){
                   window.location.reload();
                }, 2000);
        }
      });
          
        }
        })
  }

  function viewModalShow(userid){
    $.ajax({
       url: '<?=base_url('admin/user/viewUser')?>',
       type: 'POST',
       data: {userid},
       success: function (data) {
        $('#viewUserModal').modal('show');
         $('#viewUserData').html(data);
       }
     });
  }
</script>