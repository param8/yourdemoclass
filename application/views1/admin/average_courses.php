 <style>
      .toggleSwitch {
    display: inline-block;
    height: 18px;
    position: relative;
    overflow: visible;
    padding: 0;
    margin-left: 50px;
    cursor: pointer;
    width: 40px;
      user-select: none;
  }
  </style>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
	  <div class="container-full">
		<!-- Content Header (Page header) -->
		<div class="content-header">
			<div class="d-flex align-items-center">
				<div class="mr-auto">
					<h3 class="page-title"><i class="fa fa-graduation-cap"></i> <?=$page_title?></h3>
					<div class="d-inline-block align-items-center">
						<nav>
							<ol class="breadcrumb">
								<li class="breadcrumb-item"><a href="#"><i class="mdi mdi-home-outline"></i></a></li>
								<li class="breadcrumb-item" aria-current="page"><?=$page_title?></li>
								<li class="breadcrumb-item active" aria-current="page"><?=$page_title?></li>
							</ol>
						</nav>
					</div>
				</div>
				
			</div>
		</div>

		<!-- Main content -->
		<section class="content">
		  <div class="row">
			  <div class="box">
				<div class="box-header with-border">
				  <h3 class="box-title"><?=$page_title?></h3>
				  <!-- <h6 class="box-subtitle">Export data to Copy, CSV, Excel, PDF & Print</h6> -->
				</div>
				<!-- /.box-header -->
				<div class="box-body">
					<div class="table-responsive">
					  <table id="example" class="table table-bordered table-hover display nowrap margin-top-10 w-p100">
						<thead>
							<tr>
                 <th>SNO</th>
								<th>Name</th>
								<th>User Type</th>
								<th>Course</th>
								<th>City</th>
								<th>Institute</th>
								<th>Price</th>
								<th>Mode</th>
								<th>Action</th>
							</tr>
						</thead>
						<tbody>
              <?php foreach($avg_courses as $key=>$avg_course){?>
							<tr>
								<td><?=$key+1;?></td>
								<td><?= $avg_course->name?></td>
								<td><?= $avg_course->user_type?></td>
								<td><?= $avg_course->course?></td>
								<td><?= $avg_course->city?></td>
								<td><?= $avg_course->institute?></td>
								<td><?= $avg_course->price?></td>
								<td><?= $avg_course->mode?></td>
                <td onclick="updateCourseDealStatus(<?= $avg_course->id?>,<?= $avg_course->deal_status?>,)"><input type="checkbox" <?=$avg_course->deal_status == 1 ? 'checked' : ''?> data-toggle="toggle" data-on="Best Deal" data-off="No Deal"  data-size="sm" ></td>
								<!-- <td><a  href="javascript:void(0);"  <?= $avg_course->status==0 ? 'onclick="updateCourseStatus('.$avg_course->id.')" title="Click to Change Status"' : NULL;?>  data-toggle="tooltip" ><?= $avg_course->status == 1 ? '<span class="text-success">Active</span>' : '<span class="btn btn-danger">De-Active</span>'?></a></td> -->
							</tr>
                            <?php } ?>
				
						</tbody>				  
					
					</table>
					</div>              
				</div>
				<!-- /.box-body -->
			  </div>
			  <!-- /.box -->          
			</div>
			<!-- /.col -->
		  </div>
		  <!-- /.row -->
		</section>
		<!-- /.content -->
	  
	  </div>
  </div>
  <!-- /.content-wrapper -->

<script>
    function updateCourseDealStatus(userid,dealStatus){
    if(dealStatus == 1){
        var msg = 'In-active';
    }else{
        var msg = 'Active';
    }
     var messageText  = "You want to "+msg+" on deal?";
     var confirmText =  'Yes, Change it!';
     var message  ="Deal "+msg+" Successfully!";
    Swal.fire({
        title: 'Are you sure?',
        text: messageText,
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: confirmText
        }).then((result) => {
        if (result.isConfirmed) {
            $.ajax({
                url: '<?=base_url('admin/courses/update_Dealstatus')?>', 
                method: 'POST',
                data: {userid},
                success: function(result){
                toastr.success(message);
                setTimeout(function(){
                   window.location.reload();
                }, 2000);
        }
       
      });
          
        }else{
            location.reload();
        }
        })
  }
</script>
<script>


  function viewModalShow(userid){
    $.ajax({
       url: '<?=base_url('admin/user/viewUser')?>',
       type: 'POST',
       data: {userid},
       success: function (data) {
        $('#viewUserModal').modal('show');
         $('#viewUserData').html(data);
       }
     });
  }

  function updateUserStatus(id, status){
     var messageText  = "You want to "+status+" this user?";
     var confirmText =  'Yes, Change it!';
     var message  = "This user "+status+" Successfully!";
    Swal.fire({
        title: 'Are you sure?',
        text: messageText,
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: confirmText
        }).then((result) => {
        if (result.isConfirmed) {
            $.ajax({
                url: 'updateStatus.php', 
                method: 'POST',
                data: {userid: user_id, user_status: status},
                success: function(result){
                toastr.success(message);
                setTimeout(function(){
                window.location.reload();
                }, 2000);
        }
      });
          
        }else{location.reload();}
        })
  }

</script>