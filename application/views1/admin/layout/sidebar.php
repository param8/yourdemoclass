<aside class="main-sidebar">
    <!-- sidebar-->
    <section class="sidebar position-relative">	
	  	<div class="multinav">
		  <div class="multinav-scroll" style="height: 100%; overflow:auto">	
			  <!-- sidebar menu-->
			  <ul class="sidebar-menu" data-widget="tree">	
				<li class="header">Dashboard</li>
				<li class="">
				  <a href="<?=base_url('dashboard')?>">
					<i class="icon-Layout-4-blocks"><span class="path1"></span><span class="path2"></span></i>
					<span>Dashboard</span>
				</li>
		
				<li class="header">Components </li>
				<li >
				  <a href="<?=base_url('institutes')?>">
					<i class="fa fa-institution"><span class="path1"></span><span class="path2"></span></i>
					<span>Institute</span>
				</li>
				<li>
				  <a href="<?=base_url('students')?>">
					<i class="fa fa-users"><span class="path1"></span><span class="path2"></span></i>
					<span>Student</span>
				</li>

				<li >
				  <a href="<?=base_url('experts')?>">
					<i class="fa fa-users"><span class="path1"></span><span class="path2"></span></i>
					<span>Experts</span>
				</li>

				<li class="treeview">
				  <a href="#">
					<i class="fa fa-graduation-cap"><span class="path1"></span><span class="path2"></span></i>
					<span>Course</span>
					<span class="pull-right-container">
					  <i class="fa fa-angle-right pull-right"></i>
					</span>
				  </a>
				  <ul class="treeview-menu">
					<li><a href="<?=base_url('new-courses')?>"><i class="fa fa-book"><span class="path1"></span><span class="path2"></span></i>New Courses</a></li>
					<li><a href="<?=base_url('approved-courses')?>"><i class="fa fa-book"><span class="path1"></span><span class="path2"></span></i>Approved Courses</a></li>
					<li><a href="<?=base_url('averageCourses')?>"><i class="fa fa-book"><span class="path1"></span><span class="path2"></span></i>Average Courses</a></li>
					
				</ul>
				</li>
				<li class="treeview">
				  <a href="#">
					<i class="fa fa-database"><span class="path1"></span><span class="path2"></span></i>
					<span>Master</span>
					<span class="pull-right-container">
					  <i class="fa fa-angle-right pull-right"></i>
					</span>
				  </a>
				  <ul class="treeview-menu">
				  	<li><a href="<?=base_url('category')?>"><i class="fa fa-book"><span class="path1"></span><span class="path2"></span></i>Category</a></li>
					<li><a href="<?=base_url('average-price')?>"><i class="fa fa-tachometer"><span class="path1"></span><span class="path2"></span></i>Average Price</a></li>
					
				</ul>
				</li>
		

				<li class="treeview">
				  <a href="#">
					<i class="icon-Settings"><span class="path1"></span><span class="path2"></span></i>
					<span>Settings</span>
					<span class="pull-right-container">
					  <i class="fa fa-angle-right pull-right"></i>
					</span>
				  </a>
				  <ul class="treeview-menu">
					<li><a href="<?=base_url('site-info')?>"><i class="icon-Image"><span class="path1"></span><span class="path2"></span></i>Site Setting</a></li>  
				</ul>
				</li>  		
			  </ul>
		  </div>
		</div>
    </section>
	<div class="sidebar-footer">
		<a href="javascript:void(0)" class="link" data-toggle="tooltip" title="" data-original-title="Settings" aria-describedby="tooltip92529"><span class="icon-Settings-2"></span></a>
		<a href="mailbox.html" class="link" data-toggle="tooltip" title="" data-original-title="Email"><span class="icon-Mail"></span></a>
		<a href="<?=base_url('authantication/adminLogout')?>" class="link" data-toggle="tooltip" title="" data-original-title="Logout"><span class="icon-Lock-overturning"><span class="path1"></span><span class="path2"></span></span></a>
	</div>
  </aside>